package com.hdv.abyss.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.GameChooser;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Configurations.APP_WIDTH;
		config.height = Configurations.APP_HEIGHT;
		new LwjglApplication(GameChooser.createGame(), config);
	}
}
