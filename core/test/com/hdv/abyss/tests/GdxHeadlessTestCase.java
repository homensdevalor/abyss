package com.hdv.abyss.tests;

import org.junit.Before;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;

public class GdxHeadlessTestCase {
	final HeadlessApplicationConfiguration config;
	HeadlessApplication application;
	ApplicationListener applicationListener;
	
	public GdxHeadlessTestCase(){
		this(new EmptyApplicationListener());
	}
	public GdxHeadlessTestCase(ApplicationListener listener){
		config = new HeadlessApplicationConfiguration();
	}
	
	@Before
	public void setup(){
        application = new HeadlessApplication(new EmptyApplicationListener(), config);       
	}
}
