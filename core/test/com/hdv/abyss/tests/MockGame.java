package com.hdv.abyss.tests;

import com.badlogic.gdx.Game;

public class MockGame extends Game{
	private OnCreateListener listener;
	public MockGame(OnCreateListener listener) {
		this.listener = listener;
	}
	@Override
	public void create() {
		if(listener != null){
			listener.onCreate();
		}
	}
	
}