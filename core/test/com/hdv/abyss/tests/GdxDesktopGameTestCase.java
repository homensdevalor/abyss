package com.hdv.abyss.tests;

import java.util.concurrent.CountDownLatch;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.ApplicationListener;
import com.hdv.abyss.scenes.GameStage;

public abstract class GdxDesktopGameTestCase extends GdxDesktopTestCase implements OnCreateListener{

	GameStage stage;
	CountDownLatch latcher;

	@Before
	public void setup(){
		super.setup();
		latcher = new CountDownLatch(1);
	}

	@Test
	public void voidTest() throws InterruptedException{
		//Espera onCreate ser executado para encerrar teste
		latcher.await();
	}

	@Override
	protected ApplicationListener getInstance() {
		return new MockGame((OnCreateListener)this);
	}
	@Override
	public void onCreate() {
		runTests();
		latcher.countDown();
	}
	
	protected abstract void runTests();
}
