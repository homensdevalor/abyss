package com.hdv.abyss.tests;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.Direction;

public class MockDirectionalAnimation implements DirectionalAnimation{
	
	TextureRegion region;
public MockDirectionalAnimation() {
	region = new TextureRegion(new Texture(new Pixmap(1, 1, Format.RGB888)));
}
	@Override
	public TextureRegion getKeyFrame(Direction playerDirection) {
		// TODO Auto-generated method stub
		return region;
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PlayMode getPlayMode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPlayMode(PlayMode mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setAnimationTime(float animationTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public float getAnimationTime() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setAnimationDuration(float stateDurationInSeconds) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public float getAnimationDuration() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Vector2 getOffset() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setOffset(Vector2 offset) {
		// TODO Auto-generated method stub
		
	}

}
