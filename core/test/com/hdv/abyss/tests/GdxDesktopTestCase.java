package com.hdv.abyss.tests;

import org.junit.After;
import org.junit.Before;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

abstract public class GdxDesktopTestCase {
	protected abstract ApplicationListener getInstance();
	
	final LwjglApplicationConfiguration config;
	protected LwjglApplication application;
	protected ApplicationListener applicationListener;

	public GdxDesktopTestCase(){
		config = new LwjglApplicationConfiguration();
	}
	
	@Before
	public void setup(){
        application = new LwjglApplication(getInstance(), config); 
	}
	
	@After
	public void tearDown(){
		application.getApplicationListener().dispose();
		application.exit();
	}
}
