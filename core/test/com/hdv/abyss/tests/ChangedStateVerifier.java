package com.hdv.abyss.tests;

import org.junit.Assert;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.ChangedStateEvent;
import com.hdv.abyss.statemachine.OnStateChangeListener;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.State;

public class ChangedStateVerifier implements OnStateChangeListener {
	public static void testCharacterTransitionTo(GameCharacter character, GameEvent startEvent, Class<? extends State<?> > stateClass) {
		ChangedStateVerifier verifier = new ChangedStateVerifier(stateClass);
    	character.addOnStateChangeListener(verifier);
    	character.fireEvent(startEvent);
    	
    	Assert.assertTrue("Transição de estado não ocorreu", verifier.hasChanged());
    	
    	character.removeOnStateChangeListener(verifier);
	}
	public static void testCharacterTimedTransition(GameCharacter character, Class<? extends State<?> > stateClass, long MAX_TIME_MILLIS) {
		testCharacterTimedTransition(character, stateClass, MAX_TIME_MILLIS, MAX_TIME_MILLIS/20);
	}
	public static void testCharacterTimedTransition(GameCharacter character, Class<? extends State<?> > stateClass
			, long MAX_TIME_MILLIS, long ticks) 
	{
		ChangedStateVerifier timedVerifier = new ChangedStateVerifier(stateClass);
    	character.addOnStateChangeListener(timedVerifier);
    	long startTime = System.currentTimeMillis();
    	long previousTime = startTime;
    	long currentTime;
    	do{
    		currentTime = System.currentTimeMillis();
    		float deltaSeconds = (currentTime - previousTime)/1000f;
    		character.act(deltaSeconds);
    		previousTime = currentTime;
    		
    		if(ticks > 0){
	    		try {
					Thread.sleep(ticks);
				} catch (InterruptedException e) {
					e.printStackTrace();
					Assert.fail(e.getMessage());
				}
    		}
    	}while(!timedVerifier.hasChanged() && (currentTime - startTime) < MAX_TIME_MILLIS);
    	Assert.assertTrue(timedVerifier.hasChanged());
    	character.removeOnStateChangeListener(timedVerifier);
	}
	
	private boolean changedState;
	private Class<?> stateClass;
	public ChangedStateVerifier(Class<?> stateClass) {
		this.stateClass = stateClass;
		changedState = false;
	}
	@Override
	public void onStateChange(ChangedStateEvent<?> changedStateEvent) {
		State<?> newState = changedStateEvent.getNewState();
		Assert.assertNotNull(newState);
		String message = "Expected " + stateClass.toString() + ", but got " + newState.getClass().toString();
		Assert.assertTrue(message, stateClass.isInstance(newState));
		changedState = true;
	}
	public boolean hasChanged() {
		return changedState;
	}
}