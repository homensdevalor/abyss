package com.hdv.abyss.physics;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;

public class Direction_roundFromVectorTest {

	@Test
	public void roundFromVectorShouldNotConvertZeroVector(){
		assertEquals(null, Direction.roundFromVector(new Vector2(0,0)));
	}
//	@Test
//	public void round_from_vector_should_not_convert_diagonal_vectors(){
//		/** Não é possível determinar uma única direção válida para os casos em que ambos valores são iguais*/
//		assertEquals(null, Direction.roundFromVector(new Vector2(1,1)));
//		assertEquals(null, Direction.roundFromVector(new Vector2(1,-1)));
//		assertEquals(null, Direction.roundFromVector(new Vector2(-1,1)));
//		assertEquals(null, Direction.roundFromVector(new Vector2(-1,-1)));
//
//		assertEquals(null, Direction.roundFromVector(new Vector2(100,100)));
//		assertEquals(null, Direction.roundFromVector(new Vector2(20,-20)));
//		assertEquals(null, Direction.roundFromVector(new Vector2(-0.3f,0.3f)));
//		assertEquals(null, Direction.roundFromVector(new Vector2(-0.002f,-0.002f)));
//	}
	@Test
	public void roundFromVectorShouldConvertOrthogonalVectors(){
		assertEquals(Direction.Down, Direction.roundFromVector(new Vector2(0,-1)));
		assertEquals(Direction.Left, Direction.roundFromVector(new Vector2(-1,0)));
		assertEquals(Direction.Right, Direction.roundFromVector(new Vector2(1, 0)));
		assertEquals(Direction.Up, Direction.roundFromVector(new Vector2(0,1)));
	}
	@Test
	public void roundFromVectorShouldIgnoreMinusZeroErrors(){
		assertEquals(Direction.Down, Direction.roundFromVector(new Vector2(-0,-1)));
		assertEquals(Direction.Left, Direction.roundFromVector(new Vector2(-1,-0)));
		assertEquals(Direction.Right, Direction.roundFromVector(new Vector2(1, -0)));
		assertEquals(Direction.Up, Direction.roundFromVector(new Vector2(-0,1)));
	}
	@Test
	public void roundFromVectorShouldConvertNonUnitOrthogonalVectors(){
		assertEquals(Direction.Down, Direction.roundFromVector(new Vector2(0,-10)));
		assertEquals(Direction.Left, Direction.roundFromVector(new Vector2(-3,0)));
		assertEquals(Direction.Right, Direction.roundFromVector(new Vector2(11, 0)));
		assertEquals(Direction.Up, Direction.roundFromVector(new Vector2(0,77)));
	}
	@Test
	public void roundFromVectorShouldConvertNonOrthogonalVectors(){
		assertEquals(Direction.Down, Direction.roundFromVector(new Vector2(0.1f,-1)));
		assertEquals(Direction.Up, Direction.roundFromVector(new Vector2(-1,20)));
		
		assertEquals(Direction.Down, Direction.roundFromVector(new Vector2(2,-10)));
		assertEquals(Direction.Up, Direction.roundFromVector(new Vector2(11, 20)));
		assertEquals(Direction.Up, Direction.roundFromVector(new Vector2(0.1f,77)));

		assertEquals(Direction.Right, Direction.roundFromVector(new Vector2(1.1f,1)));
		assertEquals(Direction.Left, Direction.roundFromVector(new Vector2(-1.1f,1)));
		assertEquals(Direction.Up, Direction.roundFromVector(new Vector2(1,1.1f)));
		assertEquals(Direction.Down, Direction.roundFromVector(new Vector2(1,-1.1f)));
	}
	
}
