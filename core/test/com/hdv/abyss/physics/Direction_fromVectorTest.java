package com.hdv.abyss.physics;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;

public class Direction_fromVectorTest {

	@Test
	public void fromVectorShouldNotConvertZeroVector(){
		assertEquals(null, Direction.fromVector(new Vector2(0,0)));
	}
	@Test
	public void fromVectorShouldConvertOrthogonalVectors(){
		assertEquals(Direction.Down, Direction.fromVector(new Vector2(0,-1)));
		assertEquals(Direction.Left, Direction.fromVector(new Vector2(-1,0)));
		assertEquals(Direction.Right, Direction.fromVector(new Vector2(1, 0)));
		assertEquals(Direction.Up, Direction.fromVector(new Vector2(0,1)));
	}
	@Test
	public void fromVectorShouldIgnoreMinusZeroErrors(){
		assertEquals(Direction.Down, Direction.fromVector(new Vector2(-0,-1)));
		assertEquals(Direction.Left, Direction.fromVector(new Vector2(-1,-0)));
		assertEquals(Direction.Right, Direction.fromVector(new Vector2(1, -0)));
		assertEquals(Direction.Up, Direction.fromVector(new Vector2(-0,1)));
	}
	@Test
	public void fromVectorShouldConvertNonUnitOrthogonalVectors(){
		assertEquals(Direction.Down, Direction.fromVector(new Vector2(0,-10)));
		assertEquals(Direction.Left, Direction.fromVector(new Vector2(-3,0)));
		assertEquals(Direction.Right, Direction.fromVector(new Vector2(11, 0)));
		assertEquals(Direction.Up, Direction.fromVector(new Vector2(0,77)));
	}
	@Test
	public void fromVectorShouldNotConvertNonOrthogonalVectors(){
		assertEquals(null, Direction.fromVector(new Vector2(0.1f,-1)));
		assertEquals(null, Direction.fromVector(new Vector2(-1,20)));
		assertEquals(null, Direction.fromVector(new Vector2(1, 1)));
		assertEquals(null, Direction.fromVector(new Vector2(-1,1)));
		
		assertEquals(null, Direction.fromVector(new Vector2(2,-10)));
		assertEquals(null, Direction.fromVector(new Vector2(-3,-3)));
		assertEquals(null, Direction.fromVector(new Vector2(11, 20)));
		assertEquals(null, Direction.fromVector(new Vector2(0.1f,77)));
	}
}
