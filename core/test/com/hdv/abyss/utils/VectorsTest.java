package com.hdv.abyss.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class VectorsTest {

	private static final double EPSILON = 0.01;

	@Test
	public void vectorsToAngleShouldConvertOrthogonalVectors(){
		assertEquals(0, Vectors.vectorToAngle(new Vector2(0,0)), EPSILON);
		assertEquals(MathUtils.PI/2f, Vectors.vectorToAngle(new Vector2(0,1)), EPSILON);
		assertEquals(MathUtils.PI, Vectors.vectorToAngle(new Vector2(-1,0)), EPSILON);
		assertEquals(0 , Vectors.vectorToAngle(new Vector2(1,0)), EPSILON);
		assertEquals(-MathUtils.PI/2f, Vectors.vectorToAngle(new Vector2(0,-1)), EPSILON);
	}
	@Test
	public void vectorsToAngleShouldConvertDiagonalVectors(){
		assertEquals(MathUtils.PI  * 1/4f , Vectors.vectorToAngle(new Vector2(1,1)), EPSILON);
		assertEquals(-MathUtils.PI * 1/4f , Vectors.vectorToAngle(new Vector2(1,-1)), EPSILON);
		assertEquals(MathUtils.PI  *3/4f  , Vectors.vectorToAngle(new Vector2(-1,1)), EPSILON);
		assertEquals(-MathUtils.PI  *3/4f  , Vectors.vectorToAngle(new Vector2(-1,-1)), EPSILON);
	}
	
	@Test
	public void vectorsToAngleShouldBeAngleToVectorInverse(){
		assertVectorToAngleInverse(new Vector2(1,0));
		assertVectorToAngleInverse(new Vector2(0,1));
		assertVectorToAngleInverse(new Vector2(0,-1));
		assertVectorToAngleInverse(new Vector2(-1,0));
		
		assertVectorToAngleInverse(new Vector2(1,1));
		assertVectorToAngleInverse(new Vector2(1,-1));
		assertVectorToAngleInverse(new Vector2(-1,1));
		assertVectorToAngleInverse(new Vector2(-1,-1));
	}
	private void assertVectorToAngleInverse(Vector2 vector) {
		float angle = Vectors.vectorToAngle(vector);
		System.out.println("angle: " + angle * MathUtils.radiansToDegrees);
		Vector2 outVec = new Vector2();
		Vectors.angleToVector(outVec, angle);
		Vector2 expectedVector = vector.cpy().nor();
		assertEquals(expectedVector.x, outVec.x, EPSILON);
		assertEquals(expectedVector.y, outVec.y, EPSILON);
	}
	@Test
	public void angleToVectorShouldBeVectorsToAngleInverse(){
		assertEquals(MathUtils.PI  * 1/4f , Vectors.vectorToAngle(new Vector2(1,1)), EPSILON);
		assertEquals(-MathUtils.PI * 1/4f , Vectors.vectorToAngle(new Vector2(1,-1)), EPSILON);
		assertEquals(MathUtils.PI  *3/4f  , Vectors.vectorToAngle(new Vector2(-1,1)), EPSILON);
		assertEquals(-MathUtils.PI  *3/4f  , Vectors.vectorToAngle(new Vector2(-1,-1)), EPSILON);
	}
	
	@Test
	public void angle_between(){
		assertEquals(45 * MathUtils.degreesToRadians , Vectors.angleBetween(new Vector2(1,1), new Vector2(1,0)), EPSILON);
		assertEquals(135 * MathUtils.degreesToRadians , Vectors.angleBetween(new Vector2(-1,-1), new Vector2(0,0)), EPSILON);
		
		assertEquals(30 * MathUtils.degreesToRadians 
				, Vectors.angleBetween(Vectors.angleToVector(new Vector2(), -30 * MathUtils.degreesToRadians)
									 , Vectors.angleToVector(new Vector2(), -60 * MathUtils.degreesToRadians)), EPSILON);

		testAngleBetween(130, -170, 60);
		testAngleBetween(10, -20, -10);
		testAngleBetween(0, 360, 0);
		testAngleBetween(1, 359, 0);
		testAngleBetween(180, 90, 270);
		testAngleBetween(90, 45,-45);
		testAngleBetween(90, 135,45);
	}
	
	private void testAngleBetween(float expectedDegrees, float degree1, float degree2){
		float result = Vectors.angleBetween(Vectors.angleToVector(new Vector2(), degree1 * MathUtils.degreesToRadians)
				 , Vectors.angleToVector(new Vector2(), degree2 * MathUtils.degreesToRadians));
		
		String errMessage = "Expected " + expectedDegrees + " but found " + MathUtils.radiansToDegrees * result;
		assertEquals(errMessage,expectedDegrees * MathUtils.degreesToRadians  , result, EPSILON);
	}
}
