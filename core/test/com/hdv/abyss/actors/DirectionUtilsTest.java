package com.hdv.abyss.actors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Assert;
import org.junit.Test;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.Direction;

public class DirectionUtilsTest {
	@Test
	public void when_direction_is_orthogonal_new_direction_should_be_that(){
		testUpdateDirection(Direction.Right, vec(1f,0f) , anyDir() , anyVec());
		testUpdateDirection(Direction.Left , vec(-1f,0f), anyDir() , anyVec());
		testUpdateDirection(Direction.Up   , vec(0,1f)  , anyDir() , anyVec());
		testUpdateDirection(Direction.Down , vec(0,-1f) , anyDir() , anyVec());
	}
	static final Direction[] directions = new Direction[]{Direction.Down, Direction.Left,Direction.Right, Direction.Up};
	
	private Direction anyDir() {
		int dirIndex = MathUtils.random(3);
		return directions[dirIndex];
	}

	private Vector2 anyVec() {
		return new Vector2(MathUtils.random(),MathUtils.random());
	}

	@Test
	public void test_is_zero_or_diagonal(){
		testIsZeroOrDiagonal(true, vec(0,0));
		testIsZeroOrDiagonal(true, vec(1,1));
		testIsZeroOrDiagonal(true, vec(0.91f,1f));
		testIsZeroOrDiagonal(true, vec(1.09f, 1f));
		
		testIsZeroOrDiagonal(true, vec(-1,-1f));
		testIsZeroOrDiagonal(true, vec(-1,-1.1f));
	}

	private void testIsZeroOrDiagonal(boolean isZeroOrDiagonal, Vector2 vec) {
		String errMsg = "Vetor " + vec + (isZeroOrDiagonal ? " deveria":" não deveria") 
										+ " ser zero ou diagonal";
		assertEquals(errMsg, isZeroOrDiagonal, DirectionUtils.isZeroOrDiagonal(vec));
	}
	
	@Test
	public void test_is_zero_or_diagonal_false(){
		assertFalse(DirectionUtils.isZeroOrDiagonal(vec(0,1)));
		assertFalse(DirectionUtils.isZeroOrDiagonal(vec(1,-1.5f)));
		assertFalse(DirectionUtils.isZeroOrDiagonal(vec(0.8f,1f)));
		assertFalse(DirectionUtils.isZeroOrDiagonal(vec(1.1f, 0.1f)));
		assertFalse(DirectionUtils.isZeroOrDiagonal(vec(10f,1.1f)));
	}
	
//	private void printVecAngle(Vector2 vec) {
//		System.out.println("Vec: " + vec + "; angle: " + MathUtils.radiansToDegrees * Vectors.vectorToAngle(vec));
//	}

	@Test
	public void direction_non_orthogonal_and_non_diagonal_should_be_rounded(){
		testUpdateDirection(Direction.Right, vec(1f,0.4f)    , anyDir() , anyVec());
		testUpdateDirection(Direction.Left , vec(-1f,0.6f)   , anyDir() , anyVec());
		testUpdateDirection(Direction.Up   , vec(0.1f,0.8f)  , anyDir() , anyVec());
		testUpdateDirection(Direction.Down , vec(3.5f,-7.7f) , anyDir() , anyVec());
		
//		assertEquals(Direction.Right, Direction.roundFromVector(vec(1f,0.4f)));
//		assertEquals(Direction.Left , Direction.roundFromVector(vec(-1f,0.6f)));
//		assertEquals(Direction.Up   , Direction.roundFromVector(vec(0.1f,0.8f)));
//		assertEquals(Direction.Down , Direction.roundFromVector(vec(3.5f,-7.7f)));
	}

	@Test
	public void when_direction_is_diagonal_and_move_is_not_opposite_keep_direction(){
		testUpdateDirection(Direction.Up   , vec(1f,1f), Direction.Up    , Direction.Up.toVector());
		testUpdateDirection(Direction.Right, vec(1f,1f), Direction.Right , Direction.Right.toVector());
		
		testUpdateDirection(Direction.Down   , vec(1f,-1f), Direction.Down  , Direction.Down.toVector());
		testUpdateDirection(Direction.Right, vec(1f,-1f), Direction.Right , Direction.Right.toVector());
		
		testUpdateDirection(Direction.Up   , vec(-1f,1f), Direction.Up    , Direction.Up.toVector());
		testUpdateDirection(Direction.Left, vec(-1f,1f), Direction.Left  , Direction.Left.toVector());
		
		testUpdateDirection(Direction.Down   , vec(-1f,-1f), Direction.Down  , Direction.Down.toVector());
		testUpdateDirection(Direction.Left, vec(-1f,-1f), Direction.Left  , Direction.Left.toVector());
		

		testUpdateDirection(Direction.Up   , vec(1f,1f), Direction.Down    , vec(0.1f,-0.9f));
		testUpdateDirection(Direction.Right, vec(1f,1f), Direction.Right   ,  vec(1f,-0.8f));
	}
	@Test
	public void when_direction_is_diagonal_and_move_is_opposite_flip_direction(){
		testUpdateDirection(Direction.Up   , vec(1f,1f), Direction.Down    , Direction.Down.toVector());
		testUpdateDirection(Direction.Right, vec(1f,1f), Direction.Left    , Direction.Left.toVector());
		
		testUpdateDirection(Direction.Down , vec(1f,-1f), Direction.Up  , Direction.Up.toVector());
		testUpdateDirection(Direction.Right, vec(1f,-1f), Direction.Left , Direction.Left.toVector());
		testUpdateDirection(Direction.Right, vec(1f,-1f), Direction.Left , Direction.Right.toVector());
		
		testUpdateDirection(Direction.Up   , vec(-1f,1f), Direction.Down  , Direction.Down.toVector());
		testUpdateDirection(Direction.Left, vec(-1f,1f), Direction.Right  , Direction.Right.toVector());
		
		testUpdateDirection(Direction.Down   , vec(-1f,-1f), Direction.Up  , Direction.Up.toVector());
		testUpdateDirection(Direction.Left, vec(-1f,-1f), Direction.Right  , Direction.Right.toVector());
		
		testUpdateDirection(Direction.Up   , vec(1f,1f), Direction.Down    , vec(-0.1f,-1));
		testUpdateDirection(Direction.Right, vec(1f,1f), Direction.Left    , vec(-1f,-0.89f));
	}

	private void testUpdateDirection(Direction expectedDir, Vector2 moveDir, 
			Direction currentDir, Vector2 currentMoveDir) 
	{
		Direction result = DirectionUtils.updateDirection(moveDir, currentDir, currentMoveDir);
		Assert.assertEquals(expectedDir, result);
	}
	private Vector2 vec(float x, float y){
		return new Vector2(x,y);
	}
}
