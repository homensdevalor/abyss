package com.hdv.abyss.actors.statemachine;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.states.CharacterDamageState;
import com.hdv.abyss.actors.statemachine.states.CharacterDodgingState;
import com.hdv.abyss.actors.statemachine.states.CharacterMovingState;
import com.hdv.abyss.actors.statemachine.states.CharacterStillState;
import com.hdv.abyss.actors.weapon.Attack;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicalWorld;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.events.MovementEvent;
import com.hdv.abyss.tests.ChangedStateVerifier;
import com.hdv.abyss.tests.GdxHeadlessTestCase;

public class FullCharacterStatesTest extends GdxHeadlessTestCase{
	private GameCharacter character;
	private PhysicalWorld pworld;
	
	@Before
	public void setup(){     
        super.setup();
        pworld = new PhysicalWorld();
		character = new GameCharacter(new HashMap<String, DirectionalAnimation>());
		pworld.addActor(character);
	}
	
//    @Test
//    public void testTransitionToAttacking() {
//    	AttackEvent evt = new AttackEvent();
//    	ChangedStateVerifier.testCharacterTransitionTo(character, evt, CharacterAttackingState.class);
//    	
//    	long MAX_TIME_MILLIS = 1000;
//    	ChangedStateVerifier.testCharacterTimedTransition(character, CharacterStillState.class, MAX_TIME_MILLIS);
//    }
    
    @Test
    public void testTransitionToMoving() {
    	MovementEvent moveEvent = new MovementEvent(new Vector2(1, 1), Movement.NORMAL);
    	MovementEvent stopEventNormal = new MovementEvent(new Vector2(0,0), Movement.NORMAL);
    	MovementEvent stopEventDodge = new MovementEvent(new Vector2(0,0), Movement.DODGE);
    	ChangedStateVerifier.testCharacterTransitionTo(character, moveEvent, CharacterMovingState.class );
    	//De volta a estado parado
    	ChangedStateVerifier.testCharacterTransitionTo(character, stopEventNormal, CharacterStillState.class );
    	
    	ChangedStateVerifier.testCharacterTransitionTo(character, moveEvent, CharacterMovingState.class );
    	//De volta a estado parado com Movement.DODGE
    	ChangedStateVerifier.testCharacterTransitionTo(character, stopEventDodge, CharacterStillState.class );
    }
    
    @Test
    public void testTransitionToDamage() {
    	CollisionEvent event = new CollisionEvent(new Attack(new Object(), Direction.Up), character);
    	ChangedStateVerifier.testCharacterTransitionTo(character, event, CharacterDamageState.class);
    	
    	long MAX_TIME_MILLIS = 800;
    	ChangedStateVerifier.testCharacterTimedTransition(character, CharacterStillState.class, MAX_TIME_MILLIS);
    }
    
    @Test
    public void testTransitionToDodge() {
    	MovementEvent evt = new MovementEvent(new Vector2(5, 5), Movement.DODGE);
    	ChangedStateVerifier.testCharacterTransitionTo(character, evt, CharacterDodgingState.class);
    	
    	long MAX_TIME_MILLIS = 800;
    	ChangedStateVerifier.testCharacterTimedTransition(character, CharacterStillState.class, MAX_TIME_MILLIS, 40);
    }

    @Test
    public void whenMovingCharacterCanBeDamaged() {
    	MovementEvent moveEvent = new MovementEvent(new Vector2(-1, 0), Movement.NORMAL);
    	ChangedStateVerifier.testCharacterTransitionTo(character, moveEvent, CharacterMovingState.class );
    	
    	Attack someAttack = new Attack(new Object(), Direction.Down);
    	CollisionEvent event = new CollisionEvent(someAttack, character);
    	ChangedStateVerifier.testCharacterTransitionTo(character, event, CharacterDamageState.class);
    }
    @Test
    public void whenDodgingCharacterCanBeDamaged() {
    	MovementEvent dodgeEvent = new MovementEvent(new Vector2(-1, 0), Movement.DODGE);
    	ChangedStateVerifier.testCharacterTransitionTo(character, dodgeEvent, CharacterDodgingState.class );
    	
    	Attack someAttack = new Attack(new Object(), Direction.Down);
    	CollisionEvent event = new CollisionEvent(someAttack, character);
    	ChangedStateVerifier.testCharacterTransitionTo(character, event, CharacterDamageState.class);
    }
    
    // TODO: Testar Dying
}
