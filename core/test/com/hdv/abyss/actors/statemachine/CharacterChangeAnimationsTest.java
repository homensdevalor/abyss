package com.hdv.abyss.actors.statemachine;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.states.CharacterDamageState;
import com.hdv.abyss.actors.statemachine.states.CharacterDodgingState;
import com.hdv.abyss.actors.statemachine.states.CharacterMovingState;
import com.hdv.abyss.actors.weapon.Attack;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicalWorld;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.events.MovementEvent;
import com.hdv.abyss.tests.ChangedStateVerifier;
import com.hdv.abyss.tests.GdxDesktopGameTestCase;
import com.hdv.abyss.tests.MockDirectionalAnimation;

public class CharacterChangeAnimationsTest extends GdxDesktopGameTestCase{
	private GameCharacter character;
	private PhysicalWorld pworld;
	Map<Object, DirectionalAnimation> animationMap;
	@Before
	public void setup(){     
        super.setup();
	}

	private Map<Object, DirectionalAnimation> buildAnimationMap() {
		animationMap =  new HashMap<Object, DirectionalAnimation>();
		animationMap.put(BattleCharacterStates.ATTACKING, new MockDirectionalAnimation());
		animationMap.put(BattleCharacterStates.DAMAGED,   new MockDirectionalAnimation());
		animationMap.put(BattleCharacterStates.DEAD,      new MockDirectionalAnimation());
		animationMap.put(BattleCharacterStates.DODGING,   new MockDirectionalAnimation());
		animationMap.put(BattleCharacterStates.MOVING,    new MockDirectionalAnimation());
		animationMap.put(BattleCharacterStates.STILL,     new MockDirectionalAnimation());
		return animationMap;
	}

	private void assertCurrentAnimation(BattleCharacterStates animationState) {
		assertEquals(character.getCurrentAnimation(), animationMap.get(animationState));
	}

	private void internalSetup(){
		character = new GameCharacter(buildAnimationMap());
        pworld = new PhysicalWorld();
		pworld.addActor(character);
	}

	@Override
	protected void runTests() {
//		internalSetup();
//		testAttackAnimation();
		
		internalSetup();
		testMovingAnimation();
		
		internalSetup();
		testDamagedAnimation();
		
		internalSetup();
		testDodgeAnimation();
		
		internalSetup();
		testDieAnimation();
	}
//    public void testAttackAnimation() {
//    	AttackEvent evt = new AttackEvent();
//    	ChangedStateVerifier.testCharacterTransitionTo(character, evt, CharacterAttackingState.class);
//    	assertCurrentAnimation(BattleCharacterStates.ATTACKING);
//    }
    
    public void testMovingAnimation() {
    	MovementEvent moveEvent = new MovementEvent(new Vector2(1, 1), Movement.NORMAL);
    	ChangedStateVerifier.testCharacterTransitionTo(character, moveEvent, CharacterMovingState.class );
    	assertCurrentAnimation(BattleCharacterStates.MOVING);
    }
    
    public void testDamagedAnimation() {
    	CollisionEvent event = new CollisionEvent(new Attack(new Object(), Direction.Up), character);
    	ChangedStateVerifier.testCharacterTransitionTo(character, event, CharacterDamageState.class);
    	assertCurrentAnimation(BattleCharacterStates.DAMAGED);
    }
    
    public void testDodgeAnimation() {
    	MovementEvent evt = new MovementEvent(new Vector2(5, 5), Movement.DODGE);
    	ChangedStateVerifier.testCharacterTransitionTo(character, evt, CharacterDodgingState.class);
    	assertCurrentAnimation(BattleCharacterStates.DODGING);
    }

    public void testDieAnimation() {
    	character.die();
    	assertCurrentAnimation(BattleCharacterStates.DEAD);
    }
}
