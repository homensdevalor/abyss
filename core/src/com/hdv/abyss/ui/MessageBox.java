package com.hdv.abyss.ui;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.TimeUtils;
import com.hdv.abyss.Resources;
import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.global.GlobalListener;
import com.hdv.abyss.statemachine.events.ShowTextEvent;

public class MessageBox extends Table implements GlobalListener{
	public interface TextAnimationListener {
		public void onFinishAnimation();
	}

	private String textToAnimate;
	private int characterCount; 
	private long lastAnimationTime, characterAnimationDuration, timeToStart;
	private List<TextAnimationListener> listeners;
	private TextArea textArea;
	/* *************************** Inicialização ********************************/
	public MessageBox(Skin skin, int width, int height) {
		super(skin);
		setup(skin, width, height);
		
	}
	private final void setup(Skin skin, int width, int height){
		TextFieldStyle messageBoxStyle =  skin.get(Resources.messageBoxStyle,TextFieldStyle.class);
		messageBoxStyle.font = Resources.instance().generateFont(Resources.dialogueFont, Resources.dialogueFontSize);

		Texture texture = new Texture(Gdx.files.internal(Resources.messageBoxBackground));
		this.setBackground(new TextureRegionDrawable(new TextureRegion(texture)));
		textArea = new TextArea("", messageBoxStyle);

		int pad = 15;
		this.add(textArea).pad(pad).height(height - pad*2).width(width- pad*2);

		textToAnimate = "";
		characterCount = 0;
		lastAnimationTime = 0;
		characterAnimationDuration = 100;
		listeners = new ArrayList<MessageBox.TextAnimationListener>();
	}
	
	
	
	/* **************************** Getters and Setters *************************/
	public long getCharacterAnimationDuration() {
		return characterAnimationDuration;
	}
	public void setCharacterAnimationDuration(long characterAnimationDuration) {
		this.characterAnimationDuration = characterAnimationDuration;
	}
	/* **************************************************************************/
	public String getTextToAnimate(){
		return this.textToAnimate;
	}
	public void animateText(String text) {
		this.animateText(text, 0);
	}
	public void animateText(String text, long timeToStart) {
		this.textToAnimate = text;
		characterCount = 0;
		lastAnimationTime = TimeUtils.millis();
		this.timeToStart = timeToStart;
	}
	public void skipTextAnimation() {
		characterCount = textToAnimate.length() + 1;
		textArea.setText(textToAnimate);
		notifyFinishAnimation();
	}
	public void hide(){
		super.setVisible(false);
	}
	public void show(){
		super.setVisible(true);
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(hasTextToAnimate() && isTimeToAnimate()){
			if(characterCount == 0){
				this.show();
			}
			animateChar();
		}
	}

	private void animateChar() {
		textArea.setText(textToAnimate.substring(0, characterCount));
		++characterCount;
		if(animatedFinished()){
			notifyFinishAnimation();
		}
	}

	private void notifyFinishAnimation() {
		for(TextAnimationListener listener : this.listeners){
			listener.onFinishAnimation();
		}
	}

	private boolean animatedFinished() {
		return !hasTextToAnimate();
	}

	private boolean isTimeToAnimate() {
		long currentTime = TimeUtils.millis();
		if(this.characterCount == 0){
			boolean start = currentTime >= lastAnimationTime + timeToStart;
			if(start){
				lastAnimationTime += timeToStart;
			}
			return start;
		}
		else if(currentTime - lastAnimationTime > characterAnimationDuration){
			lastAnimationTime += characterAnimationDuration;
			return true;
		}
		return false;
	}

	private boolean hasTextToAnimate() {
		return textToAnimate != null && !textToAnimate.isEmpty()
				&& characterCount <= textToAnimate.length();
	}
	
	/* **************************** GlobalListener ******************************/
	@Override
	public void onGlobalEvent(GlobalEvent evt) {
		if(evt != null && evt instanceof ShowTextEvent){
			showText((ShowTextEvent)evt);
		}
	}

	public void showText(ShowTextEvent evt) {
		switch(evt.getShowTextType()){
		case Animate:
			this.show();
			this.animateText(evt.getText());
			break;
		case Show:
			this.show();
			textArea.setText(evt.getText());
			break;
		case Skip:
			this.show();
			this.skipTextAnimation();
			break;
		case Hide:
			this.hide();
			break;
		}
	}

	public void addTextAnimationListener(TextAnimationListener textAnimationListener) {
		listeners.add(textAnimationListener);
	}
	public void removeTextAnimationListener(TextAnimationListener textAnimationListener) {
		listeners.remove(textAnimationListener);
	}
}
