package com.hdv.abyss.ui;

public class Message {
	private String text;
	private long startTime;
	public Message(String text) {
		this(text, 0);
	}
	public Message(String text, long startTime) {
		this.text = text;
		this.startTime = startTime;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

}
