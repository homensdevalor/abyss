package com.hdv.abyss.ui;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.hdv.abyss.Resources;
import com.hdv.abyss.health.HealthController;
import com.hdv.abyss.health.HealthValue;
import com.hdv.abyss.health.HealthValueListener;
import com.hdv.abyss.health.event.HealthValueEvent;

public class HealthBar extends Table implements HealthValueListener{
	private HealthController health;
	private final List<Image> lifeItems;
	private TextureRegionDrawable lifeFullTexture;
	private TextureRegionDrawable lifeEmptyTexture;
	
	public HealthBar(Skin skin) {
		this();
		super.setSkin(skin);
	}
	public HealthBar() {
		super();
		lifeItems = new ArrayList<Image>();
		TextureRegion fullTextureRegion = new TextureRegion(new Texture(Gdx.files.internal(Resources.healthFullTexture)));
		TextureRegion emptyTextureRegion = new TextureRegion(new Texture(Gdx.files.internal(Resources.healthEmptyTexture)));
		lifeFullTexture = new TextureRegionDrawable(fullTextureRegion);
		lifeEmptyTexture = new TextureRegionDrawable(emptyTextureRegion);
	}

	public HealthController getHealth() {
		return health;
	}

	public void setHealth(HealthController health) {
		if(this.health != null){
			this.health.removeValueListener(this);
		}
		if(health != null){
			health.addValueListener(this);
		}
		this.health = health;
		updateLayout();
	}

	protected void updateLayout() {
		this.clear();
		if(health != null){
			HealthValue value = health.getHealthValue();
			for(int i=0; i < value.getMaxValue(); ++i){
				createLifeItem(i < value.getValue());
			}
		}
	}
	private void createLifeItem(boolean isFull) {
		Image image = new Image(isFull ? lifeFullTexture : lifeEmptyTexture);
		this.lifeItems.add(image);
		this.add(image).size(10, 10);
	}
	/* ************************************************* HealthValueListener ******************************************/
//	@Override
//	public void onHealthValueEvent(HealthValueEvent event) {
//		boolean decreasedLife = event.getActualValue() < event.getPreviousValue();
//		int itemLifeIndex = (decreasedLife ? event.getPreviousValue() : event.getActualValue()) - 1;
//		TextureRegionDrawable lifeTexture = decreasedLife ? lifeEmptyTexture : lifeFullTexture;
//
//		int startIndex = Math.min(event.getPreviousValue(), event.getActualValue());
//		int finalIndex = Math.max(event.getPreviousValue(), event.getActualValue());
//		
//		this.lifeItems.get(itemLifeIndex).setDrawable(new TextureRegionDrawable(lifeTexture));
//	}
	
	@Override
	public void onHealthValueEvent(HealthValueEvent event) {
		int currentValue = Math.max(event.getActualValue(), 0);
		for(int i=0; i< currentValue; ++i){
			lifeItems.get(i).setDrawable(lifeFullTexture);
		}
		for(int i= currentValue; i<lifeItems.size(); ++i){
			lifeItems.get(i).setDrawable(lifeEmptyTexture);
		}
	}
	
	
}
