package com.hdv.abyss.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hdv.abyss.ui.MessageBox.TextAnimationListener;
import com.hdv.abyss.utils.Counter;

public class MessageController implements TextAnimationListener{
	private static MessageController singleton;
	public static MessageController instance(){
		if (singleton == null) {
			singleton = new MessageController();
		}

		return singleton;
	}
	
	private MessageBox messageBox;
	private final List<Message> messageList;
	private int currentMessageIndex;
	private boolean showingMessage;
	private long timeBetweenMessages;
	private long defaultTimeBetweenMessages;
	private final Counter counter;
	
	public MessageController() {
		this.messageBox = null;
		messageList = new ArrayList<Message>();
		currentMessageIndex = 0;
		defaultTimeBetweenMessages = 0;
		showingMessage = false;
		counter = new Counter();
	}
	
	public MessageBox getMessageBox() {
		return messageBox;
	}
	public void setMessageBox(MessageBox messageBox) {
		if(this.messageBox != null){
			messageBox.removeTextAnimationListener(this);
		}
		this.messageBox = messageBox;
		if(messageBox != null){
			messageBox.addTextAnimationListener(this);
			messageBox.hide();
		};
	}
	public long getDefaultTimeBetweenMessages() {
		return defaultTimeBetweenMessages;
	}
	public void setDefaultTimeBetweenMessages(long defaultTimeBetweenMessages) {
		this.defaultTimeBetweenMessages = defaultTimeBetweenMessages;
	}

	public void update(float delta){
		if((!counter.isCounting() || counter.finishedCount()) && messageBox != null){
			if(hasMessageToShow() && !showingMessage){
				Message currentMessage = this.messageList.get(currentMessageIndex);
				messageBox.animateText(currentMessage.getText(), currentMessage.getStartTime());
				showingMessage = true;
			}
			else if(!hasMessageToShow() && !messageList.isEmpty()){
				messageList.clear();
				if(messageBox != null){
					messageBox.hide();
				}
			}
		}
	}

	public void showMessageSequence(Message ... messages) {
		this.showMessageSequence(Arrays.asList(messages));
	}
	public void showMessageSequence(long timeBetweenMessage, Message ... messages) {
		this.showMessageSequence(Arrays.asList(messages));
	}
	public void showMessageSequence(List<Message> messages) {
		this.showMessageSequence(defaultTimeBetweenMessages, messages);
	}
	public void showMessageSequence(long timeBetweenMessage, List<Message> messages) {
		this.timeBetweenMessages = timeBetweenMessage;
		messageList.clear();
		messageList.addAll(messages);
	}
	public boolean hasMessageToShow() {
		return !messageList.isEmpty() && currentMessageIndex < messageList.size();
	}
	public void skip(){
		if(messageBox != null){
			messageBox.skipTextAnimation();
		}
	}
	/* ****************************************** TextAnimationListener ********************************************/
	@Override
	public void onFinishAnimation() {
		++currentMessageIndex;
		showingMessage = false;
		counter.startCount(this.timeBetweenMessages);
	}
}
