package com.hdv.abyss.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ValueBar extends Actor{

	private NinePatch barBackground;
	private NinePatch barForeground;
	private float value;
	private float border;
	
	public ValueBar(NinePatch background, NinePatch foreground, float border){
		barBackground = background;
		barForeground = foreground;
		this.border = border;
	}
	
	public float getBackgroundHeight(){
		return this.getHeight() + (border * 2);
	}
	
	public float getBackgroundWidth(){
		return this.getWidth() + (border * 2);
	}
	
	public float getBorder(){
		return border;
	}
	
	public void setValue(float value){
		if(value < 0){
			this.value = 0;
		}else if (value > 100){
			this.value = 100;
		}else{
			this.value = value;
		}
	}
	
	public float getValue(){
		return this.value;
	}
	
	public void draw (Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		barBackground.draw(batch, this.getX(), this.getY(), this.getWidth() + (border * 2), this.getHeight() + (border * 2));
		float preferedWidth = getPercentageWidth(this.getWidth(), value);
		if(preferedWidth > barBackground.getLeftWidth() - border){
			barForeground.draw(batch, this.getX() + border, this.getY() + border, preferedWidth, this.getHeight());
		}
	}
	
	public float getPercentageWidth(float value, float percentage){
		return (value / 100) * percentage;
	}
	
}
