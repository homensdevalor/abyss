package com.hdv.abyss.ui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class UISimpleTest extends Game {
	boolean decreasing;
	static int hp;
	//static ProgressBar bar;
	static ValueBar health;
	static Container<ValueBar> container;
	Skin skin;
	Stage stage;
	SpriteBatch batch;
	@Override
	public void create () {
		decreasing = true;
		hp = 100;
		batch = new SpriteBatch();
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		// A skin can be loaded via JSON or defined programmatically, either is fine. Using a skin is optional but strongly
		// recommended solely for the convenience of getting a texture, region, etc as a drawable, tinted drawable, etc.
		skin = new Skin();
		// Generate a 1x1 white texture and store it in the skin named "white".
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));
		// Store the default libgdx font under the name "default".
		skin.add("default", new BitmapFont());
		// Configure a TextButtonStyle and name it "default". Skin resources are stored by type, so this doesn't overwrite the font.
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.down = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.checked = skin.newDrawable("white", Color.BLUE);
		textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
		textButtonStyle.font = skin.getFont("default");
		skin.add("default", textButtonStyle);

		//		SliderStyle sliderStyle = new SliderStyle();
		//		sliderStyle.background = skin.newDrawable("white", Color.RED);
		//		skin.add("default-horizontal", sliderStyle);

		container = new Container<ValueBar>();
		container.setFillParent(true);
		stage.addActor(container);
		
		ProgressBarStyle pStyle = new ProgressBarStyle();
		pStyle.background = skin.newDrawable("white");
		pStyle.knob = skin.newDrawable("white", Color.BLUE);
		pStyle.knobBefore = pStyle.knob;
		
		skin.add("default-horizontal", pStyle);

		// Create a table that fills the screen. Everything else will go inside this table.
//		Table table = new Table();
//		table.debug();
//		table.top().left();
//		table.setFillParent(true);
//		stage.addActor(table);
		// Create a button with the "default" TextButtonStyle. A 3rd parameter can be used to specify a name other than "default".
//		final TextButton button = new TextButton("Click me!", skin);
//		table.add(button).left();

		// Add an image actor. Have to set the size, else it would be the size of the drawable (which is the 1x1 texture).

		//hpCell = table.add(new Slider(0, 100, 1, false, skin)).expandX().height(64);
		//hpCell.getActor().setValue(hp);

		NinePatch bg = new NinePatch(new Texture(Gdx.files.internal("ui/bar-border.png")), 8, 8, 8, 8);
		NinePatch fg = new NinePatch(new Texture(Gdx.files.internal("ui/bar-fill.png")), 2, 2, 2, 2);
		
		health = new ValueBar(bg, fg, 6);
		health.setWidth(25);
		health.setHeight(2);
		health.setValue(100);
//		bar = new ProgressBar(0, 100, 1, false, skin);
//		bar.setHeight(64);
		
		container.setActor(health);
		container.width(health.getBackgroundWidth());
		container.height(health.getBackgroundHeight());
		container.left().top();
		container.padTop(16);
		container.padLeft(8);

		//		hpCell = table.add(new Image(skin.newDrawable("white", Color.RED)));
		//		hpCell.minWidth(0);
		//		hpCell.width(32).height(64).expandX();


		// Add a listener to the button. ChangeListener is fired when the button's checked state changes, eg when clicked,
		// Button#setChecked() is called, via a key press, etc. If the event.cancel() is called, the checked state will be reverted.
		// ClickListener could have been used, but would only fire when clicked. Also, canceling a ClickListener event won't
		// revert the checked state.
//		button.addListener(new ChangeListener() {
//			public void changed (ChangeEvent event, Actor actor) {
//				float width = bar.getValue();
//				System.out.println(width);
//				if(width > 0){
//					bar.setValue(width - 3);
//				}
//
//				System.out.println("Clicked! Is checked: " + button.isChecked());
//				button.setText("Good job!");
//			}
//		});
	}
	@Override
	public void render () {
		ValueBar pBar = container.getActor();
		float value = pBar.getValue();
		
		if(decreasing){
			pBar.setValue(value - 1);
		}else{
			pBar.setValue(value + 1);
		}
		
		if(value < 1){
			decreasing = false;
		}else if(value > 99){
			decreasing = true;
		}
		
		
		
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}
	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}
	@Override
	public void dispose () {
		stage.dispose();
		skin.dispose();
	}
}
