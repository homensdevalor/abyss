package com.hdv.abyss.prototype;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class PrototypeGame extends Game implements Screen{
    protected Stage stage;
	
	protected abstract Stage createStage();
	
	@Override
    public void create () {
		stage = createStage();
        setScreen(this);
    }
	
	@Override
    public void render (float delta) {
		stage.act();
		
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.draw();
    }

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
	}
	@Override
	public void resume() {
	}
	@Override
	public void pause() {
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
}
