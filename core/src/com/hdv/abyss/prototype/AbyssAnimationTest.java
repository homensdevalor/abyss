package com.hdv.abyss.prototype;

import java.util.HashMap;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.hdv.abyss.anim.AnimationParser;

public class AbyssAnimationTest extends ApplicationAdapter {
	SpriteBatch batch;
	HashMap<String, Animation> animations;
	
	String currentAnimation;
	TextureRegion currentFrame;
	float stateTime;
	
	int xAxis;
	int yAxis;
	
	boolean movingX;
	boolean movingY;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		animations = new HashMap<String, Animation>();
		
		AnimationParser dragon = new AnimationParser("dragon.gif", 10, 8);
		int dragonFramerate = 20;
		animations.put("dragon-right", dragon.getAnimation(1, 9, dragonFramerate));
		animations.put("dragon-right-down", dragon.getAnimation(11, 20, dragonFramerate));
		animations.put("dragon-down", dragon.getAnimation(21, 30, dragonFramerate));
		animations.put("dragon-left-down", dragon.getAnimation(31, 40, dragonFramerate));
		animations.put("dragon-left", dragon.getAnimation(41, 50, dragonFramerate));
		animations.put("dragon-left-up", dragon.getAnimation(51, 60, dragonFramerate));
		animations.put("dragon-up", dragon.getAnimation(61, 70, dragonFramerate));
		animations.put("dragon-right-up", dragon.getAnimation(71, 80, dragonFramerate));
		
		currentAnimation = "dragon-down";
		
		xAxis = 0;
		yAxis = 0;
		
		stateTime = 0f;
	}

	@Override
	public void render () {
		movingX = false;
		movingY = false;
		
		System.out.println(stateTime);
		stateTime += Gdx.graphics.getDeltaTime();
		currentAnimation = this.getCurrentAnimation();
        currentFrame = animations.get(currentAnimation).getKeyFrame(stateTime, true);
		
        if(Gdx.input.isKeyPressed(Keys.LEFT)){
        	xAxis = -1;
        	movingX = true;
        }
        if(Gdx.input.isKeyPressed(Keys.RIGHT)){
        	xAxis = 1;
        	movingX = true;
        }
        if(Gdx.input.isKeyPressed(Keys.UP)){
        	yAxis = 1;
        	movingY = true;
        }
        if(Gdx.input.isKeyPressed(Keys.DOWN)){
        	yAxis = -1;
        	movingY = true;
        }
        
        if(!movingX){
        	xAxis = 0;
        }
        
        if(!movingY){
        	yAxis = 0;
        }
        
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		
		Sprite sprite = new Sprite(currentFrame);
		sprite.setCenter(90, 90);
		sprite.setSize(90, 90);
		sprite.draw(batch);
		
		//batch.draw(currentFrame, 90, 90);
		
		batch.end();
		
		if(stateTime > 10f){
			stateTime = stateTime - 10f;
		}
	}
	
	private String getCurrentAnimation(){
		// Direita e em cima
		if(xAxis == 0 && yAxis == 0){
			return currentAnimation;
		}
		
		if(xAxis == 1 && yAxis == 0){
			return "dragon-right";
		}
		
		if(xAxis == -1 && yAxis == 0){
			return "dragon-left";
		}
		
		if(xAxis == 0 && yAxis == -1){
			return "dragon-down";
		}
		
		if(xAxis == 1 && yAxis == -1){
			return "dragon-right-down";
		}
		
		if(xAxis == -1 && yAxis == -1){
			return "dragon-left-down";
		}
		
		if(xAxis == 0 && yAxis == 1){
			return "dragon-up";
		}
		
		if(xAxis == 1 && yAxis == 1){
			return "dragon-right-up";
		}
		
		if(xAxis == -1 && yAxis == 1){
			return "dragon-left-up";
		}
		
		return currentAnimation;
	}
}
