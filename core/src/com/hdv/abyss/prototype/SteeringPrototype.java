package com.hdv.abyss.prototype;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.steer.behaviors.Arrive;
import com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance;
import com.badlogic.gdx.ai.steer.behaviors.PrioritySteering;
import com.badlogic.gdx.ai.steer.utils.paths.LinePath;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.animations.MainCharacterAnimationMapBuilder;
import com.hdv.abyss.actors.configurators.SlimeBuilderConfigurator;
import com.hdv.abyss.actors.entities.Collider;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.ai.BehaviourController;
import com.hdv.abyss.ai.GameCharacterSteerable;
import com.hdv.abyss.ai.steering.CollisionAvoidanceBehavior;
import com.hdv.abyss.ai.steering.PathFollowerBehavior;
import com.hdv.abyss.ai.steering.ProximityFieldOfView;
import com.hdv.abyss.ai.steering.SteerableUtils;
import com.hdv.abyss.ai.steering.TolerantArrive;
import com.hdv.abyss.input.CharacterInputProcessor;
import com.hdv.abyss.physics.PhysicalWorld;

public class SteeringPrototype extends PrototypeGame{

    static final int VIEWPORT_WIDTH = 20;
    static final int VIEWPORT_HEIGHT = 15;
    
    public class SteeringStage extends Stage{
		static final float PLAYER_FAST_VELOCITY = 12f;
		static final float PLAYER_NORMAL_VELOCITY = 7f;
		private PhysicalWorld world;
		GameCharacter character;
		GameCharacter target;
		GameCharacter wanderer;
		GameCharacterSteerable wandererSteerable;
//		Box2dSteeringEntity wandererSteerable;

	    Box2DDebugRenderer renderer;	    
		private InputMultiplexer inputProcessorMultiplexer;
		BehaviourController behaviourController;		
		
		
		public SteeringStage() {
	    	super(new FitViewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT
	    			, new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));

	        renderer = new Box2DDebugRenderer(); 
	        
			world = new PhysicalWorld();
			world.setActorsContainer(this);
//			character = createCharacter(new Vector2(VIEWPORT_WIDTH/2, VIEWPORT_HEIGHT * 3/4f));

			character = new GameCharacterBuilder(new SlimeBuilderConfigurator()).build();
			world.addActor(character, new Vector2(VIEWPORT_WIDTH/2, VIEWPORT_HEIGHT * 3/4f));
			target = createTarget();
			wanderer = createWanderer();
			
			setupCollider();
			
			inputProcessorMultiplexer = new InputMultiplexer();
	        if(Gdx.input != null){
	        	Gdx.input.setInputProcessor(inputProcessorMultiplexer);
	        }
			inputProcessorMultiplexer.addProcessor(new CharacterInputProcessor(target));
			behaviourController = new BehaviourController(character, target);
		}
		
		private void setupCollider() {
			Vector2 positions[] = new Vector2[]{
					new Vector2(VIEWPORT_WIDTH/2f,VIEWPORT_HEIGHT/2f)
					  , new Vector2(VIEWPORT_WIDTH/2f + 5 ,VIEWPORT_HEIGHT/2f + 2f)
					  , new Vector2(VIEWPORT_WIDTH/2f - 10, VIEWPORT_HEIGHT * 0.73f)
					  , new Vector2(3					  , VIEWPORT_HEIGHT * 0.4f)
					  , new Vector2(VIEWPORT_WIDTH *0.85f,9f)
					  , new Vector2(VIEWPORT_WIDTH * 0.25f,VIEWPORT_HEIGHT * 0.1f)
			};
			List<Collider> colliders = new ArrayList<Collider>();
			for(int i=0; i < positions.length; ++i){
				colliders.add(new Collider(positions[i], new Vector2(1,1)));
			}
			for(Collider collider : colliders){
				world.addActor(collider);
			}
		}

		LinePath<Vector2> path;
		SteeringBehavior<Vector2> followPath;
		SteeringBehavior<Vector2> collisionAvoidanceSB;
		Arrive<Vector2> arrive;
		private GameCharacter createWanderer() {
			Vector2 startPosition = new Vector2(VIEWPORT_WIDTH * 3/4f, VIEWPORT_HEIGHT * 3/4f);
//			GameCharacter slime = createCharacter(startPosition);
			GameCharacter slime = new GameCharacterBuilder(new SlimeBuilderConfigurator()).build();
			world.addActor(slime, startPosition);
			GameCharacterSteerable slimeSteerable = new GameCharacterSteerable(slime);
			slime.setUserObject(slimeSteerable);

			Proximity<Vector2> proximity = new ProximityFieldOfView(slimeSteerable, 
							world.getWorld(), slimeSteerable.getBoundingRadius()*2,MathUtils.degreesToRadians * 90);

			collisionAvoidanceSB = new CollisionAvoidanceBehavior<Vector2>(slimeSteerable, proximity);

			setupFollowPathBehavior(startPosition, slimeSteerable, proximity);
			
			arrive = new Arrive<Vector2>(slimeSteerable,SteerableUtils.getSteerable(target));
			
			slimeSteerable.setMaxLinearSpeed(4);
			
			PrioritySteering<Vector2> prioritySteeringSB = new PrioritySteering<Vector2>(
					slimeSteerable, 0.0001f);
			prioritySteeringSB.add(new CollisionAvoidance<Vector2>(slimeSteerable, proximity));
//			prioritySteeringSB.add(new LinearPathWander<Vector2>(slimeSteerable));
//			prioritySteeringSB.add(new Arrive<Vector2>(slimeSteerable,(Steerable<Vector2>) target.getUserObject()));
			prioritySteeringSB.add(new TolerantArrive<Vector2>(
					slimeSteerable, SteerableUtils.getSteerable(target), 2f, MathUtils.PI*0.8f) );

			slimeSteerable.setSteeringBehavior(prioritySteeringSB);
			wandererSteerable = slimeSteerable;
			
			return slime;
		}

		private void setupFollowPathBehavior(Vector2 startPosition,
				GameCharacterSteerable slimeSteerable,
				Proximity<Vector2> proximity) {
			Array<Vector2> pathPoints = new Array<Vector2>();
			pathPoints.add(startPosition);
			pathPoints.add(startPosition.cpy().add(0, -5));
			pathPoints.add(startPosition.cpy().add(-5, -5));
			pathPoints.add(startPosition.cpy().add(-5, 0));
			
			for(Vector2 pathPoint : pathPoints){
				Gdx.app.log(getClass().getName(), "Point: " + pathPoint);
			}
			
			path = new LinePath<Vector2>(pathPoints,true);

			
			PathFollowerBehavior< Vector2, LinePath.LinePathParam> pathFollowBehaviour 
				= new PathFollowerBehavior<Vector2, LinePath.LinePathParam>(proximity, slimeSteerable, path,1,0.3f);		
			pathFollowBehaviour.setArrivalTolerance(1.2f);
			pathFollowBehaviour.setArriveEnabled(true);
			pathFollowBehaviour.setDecelerationRadius(3).setArrivalTolerance(1);
			this.followPath = pathFollowBehaviour;
		}
		private GameCharacter createTarget() {
			GameCharacter gameCharacter = new GameCharacter(new MainCharacterAnimationMapBuilder().buildMap());
			gameCharacter.getPhysicalProperties().setStartPosition(new Vector2(VIEWPORT_WIDTH/2, VIEWPORT_HEIGHT/2));
			gameCharacter.getPhysicalProperties().setSize(new Vector2(1,1));
			

			gameCharacter.setNormalVelocity(PLAYER_NORMAL_VELOCITY);
			gameCharacter.setFastVelocity(PLAYER_FAST_VELOCITY);

			gameCharacter.setUserObject(new GameCharacterSteerable(gameCharacter));
			
			world.addActor(gameCharacter);
			return gameCharacter;
		}
		@Override
		public void act(float delta) {
			super.act(delta);
			world.update(delta);
			behaviourController.update(delta);
			wandererSteerable.update(delta);
		}
		@Override
		public void draw() {
			super.draw();

	        renderer.render(world.getWorld(), getViewport().getCamera().combined);
		}
	}

	@Override
	protected Stage createStage() {
		return new SteeringStage();
	}
}
