package com.hdv.abyss.prototype;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.scenes.GameStage;

public class CarregandoMapTiled extends Game implements InputProcessor {
	public class TileScreen implements Screen {

		private static final String CHARACTERS_LAYER_NAME = "personagens";
		private static final String COLLISION_LAYER_NAME = "Colisões";
        OrthographicCamera camera;
        TiledMap tiledMap;
        OrthogonalTiledMapRenderer tiledMapRenderer;

        GameStage stage;

        int[] backgroundLayers;
        int[] foregroundLayers;
        
        public TileScreen() {
            camera = new OrthographicCamera(20,15);
            camera.update();

            tiledMap = new TmxMapLoader(new InternalFileHandleResolver()).load("maps/mapa1.tmx");
            float unitScale = 1f/ Configurations.TILE_IN_PIXELS;
            tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, unitScale);
            
            setupLayers();
            stage = new GameStage();
            setupMapColliders();
//            tracker = new CameraTracker(camera, stage.player1);
		}
        
        private void setupMapColliders() {
        	List<Rectangle> colliders = new ArrayList<Rectangle>();
        	Iterator<MapObject> mapObjectsIterator = tiledMap.getLayers().get("Colisões").getObjects().iterator();
        	while(mapObjectsIterator.hasNext()){
        		MapObject object = mapObjectsIterator.next();
        		Gdx.app.log("Colision object = ", "" + object);
        		
        		if(object instanceof RectangleMapObject){
        			RectangleMapObject rectObj = (RectangleMapObject)object;
        			colliders.add(rectObj.getRectangle());
        		}
        	}
//        	stage.setMapCollidersInPixels(colliders);
		}

		private void setupLayers() {
        	List<Integer> backgroundLayers = new ArrayList<Integer>(),
        			      foregroundLayers = new ArrayList<Integer>();
        	
        	
        	boolean foundCharacterLayer = false;
        	Iterator<MapLayer> layersIterator = tiledMap.getLayers().iterator();
        	int index = 0;
        	while(layersIterator.hasNext()){
        		String layerName = layersIterator.next().getName();
				Gdx.app.log(getClass().getName(), "Layer = " + layerName);
        		if(!layerName.equals(COLLISION_LAYER_NAME)){
	        		if(!foundCharacterLayer){
	        			if(!layerName.equals(CHARACTERS_LAYER_NAME)){
	        				Gdx.app.log(getClass().getName(), "Adding layer to background = " + layerName);
	        				backgroundLayers.add(index);
	        			}
	        			else{
	        				foundCharacterLayer = true;
	        			}
	        		}
	        		else{
        				Gdx.app.log(getClass().getName(), "Adding layer to foreground = " + layerName);
	        			foregroundLayers.add(index);
	        		}
        		}
        		++index;
        	}

        	this.backgroundLayers = new int[backgroundLayers.size()];
        	for(int i=0; i < backgroundLayers.size(); ++i){
        		this.backgroundLayers[i] = backgroundLayers.get(i);
        	}
        	this.foregroundLayers = new int[foregroundLayers.size()];
        	for(int i=0; i < foregroundLayers.size(); ++i){
        		this.foregroundLayers[i] = foregroundLayers.get(i);
        	}
		}

		@Override
        public void render (float delta) {
        	checkKeys();
        	
            Gdx.gl.glClearColor(1, 0, 0, 1);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//            camera.position.set(stage.player1.getX() ,stage.player1.getY(), camera.position.z);
            camera.update();
            tiledMapRenderer.setView(camera);
            
//            tiledMapRenderer.render();

            this.stage.act(delta);
            
            tiledMapRenderer.render(backgroundLayers);
            this.stage.draw();
            tiledMapRenderer.render(foregroundLayers);
            
//            tracker.act(delta);
        }

        private void checkKeys() {
    		// TODO Auto-generated method stub

//            if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
//                camera.translate(-MOVE_AMOUNT,0);
//            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
//                camera.translate(MOVE_AMOUNT,0);
//            if(Gdx.input.isKeyPressed(Input.Keys.UP))
//                camera.translate(0,MOVE_AMOUNT);
//            if(Gdx.input.isKeyPressed(Input.Keys.DOWN))
//                camera.translate(0,-MOVE_AMOUNT);
//            if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
//                Gdx.app.log(getClass().getName(), "" + stage.player1.getPositionInWorld());
    	}

		@Override
		public void resize(int width, int height) {
			stage.getViewport().update(width, height);
		}

		@Override
		public void show() {
			// TODO Auto-generated method stub

		}

		@Override
		public void hide() {
			// TODO Auto-generated method stub

		}

		@Override
		public void pause() {
			// TODO Auto-generated method stub

		}

		@Override
		public void resume() {
			// TODO Auto-generated method stub

		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub

		}

	}
    
    @Override
    public void create () {
        
        setScreen(new TileScreen());
//        camera.position.x = 50;
    }

	@Override
    public boolean keyDown(int keycode) {
        return false;
    }

    
    
    @Override
    public boolean keyUp(int keycode) {
    	/*if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            camera.zoom += 0.2;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            camera.zoom -= 0.2;
        }*/
//        if(keycode == Input.Keys.NUM_1)
//            tiledMap.getLayers().get(0).setVisible(!tiledMap.getLayers().get(0).isVisible());
//        if(keycode == Input.Keys.NUM_2)
//            tiledMap.getLayers().get(1).setVisible(!tiledMap.getLayers().get(1).isVisible());
        return false;
    }

    @Override
    public boolean keyTyped(char character) {

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
