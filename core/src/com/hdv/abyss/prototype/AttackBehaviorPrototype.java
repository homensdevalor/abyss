package com.hdv.abyss.prototype;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.configurators.PlayerBuilderConfigurator;
import com.hdv.abyss.actors.configurators.SlimeBuilderConfigurator;
import com.hdv.abyss.actors.entities.Collider;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.ai.BehaviourController;
import com.hdv.abyss.ai.GameCharacterSteerable;
import com.hdv.abyss.ai.controlstates.SimpleFightBehaviourStateBuilder;
import com.hdv.abyss.input.CharacterInputProcessor;
import com.hdv.abyss.physics.PhysicalWorld;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.physics.collision.CollisionManager.CollisionListener;

public class AttackBehaviorPrototype extends PrototypeGame{
	static final int VIEWPORT_WIDTH = 20;
    static final int VIEWPORT_HEIGHT = 15;
    
    public class SteeringStage extends Stage{
		private PhysicalWorld world;
		GameCharacter iaCharacter;
		GameCharacter player;
		GameCharacterSteerable iaSteerable;
		
//		PositioningAttackState positioningAttackState;
//		Transition reachAttackPosTransition;
		
	    Box2DDebugRenderer renderer;	    
		private InputMultiplexer inputProcessorMultiplexer;
		BehaviourController behaviourController;		
		
	    
		public SteeringStage() {
	    	super(new FitViewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT
	    			, new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));

	        renderer = new Box2DDebugRenderer(); 
	        
			world = new PhysicalWorld();
			world.setActorsContainer(this);

			setupCharacters();
			
			iaSteerable = new GameCharacterSteerable(iaCharacter);
			iaCharacter.setUserObject(iaSteerable);
//			posAttackSteering = new PositioningAttackSteering(iaCharacter, player);
//			iaSteerable.setSteeringBehavior(posAttackSteering);
			
//			positioningAttackState = new PositioningAttackState(iaCharacter.getStateMachine(), player);
//			positioningAttackState.onStart(null);
//			reachAttackPosTransition = new AttackDistanceTransition("asdasd", iaCharacter, player);
			
			
			
			setupColliders();
			
			inputProcessorMultiplexer = new InputMultiplexer();
	        if(Gdx.input != null){
	        	Gdx.input.setInputProcessor(inputProcessorMultiplexer);
	        }
			inputProcessorMultiplexer.addProcessor(new CharacterInputProcessor(player));
			behaviourController = new BehaviourController(iaCharacter, player, new SimpleFightBehaviourStateBuilder(player));
		
			world.getCollisionManager().addCollisionListener(new CollisionListener() {
				
				@Override
				public void onCollision(CollisionEvent evt) {
					behaviourController.onEvent(iaCharacter, evt);
				}
			}, iaCharacter);
		}

		private void setupCharacters() {
			iaCharacter = new GameCharacterBuilder(new SlimeBuilderConfigurator()).build();
			world.addActor(iaCharacter, new Vector2(VIEWPORT_WIDTH/2, VIEWPORT_HEIGHT * 3/4f));
			player = new GameCharacterBuilder(new PlayerBuilderConfigurator()).build();
			player.getHealthValue().setMaxValue(100);
			player.getHealthValue().setValue(100);
			System.out.println("MaxLife: " + player.getHealthValue().getMaxValue());
			System.out.println("Life: " + player.getHealthValue().getValue());
			world.addActor(player, new Vector2(VIEWPORT_WIDTH/2, VIEWPORT_HEIGHT/2));
		}
		
		private void setupColliders() {
			Vector2 positions[] = new Vector2[]{
					new Vector2(VIEWPORT_WIDTH/2f,VIEWPORT_HEIGHT/2f)
					  , new Vector2(VIEWPORT_WIDTH/2f + 5 ,VIEWPORT_HEIGHT/2f + 2f)
					  , new Vector2(VIEWPORT_WIDTH/2f - 10, VIEWPORT_HEIGHT * 0.73f)
					  , new Vector2(3					  , VIEWPORT_HEIGHT * 0.4f)
					  , new Vector2(VIEWPORT_WIDTH *0.85f,9f)
					  , new Vector2(VIEWPORT_WIDTH * 0.25f,VIEWPORT_HEIGHT * 0.1f)
			};
			List<Collider> colliders = new ArrayList<Collider>();
			for(int i=0; i < positions.length; ++i){
				colliders.add(new Collider(positions[i], new Vector2(1,1)));
			}
			for(Collider collider : colliders){
				world.addActor(collider);
			}
		}
		
		boolean attacking = false;

		@Override
		public void act(float delta) {
			super.act(delta);
			world.update(delta);
			behaviourController.update(delta);
		}

		@Override
		public void draw() {
			super.draw();

	        renderer.render(world.getWorld(), getViewport().getCamera().combined);
		}
	}

	@Override
	protected Stage createStage() {
		return new SteeringStage();
	}
}
