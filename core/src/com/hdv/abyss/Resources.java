package com.hdv.abyss;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class Resources {
	private static Resources singleton;
	public static Resources instance() {
		if (singleton == null) {
			singleton = new Resources();
		}
		return singleton;
	}

	/* ************************** Resources ******************************/
	public static final String dialogueFont = "fonts/Metamorphous-Regular.ttf";
	public static final String gameSkin = "ui/uiskin.json";
	public static final String messageBoxStyle = "transparentTextField";
	public static final int dialogueFontSize = 18;
	public static final String messageBoxBackground = "background/caixa_de_texto.png";
	public static final String titleBackground = "background/menu_abyss.png";
//	public static final String titleBackground = "background/title.png";
	public static final String healthFullTexture = "objects/health_full.png";
	public static final String healthEmptyTexture = "objects/health_empty.png";
	public static final String spikeAttackSprite = "sprites/espinhos/espinhos.png";
	public static final String spikeStillSprite = "sprites/espinhos/espinhos-off.png";
	public static final String victoryTexture = "background/congratulations_background.png";
	public static final String titleFont = "fonts/Metamorphous-Regular.ttf";
	public static final int congratulationsTitleFontSize = 30;
	public static final int congratulationsFontSize = 23;
	public static final String congratulationsMusic = "musics/Cataclysm.ogg";

	/* *******************************************************************/
	
	public BitmapFont generateFont(String fontResource, int fontSize) {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(fontResource));
		
		FreeTypeFontParameter fontParam = new FreeTypeFontParameter();
		fontParam.size = fontSize;
		return generator.generateFont(fontParam);
	}

}
