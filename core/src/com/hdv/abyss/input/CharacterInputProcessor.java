package com.hdv.abyss.input;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.ChangedStateEvent;
import com.hdv.abyss.statemachine.OnStateChangeListener;
import com.hdv.abyss.statemachine.events.AttackEvent;
import com.hdv.abyss.statemachine.events.ChangeItemCommandEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.events.MovementEvent;

public class CharacterInputProcessor extends InputAdapter implements OnStateChangeListener {
		//ACTIONS
		public static final String UP     = "UP";
		public static final String DOWN   = "DOWN";
		public static final String LEFT   = "LEFT";
		public static final String RIGHT  = "RIGHT";
		public static final String ATTACK = "ATTACK";
		public static final String DODGE  = "DODGE";
		public static final String CHANGE_ITEM  = "CHANGE_ITEM";
		
		//DEFAULT_KEYS	
		private static final int DEFAULT_UP_KEY = Keys.UP;
		private static final int DEFAULT_DOWN_KEY = Keys.DOWN;
		private static final int DEFAULT_LEFT_KEY = Keys.LEFT;
		private static final int DEFAULT_RIGHT_KEY = Keys.RIGHT;
		private static final int DEFAULT_ATTACK_KEY = Keys.X;
		private static final int DEFAULT_DODGE_KEY = Keys.Z;
		private static final int DEFAULT_CHANGEITEM_KEY = Keys.C;

		private GameCharacter player;
		private boolean enabled;

		private int moveUpKey, 
					moveDownKey, 
					moveRightKey,
					moveLeftKey,
					attackKey, 
					dodgeKey,
					changeItemKey;

		public CharacterInputProcessor(GameCharacter player){
			this(player, new HashMap<String, Integer>());
		}
		public CharacterInputProcessor(GameCharacter player, Map<String, Integer> commandsToKeys){
			if(player == null){
				throw new IllegalArgumentException("Argument of type " + GameCharacter.class.getName() + " should not be null!");
			}
			this.player = player;
			player.addOnStateChangeListener(this);
			
			setKeyMap(commandsToKeys);
			
			enabled = true;
		}

		public boolean isEnabled() {
			return enabled;
		}
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
		public void setKeyMap(Map<String, Integer> commandsToKeys) {
			this.moveUpKey    = getKey(UP    , commandsToKeys, DEFAULT_UP_KEY);
			this.moveDownKey  = getKey(DOWN  , commandsToKeys, DEFAULT_DOWN_KEY);
			this.moveLeftKey  = getKey(LEFT  , commandsToKeys, DEFAULT_LEFT_KEY);
			this.moveRightKey = getKey(RIGHT , commandsToKeys, DEFAULT_RIGHT_KEY);
			this.attackKey 	  = getKey(ATTACK, commandsToKeys, DEFAULT_ATTACK_KEY);
			this.dodgeKey 	  = getKey(DODGE , commandsToKeys, DEFAULT_DODGE_KEY);
			this.changeItemKey = getKey(CHANGE_ITEM, commandsToKeys, DEFAULT_CHANGEITEM_KEY);
		}
		private int getKey(String command, Map<String, Integer> commandsToKeys, int defaultKey) {
			Integer keyValue = commandsToKeys.get(command);
			return keyValue != null ? keyValue : defaultKey;
		}
		@Override
		public boolean keyDown(int keycode) {
			receiveKeyEvent(keycode, true);
			return false;
		}
		@Override
		public boolean keyUp(int keycode) {
			receiveKeyEvent(keycode, false);
			return false;
		}
		private void receiveKeyEvent(int keycode, boolean keyDown) {
			if(!isEnabled()){
				return;
			}
			if(keycode == attackKey){
				attack(keyDown);
			}
			else if(isIn(keycode
					, moveDownKey,moveLeftKey, moveRightKey, moveUpKey, dodgeKey))
			{
				updateMovement(true);
			}
			else if(keycode == changeItemKey && !keyDown){
				changeItem();
			}
		}

		private void changeItem() {
			player.fireEvent(new ChangeItemCommandEvent());
		}
		private boolean isIn(int keycode, int ... keys) {
			for(int key : keys){
				if(keycode == key){
					return true;
				}
			}
			return false;
		}
		private void attack(boolean keyDown) {
			if(keyDown == true){
				player.fireEvent(new AttackEvent());
			}
		}
		private void updateMovement(boolean allowDodge) {
			Vector2 direction = getMoveDirection();
			
			Movement moveType = (Gdx.input.isKeyPressed(dodgeKey) && allowDodge ? Movement.DODGE : Movement.NORMAL);
			direction = direction.isZero() && moveType == Movement.DODGE ? player.getDirection().toVector() : direction;
			player.fireEvent(new MovementEvent(direction, moveType));
		}
		private Vector2 getMoveDirection() {
			Vector2 direction = new Vector2();
			direction.x = getAxisDirection(moveRightKey, moveLeftKey);
			direction.y = getAxisDirection(moveUpKey, moveDownKey);
			return direction;
		}
		private float getAxisDirection(int keyPositive, int keyNegative) {
			if(Gdx.input.isKeyPressed(keyPositive)){
				return 1f;
			}
			else if(Gdx.input.isKeyPressed(keyNegative)){
				return -1f;
			}
			return 0f;
		}
		@Override
		public void onStateChange(ChangedStateEvent<?> changedEvent) {
			if(!isEnabled()){
				return;
			}
			/* Caso um personagem tenha mudado de estado e uma tecla direcional esteja sendo pressionada,
			 * ele poderá não ter recebido o evento de movimentação, por isso, sempre que ele mudar de
			 * estado dispara evento novamente*/
			updateMovement(false);
		}
	}