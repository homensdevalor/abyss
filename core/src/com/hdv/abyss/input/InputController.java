package com.hdv.abyss.input;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;

public class InputController {
	private static InputController singleton;
	public static InputController instance(){
		if (singleton == null) {
			singleton = new InputController();
		}
		return singleton;
	}
	
	private final InputMultiplexer inputMultiplexer;

	public InputController() {
		inputMultiplexer = new InputMultiplexer();
	}
	
	public InputProcessor getInputProcessor(){
		return this.inputMultiplexer;
	}
	
	public void addInputListener(InputProcessor input){
		inputMultiplexer.addProcessor(input);
	}
	public void removeInputListener(InputProcessor input){
		inputMultiplexer.removeProcessor(input);
	}
	
	
}
