package com.hdv.abyss.input;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.hdv.abyss.global.GlobalEventCollector;
import com.hdv.abyss.global.events.PauseEvent;
import com.hdv.abyss.global.events.PauseEvent.PauseAction;

public class GlobalInputProcessor extends InputAdapter{
	//ACTIONS
	public static final String PAUSE  = "PAUSE";
	//DEFAULT_KEYS
	private static final Integer DEFAULT_PAUSE_KEY = Keys.ENTER;
	
	private int pauseKey;

	public GlobalInputProcessor(){
		this(new HashMap<String, Integer>());
	}
	public GlobalInputProcessor(Map<String, Integer> commandsToKeys){		
		setKeyMap(commandsToKeys);
	}
	public void setKeyMap(Map<String, Integer> commandsToKeys) {
		this.pauseKey 	  = getKey(PAUSE , commandsToKeys, DEFAULT_PAUSE_KEY);
	}
	private int getKey(String command, Map<String, Integer> commandsToKeys, int defaultKey) {
		Integer keyValue = commandsToKeys.get(command);
		return keyValue != null ? keyValue : defaultKey;
	}
	@Override
	public boolean keyDown(int keycode) {
		return receiveKeyEvent(keycode, true);
	}
	@Override
	public boolean keyUp(int keycode) {
		return receiveKeyEvent(keycode, false);
	}
	private boolean receiveKeyEvent(int keycode, boolean keyDown) {
		if(keycode == pauseKey && !keyDown){
			pauseGame();
			return true;
		}
		return false;
	}

	private void pauseGame() {
		GlobalEventCollector.getInstance().fireEvent(new PauseEvent(PauseAction.Toggle));
	}
}
