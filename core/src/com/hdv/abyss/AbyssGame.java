package com.hdv.abyss;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.hdv.abyss.input.InputController;
import com.hdv.abyss.scenes.BaseScreen;
import com.hdv.abyss.scenes.GameScreen;
import com.hdv.abyss.scenes.ScreenController;
import com.hdv.abyss.scenes.StartScreen;
import com.hdv.abyss.scenes.VictoryScreen;

public class AbyssGame extends Game implements ScreenController{
	public enum GameScreens{MainMenu, GameScreen, Victory};
	final Map<Object, Screen> screens;
	BaseScreen currentScreen;
	
	public AbyssGame() {
		screens = new HashMap<Object, Screen>();
	}
	
	@Override
	public void create() {
		setupScreens();
		Gdx.input.setInputProcessor(InputController.instance().getInputProcessor());
		changeToScreen(GameScreens.MainMenu);
//		changeToScreen(GameScreens.GameScreen);
//		changeToScreen(GameScreens.Victory);
	}

	private void setupScreens() {
		screens.put(GameScreens.MainMenu, new StartScreen(this));
		screens.put(GameScreens.GameScreen, new GameScreen(this));
		screens.put(GameScreens.Victory, new VictoryScreen(this));
	}

	@Override
	public Screen getCurrentScreen() {
		return currentScreen;
	}

	@Override
	public void changeToScreen(Object screenKey) {
		BaseScreen nextScreen = (BaseScreen) screens.get(screenKey);
		if(nextScreen != null){
			if(currentScreen != null){
				currentScreen.pause();
			}
			currentScreen = nextScreen;
			if(!currentScreen.isCreated()){
				currentScreen.onCreate();
			}
			currentScreen.resume();
			
			setScreen(currentScreen);
		}
	}
}
