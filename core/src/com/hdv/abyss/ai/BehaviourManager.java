package com.hdv.abyss.ai;

import java.util.Collection;
import java.util.HashSet;

public class BehaviourManager {
	private static BehaviourManager singleton;
	public static BehaviourManager instance() {
		if (singleton == null) {
			singleton = new BehaviourManager();
		}
		return singleton;
	}
	
	private final Collection<BehaviourController> behaviourControllers;
	private boolean paused;
	
	public BehaviourManager() {
		behaviourControllers = new HashSet<BehaviourController>();
		paused = false;
	}

	public void addBehaviourController(BehaviourController controller){
		this.behaviourControllers.add(controller);
	}
	public void removeBehaviourController(BehaviourController controller){
		this.behaviourControllers.remove(controller);
	}
	
	public void update(float delta){
		if(!isPaused()){
			for(BehaviourController controller : behaviourControllers){
				controller.update(null, delta);
			}
		}
	}

	public boolean isPaused(){
		return paused;
	}
	public void setPaused(boolean paused) {
		this.paused = paused;
	}
}
