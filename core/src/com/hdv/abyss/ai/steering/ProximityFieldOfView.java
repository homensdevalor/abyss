package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

public class ProximityFieldOfView extends Box2dFieldOfViewProximity{

	
	public ProximityFieldOfView(Steerable<Vector2> owner, World world,
			float detectionRadius, float angle) {
		super(owner, world, detectionRadius, angle);
	}

	@Override
	protected Steerable<Vector2> getSteerable(Fixture fixture) {
		return SteerableUtils.getSteerable(fixture);
	}
}