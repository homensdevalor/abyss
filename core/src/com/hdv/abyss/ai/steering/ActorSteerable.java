package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.hdv.abyss.ai.BaseSteerable;
import com.hdv.abyss.utils.Actors;

public class ActorSteerable extends BaseSteerable {

	Actor actor;
	private Vector2 velocity;
	
	public ActorSteerable(Actor actor) {
		this.actor = actor;
		this.velocity = new Vector2();
		
		setUserData(actor);
	}

	
	@Override
	protected void applySteering(SteeringAcceleration<Vector2> steering,
			float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Vector2 getPosition() {
		return Actors.getCenter(actor);
	}

	@Override
	public float getOrientation() {
		return actor.getRotation();
	}

	@Override
	public Vector2 getLinearVelocity() {
		
		return velocity.cpy();
	}

	@Override
	public float getAngularVelocity() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getBoundingRadius() {
		return calcultaBoundingRadius();
	}


	private float calcultaBoundingRadius() {
		return Math.max(actor.getWidth()/2f, actor.getHeight()/2f);
	}
}
