package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/** Implementação de Proximity que desconsidera o alvo perseguido como obstáculo.
 * Ou seja, faz com que o personagem automatizado não tente desviar do alvo que está perseguindo.*/
public class PursueProximity extends ProximityFieldOfView{
	Steerable<Vector2> target;
	public PursueProximity(Steerable<Vector2> owner, Steerable<Vector2> target, World world,
			float detectionRadius, float angle) {
		super(owner, world, detectionRadius, angle);
		this.target = target;
	}
	@Override
	protected boolean accept(Steerable<Vector2> steerable) {
		if(steerable == target || steerable.equals(target)){
			return false;
		}
		return super.accept(steerable);
	}

//	@Override
//	protected boolean acceptFixture(Fixture fixture) {
//		return !fixture.isSensor() && fixture.getBody().isActive();//TODO: checar fillterCollision com owner
//	}
	
	
	
}