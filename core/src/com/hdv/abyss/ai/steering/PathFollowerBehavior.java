package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Proximity.ProximityCallback;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.behaviors.Arrive;
import com.badlogic.gdx.ai.steer.utils.Path;
import com.badlogic.gdx.ai.steer.utils.Path.PathParam;
import com.badlogic.gdx.math.Vector;

public class PathFollowerBehavior<T extends Vector<T>, P extends PathParam> extends Arrive<T> implements ProximityCallback<T> {

	/** The path to follow */
	protected Path<T, P> path;

	/** The distance along the path to generate the target. Can be negative if the owner has to move along the reverse direction. */
	protected float pathOffset;

	/** The current position on the path */
	protected P pathParam;

	/** The flag indicating whether to use {@link Arrive} behavior to approach the end of an open path. It defaults to {@code true}. */
	protected boolean arriveEnabled;

	/** The time in the future to predict the owner's position. Set it to 0 for non-predictive path following. */
	protected float predictionTime;

	private T internalTargetPosition;

//	private float maxDerivingAngle;

	
	
	
	
	CollisionAvoidanceBehavior<T> collisionAvoidance;
	Proximity<T> proximity;
	
	
	
	
	
	
	
	/** Creates a non-predictive {@code FollowPath} behavior for the specified owner and path.
	 * @param owner the owner of this behavior
	 * @param path the path to be followed by the owner. */
	public PathFollowerBehavior (Proximity<T> proximity, Steerable<T> owner, Path<T, P> path) {
		this(proximity, owner, path, 0, 0);
	}

//	/** Creates a non-predictive {@code FollowPath} behavior for the specified owner, path and path offset.
//	 * @param owner the owner of this behavior
//	 * @param path the path to be followed by the owner
//	 * @param pathOffset the distance along the path to generate the target. Can be negative if the owner is to move along the
//	 *           reverse direction. */
//	public PathFollowerBehaviour (Steerable<T> owner, Path<T, P> path, float pathOffset) {
//		this(owner, path, pathOffset, 0);
//	}

	/** Creates a {@code FollowPath} behavior for the specified owner, path, path offset, maximum linear acceleration and prediction
	 * time.
	 * @param owner the owner of this behavior
	 * @param path the path to be followed by the owner
	 * @param pathOffset the distance along the path to generate the target. Can be negative if the owner is to move along the
	 *           reverse direction.
	 * @param predictionTime the time in the future to predict the owner's position. Can be 0 for non-predictive path following. */
	public PathFollowerBehavior (Proximity<T> proximity, Steerable<T> owner, Path<T, P> path, float pathOffset, float predictionTime) {
		super(owner);
		this.path = path;
		this.pathParam = path.createParam();
		this.pathOffset = pathOffset;
		this.predictionTime = predictionTime;

		this.arriveEnabled = true;

		this.internalTargetPosition = owner.newVector();
		
		
//		this.maxDerivingAngle = MathUtils.PI/2;
		
		this.proximity = proximity;
		collisionAvoidance = new CollisionAvoidanceBehavior<T>(owner, proximity);

		proximity.setOwner(owner);
	}

	boolean first = true;
	@Override
	protected SteeringAcceleration<T> calculateRealSteering (SteeringAcceleration<T> inputSteering) {
		SteeringAcceleration<T> steering = new SteeringAcceleration<T>(inputSteering.linear,inputSteering.angular);
		// Predictive or non-predictive behavior?
		T location = (predictionTime == 0) ?
		// Use the current position of the owner
		owner.getPosition()
			:
			// Calculate the predicted future position of the owner. We're reusing steering.linear here.
			steering.linear.set(owner.getPosition()).mulAdd(owner.getLinearVelocity(), predictionTime);

		// Find the distance from the start of the path
		float distance = path.calculateDistance(location, pathParam);

		// Offset it
		float targetDistance = distance + pathOffset;

		// Calculate the target position
		path.calculateTargetPosition(internalTargetPosition, pathParam, targetDistance);

		Gdx.app.log(getClass().getName(), "InputSteering.linear: " + inputSteering.linear);
//		if(!deriving(internalTargetPosition)){
////			steering.linear.set(owner.getPosition()).mulAdd(owner.getLinearVelocity(), predictionTime);
//			inputSteering.linear.set(steering.linear);
//			return inputSteering;
//		}

		steering = inputSteering;
		
//		if (arriveEnabled && path.isOpen()) {
//			if (pathOffset >= 0) {
//				// Use Arrive to approach the last point of the path
//				if (targetDistance > path.getLength() - decelerationRadius){
//					return arrive(inputSteering, internalTargetPosition);
//				}
//			} else {
//				// Use Arrive to approach the first point of the path
//				if (targetDistance < decelerationRadius){
//					return arrive(steering, internalTargetPosition);
//				}
//			}
//		}
		T positionToMove = owner.newVector().set(internalTargetPosition).sub(owner.getPosition()).nor()
				
				.scl(getActualLimiter().getMaxLinearAcceleration());

//		T movingDirection = owner.getLinearVelocity().cpy().nor();
//		T predictedLocation = owner.newVector().set(movingDirection).scl(getActualLimiter().getMaxLinearAcceleration());
		
		if(!avoidObstacle(positionToMove, steering)){
			steering.linear.set(positionToMove);
		}
		
		
//		if(false && deriving(positionToMove, predictedLocation)){
//		// Seek the target position
////			steering.linear.set(internalTargetPosition).sub(owner.getPosition()).nor()
////				.scl(getActualLimiter().getMaxLinearAcceleration());
//			steering.linear.set(positionToMove);
//			first = false;
//			
//			
//			Gdx.app.log(getClass().getName(), "DERIVING!!!");
//			
//		}
//		else{
//			steering.linear.set(positionToMove);
//		}
		// No angular acceleration
//		steering.angular = 0;

		// Output steering acceleration
		return steering;
	}
	
	private boolean avoidObstacle(T positionToMove, SteeringAcceleration<T> steering) {
		int neighborsCount = proximity.findNeighbors(this);
		
		Gdx.app.log(getClass().getName(), "find " + neighborsCount + " neighbors");
		
		if(neighborsCount > 0){
			collisionAvoidance.calculateSteering(steering);
			return true;
		}
		
		return false;
	}

	@Override
	public boolean reportNeighbor(Steerable<T> neighbor) {
		// TODO Auto-generated method stub
		return true;
	}

//	@Override
//	protected SteeringAcceleration<T> calculateRealSteering (SteeringAcceleration<T> steering) {
//		T location = (predictionTime == 0) ?
//				// Use the current position of the owner
//				owner.getPosition()
//					:
//					// Calculate the predicted future position of the owner. We're reusing steering.linear here.
//					steering.linear.set(owner.getPosition()).mulAdd(owner.getLinearVelocity(), predictionTime);
//		// Find the distance from the start of the path
//		float distance = path.calculateDistance(location, pathParam);
//
//		// Offset it
//		float targetDistance = distance + pathOffset;
//
//		// Calculate the target position
//		path.calculateTargetPosition(internalTargetPosition, pathParam, targetDistance);
//		
//		Gdx.app.log(getClass().getName(), "TargetPosition: " + internalTargetPosition);
//		
//		float maxLinearAcceleration = getActualLimiter().getMaxLinearAcceleration();
//
//
//		// Seek the internal target position
//		steering.linear.set(internalTargetPosition).sub(owner.getPosition()).nor().scl(maxLinearAcceleration);
//
//		// No angular acceleration
//		steering.angular = 0;
//
//		return steering;
//	}

//	private boolean deriving(T targetPosition, T predictedPosition) {
//		T currentPos = owner.getPosition();
//		T movingDirection = predictedPosition.cpy().sub(currentPos).nor();
//		T expectedDirection = targetPosition.cpy().sub(currentPos).nor();
//		float movingAngle = owner.vectorToAngle(movingDirection);
//		float expectedAngle = owner.vectorToAngle(expectedDirection);
//		
//		float deltaAngle = Math.abs(movingAngle - expectedAngle);
//		
//		
//		
//		Gdx.app.log(getClass().getName(), "Position to move: " + targetPosition 
//				+ "; MovingDir: " + movingDirection 
//				+ "; expectedDirection: " + expectedDirection
//				+ "; MovingAngle: " + movingAngle * MathUtils.radiansToDegrees
//				+ "; expectedAngle: " + expectedAngle* MathUtils.radiansToDegrees
//				+ "; deltaAngle: " + deltaAngle * MathUtils.radiansToDegrees);
//		
//		
//		
//		return deltaAngle > maxDerivingAngle;
//	}

	/** Returns the path to follow */
	public Path<T, P> getPath () {
		return path;
	}

	/** Sets the path followed by this behavior.
	 * @param path the path to set
	 * @return this behavior for chaining. */
	public PathFollowerBehavior<T, P> setPath (Path<T, P> path) {
		this.path = path;
		return this;
	}

	/** Returns the path offset. */
	public float getPathOffset () {
		return pathOffset;
	}

	/** Returns the flag indicating whether to use {@link Arrive} behavior to approach the end of an open path. */
	public boolean isArriveEnabled () {
		return arriveEnabled;
	}

	/** Returns the prediction time. */
	public float getPredictionTime () {
		return predictionTime;
	}

	/** Sets the prediction time. Set it to 0 for non-predictive path following.
	 * @param predictionTime the predictionTime to set
	 * @return this behavior for chaining. */
	public PathFollowerBehavior<T, P> setPredictionTime (float predictionTime) {
		this.predictionTime = predictionTime;
		return this;
	}

	/** Sets the flag indicating whether to use {@link Arrive} behavior to approach the end of an open path. It defaults to
	 * {@code true}.
	 * @param arriveEnabled the flag value to set
	 * @return this behavior for chaining. */
	public PathFollowerBehavior<T, P> setArriveEnabled (boolean arriveEnabled) {
		this.arriveEnabled = arriveEnabled;
		return this;
	}

	/** Sets the path offset to generate the target. Can be negative if the owner has to move along the reverse direction.
	 * @param pathOffset the pathOffset to set
	 * @return this behavior for chaining. */
	public PathFollowerBehavior<T, P> setPathOffset (float pathOffset) {
		this.pathOffset = pathOffset;
		return this;
	}

	/** Returns the current path parameter. */
	public P getPathParam () {
		return pathParam;
	}

	/** Returns the current position of the internal target. This method is useful for debug purpose. */
	public T getInternalTargetPosition () {
		return internalTargetPosition;
	}

	//
	// Setters overridden in order to fix the correct return type for chaining
	//

	@Override
	public PathFollowerBehavior<T, P> setOwner (Steerable<T> owner) {
		this.owner = owner;
		return this;
	}

	@Override
	public PathFollowerBehavior<T, P> setEnabled (boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	/** Sets the limiter of this steering behavior. The given limiter must at least take care of the maximum linear speed and
	 * acceleration. However the maximum linear speed is not required for a closed path.
	 * @return this behavior for chaining. */
	@Override
	public PathFollowerBehavior<T, P> setLimiter (Limiter limiter) {
		this.limiter = limiter;
		return this;
	}

	@Override
	public PathFollowerBehavior<T, P> setTarget (Steerable<T> target) {
		this.target = target;
		return this;
	}

	@Override
	public PathFollowerBehavior<T, P> setArrivalTolerance (float arrivalTolerance) {
		this.arrivalTolerance = arrivalTolerance;
		return this;
	}

	@Override
	public PathFollowerBehavior<T, P> setDecelerationRadius (float decelerationRadius) {
		this.decelerationRadius = decelerationRadius;
		return this;
	}

	@Override
	public PathFollowerBehavior<T, P> setTimeToTarget (float timeToTarget) {
		this.timeToTarget = timeToTarget;
		return this;
	}
}
