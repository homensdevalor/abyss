package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.behaviors.Arrive;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.TimeUtils;
import com.hdv.abyss.utils.Vectors;

public class TolerantArrive<T extends Vector<T>> extends Arrive<T>{
	private float toleranceDistance;
	private float toleranceOrientation;
	private Long maxTimeMillis;
	private long startTimeCountMillis;
	public TolerantArrive(Steerable<T> owner) {
		this(owner, null);
	}
	public TolerantArrive(Steerable<T> owner, Steerable<T> target) {
		this(owner,target, 1, MathUtils.PI/2);
	}
	public TolerantArrive(Steerable<T> owner, Steerable<T> target
			, float toleranceDistance, float toleranceOrientation) 
	{
		super(owner, target);
		
		this.toleranceDistance = toleranceDistance;
		this.toleranceOrientation = toleranceOrientation;
		this.maxTimeMillis = null;
	}
	
	
	public Long getMaxTimeMillis() {
		return maxTimeMillis;
	}
	public void setMaxTimeMillis(Long maxTimeMillis) {
		if(this.maxTimeMillis == null && maxTimeMillis != null){
			resetTime();
		}
		this.maxTimeMillis = maxTimeMillis;
	}
	public float getToleranceDistance() {
		return toleranceDistance;
	}
	public void setToleranceDistance(float toleranceDistance) {
		this.toleranceDistance = toleranceDistance;
	}
	public float getToleranceOrientation() {
		return toleranceOrientation;
	}
	public void setToleranceOrientation(float toleranceOrientation) {
		this.toleranceOrientation = toleranceOrientation;
	}
	
	@Override
	protected SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
		return tolerantArrive(steering, getTargetPosition());
	}
	@Override
	public SteeringAcceleration<T> arrive(SteeringAcceleration<T> steering, T position){
		return super.arrive(steering, position);
	}
	public SteeringAcceleration<T> tolerantArrive(SteeringAcceleration<T> steering, T position){
		if(isFarFromTheWay() || getOwner().getLinearVelocity().isZero()){
			super.arrive(steering, getTargetPosition());
		}
		else{
			continueMoving(steering);
		}
		return steering;
	}
	protected T getTargetPosition() {
		return target.getPosition();
	}
	protected void continueMoving(SteeringAcceleration<T> steering) {
		T direction = getMoveDirection();
		T predictedPosition = direction.scl(owner.getMaxLinearSpeed()).add(owner.getPosition());

		float maxLinearAcceleration = getActualLimiter().getMaxLinearAcceleration();

		// Seek the internal target position
		steering.linear.set(predictedPosition).sub(owner.getPosition()).nor().scl(maxLinearAcceleration);
	}
	protected T getMoveDirection() {
		return getOwner().getLinearVelocity().cpy().nor();
	}
	protected boolean isFarFromTheWay() {
		if(timeOver()){
			resetTime();
			return true;
		}
		
		T expectedDir = getExpectedDirection();
		if(checkOrientation(expectedDir)){
			return true;
		}
		
		T currentDir = getMoveDirection();
		if(checkDistance(currentDir, expectedDir)){
			return true;
		}
		
		return false;
	}
	private boolean checkOrientation(T expectedDir) {
		float currentOrientation = getCurrentOrientation();
		float expectedOrientation = owner.vectorToAngle(expectedDir);
		float deltaDir = expectedOrientation -currentOrientation;
		boolean wrongDirection = deltaDir > toleranceOrientation;
		return wrongDirection;
	}
	private boolean checkDistance(T currentDir, T expectedDir) {
		T expectedPosition = expectedDir.cpy().nor().scl(owner.getLinearVelocity());
		T predictedPosition = currentDir.cpy().nor().scl(owner.getLinearVelocity());
		float distance = Vectors.distance(expectedPosition, predictedPosition);
		return (distance > toleranceDistance);
	}
	private boolean timeOver() {
		if(maxTimeMillis != null){
			return (TimeUtils.millis() >= startTimeCountMillis + maxTimeMillis);
		}
		return false;
	}
	private void resetTime() {
		startTimeCountMillis = TimeUtils.millis();
	}
	protected float getCurrentOrientation() {
		return getOwner().getOrientation();
	}
	protected T getExpectedDirection() {
		Steerable<T> target = getTarget();
		if(target != null){
			T directionToTarget = target.getPosition().cpy().sub(getOwner().getPosition()).nor();
			return directionToTarget;
		}
		return getOwner().getLinearVelocity().cpy().nor();
	}

}
