package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.weapon.Weapon;
import com.hdv.abyss.physics.PhysicsActor;

public class PositioningAttackSteering extends SteeringBehavior<Vector2>{
	TolerantArrive<Vector2> arriveBehavior;
	GameCharacter owner;
	GameCharacter target;
	final Vector2 positionToMove;
	public PositioningAttackSteering(GameCharacter owner, GameCharacter target) {
		this(owner,target, null);
	}
	public PositioningAttackSteering(GameCharacter owner, GameCharacter target, TolerantArrive<Vector2> arrive) {
		super(SteerableUtils.getSteerable(owner));
		this.owner = owner;
		this.target = target;
		
		arriveBehavior = arrive != null ? arrive 
										: new TolerantArrive<Vector2>(super.getOwner(), SteerableUtils.getSteerable(target));
		positionToMove = new Vector2();
	}

	@Override
	protected SteeringAcceleration<Vector2> calculateRealSteering(SteeringAcceleration<Vector2> steering){
		Weapon weapon = owner.getWeapon();
		weapon.getAttackPosition(owner, target, positionToMove);
		
		return arriveBehavior.tolerantArrive(steering, positionToMove);
	}

	protected PhysicsActor getTargetCharacter() {
		return target;
	}

	protected GameCharacter getOwnerCharacter() {
		return owner;
	}

}
