package com.hdv.abyss.ai.steering;

import java.util.Random;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.TimeUtils;

public class LinearPathWander<T extends Vector<T>> extends SteeringBehavior<T>{

	final Random random;
	long startTime;
	long timeMove;
	
	T currentDirection;
	
	public LinearPathWander(Steerable<T> owner) {
		super(owner, owner, true);
		random = new Random();
		currentDirection = owner.newVector();
		
		reset(owner);
	}

	private void reset(Steerable<T> steerable) {
		choiceDirection();
		startTime = TimeUtils.millis();
		timeMove = random.nextInt(1000) + 700;
	}
	
	@Override
	public SteeringBehavior<T> setOwner(Steerable<T> owner) {
		super.setOwner(owner);
		reset(owner);
		
		return this;
	}

	@Override
	protected SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
		T direction = getOwner().getLinearVelocity().cpy().nor();
		if(needChange()){
			reset(getOwner());
			direction = currentDirection.cpy();
		}

		T targetPosition = direction.scl(owner.getMaxLinearSpeed()).add(owner.getPosition());
		float maxLinearAcceleration = getActualLimiter().getMaxLinearAcceleration();

		// Seek the internal target position
		steering.linear.set(targetPosition).sub(owner.getPosition()).nor().scl(maxLinearAcceleration);

		// No angular acceleration
		steering.angular = 0;

		return steering;
	}

	private boolean needChange() {
		return TimeUtils.millis() > startTime + timeMove;
	}

	protected void choiceDirection() {
		float randomAngle = random.nextFloat()*(MathUtils.PI*2);
		owner.angleToVector(currentDirection, randomAngle).nor();
	}
}
