package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.hdv.abyss.ai.BaseBox2dSteerable;
import com.hdv.abyss.physics.PhysicsActor;

public class SteerableUtils {
	@SuppressWarnings("unchecked")
	public static Steerable<Vector2> getSteerable(Fixture fixture) {
		Body body = fixture.getBody();
		Object userData = body.getUserData();
		if(userData != null){
			if(userData instanceof Actor){
				Steerable<Vector2> steerable = getSteerable((Actor)userData);
				if(steerable != null){
					return steerable;
				}
				else{
					return new ActorSteerable((Actor)userData);
				}
			}
			else if(userData instanceof Steerable<?>){
				return (Steerable<Vector2>) userData;
			}
		}
		return new BaseBox2dSteerable(body);
	}

	@SuppressWarnings("unchecked")
	public static Steerable<Vector2> getSteerable(Actor actor) {
		Object userObject = actor.getUserObject();
		if(userObject != null){
			if(userObject instanceof Steerable<?>){
				return (Steerable<Vector2>) userObject;
			}
		}
		else if(actor instanceof PhysicsActor){
			PhysicsActor pActor = ((PhysicsActor)actor);
			Steerable<Vector2> steerable = new BaseBox2dSteerable(pActor.getBody(), pActor.getSizeInWorld());
			actor.setUserObject(steerable);
			return steerable;
		}
		return null;
	}
}
