package com.hdv.abyss.ai.steering;

import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance;
import com.badlogic.gdx.math.Vector;

public class CollisionAvoidanceBehavior<T extends Vector<T> > extends CollisionAvoidance<T> {

	public CollisionAvoidanceBehavior(Steerable<T> owner, Proximity<T> proximity) {
		super(owner, proximity);

		this.firstRelativePosition = owner.newVector();
		this.firstRelativeVelocity = owner.newVector();

		this.relativeVelocity = owner.newVector();
	}
	private float shortestTime;
	private Steerable<T> firstNeighbor;
//	private float firstMinSeparation;
//	private float firstDistance;
	private T firstRelativePosition;
	private T firstRelativeVelocity;
	private T relativePosition;
	private T relativeVelocity;

	@Override
	protected SteeringAcceleration<T> calculateRealSteering (SteeringAcceleration<T> steering) {
		shortestTime = Float.POSITIVE_INFINITY;
		firstNeighbor = null;
//		firstMinSeparation = 0;
//		firstDistance = 0;
		relativePosition = steering.linear;

		// Take into consideration each neighbor to find the most imminent collision.
		int neighborCount = proximity.findNeighbors(this);

		// If we have no target, then return no steering acceleration
		if (neighborCount == 0 || firstNeighbor == null) return steering.setZero();

		T ahead = owner.getPosition().cpy().add(owner.getLinearVelocity().cpy().nor().scl(3));
		// If we're going to hit exactly, or if we're already
		// colliding, then do the steering based on current position.
//		if (firstMinSeparation <= 0 || 
//				firstDistance < owner.getBoundingRadius() + firstNeighbor.getBoundingRadius()) {
//			
//
////			relativePosition.set(firstNeighbor.getPosition()).sub(owner.getPosition());
//			relativePosition.set(ahead).sub(firstNeighbor.getPosition());
//		} else {
//			// Otherwise calculate the future relative position
//			relativePosition.set(firstRelativePosition).mulAdd(firstRelativeVelocity, shortestTime);
//		}
		relativePosition.set(ahead).sub(firstNeighbor.getPosition());

		// Avoid the target
		// Notice that steerling.linear and relativePosition are the same vector
		relativePosition.nor().scl(-getActualLimiter().getMaxLinearAcceleration());

		// No angular acceleration
		steering.angular = 0f;

		// Output the steering
		return steering;
	}

	@Override
	public boolean reportNeighbor (Steerable<T> neighbor) {
		// Calculate the time to collision
		relativePosition.set(neighbor.getPosition()).sub(owner.getPosition());
		relativeVelocity.set(neighbor.getLinearVelocity()).sub(owner.getLinearVelocity());
		float relativeSpeed2 = relativeVelocity.len2();
		float timeToCollision = -relativePosition.dot(relativeVelocity) / relativeSpeed2;

		// If timeToCollision is negative, i.e. the owner is already moving away from the the neighbor,
		// or it's not the most imminent collision then no action needs to be taken.
		if (timeToCollision <= 0 || timeToCollision >= shortestTime) return false;

		// Check if it is going to be a collision at all
		float distance = relativePosition.len();
		float minSeparation = distance - (float)Math.sqrt(relativeSpeed2) * timeToCollision /* shortestTime */;
		if (minSeparation > owner.getBoundingRadius() + neighbor.getBoundingRadius()) return false;

		// Store most imminent collision data
		shortestTime = timeToCollision;
		firstNeighbor = neighbor;
//		firstMinSeparation = minSeparation;
//		firstDistance = distance;
		firstRelativePosition.set(relativePosition);
		firstRelativeVelocity.set(relativeVelocity);

		return true;
	}
}
