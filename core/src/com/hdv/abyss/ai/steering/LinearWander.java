package com.hdv.abyss.ai.steering;

import java.util.Random;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector;

public class LinearWander<T extends Vector<T>> extends SteeringBehavior<T>{

	final T currentDirection;
	final T startMovePosition;
	final Random random;
	float distanceToMove;
	float minimumDistance, maximumDistance;
	final T previousPosition;
	
	public LinearWander(Steerable<T> owner) {
		super(owner, owner, true);
		
		currentDirection = owner.newVector();
		startMovePosition = owner.newVector();
		previousPosition = owner.newVector();
		random = new Random();
		this.minimumDistance = 1;
		this.maximumDistance = 4;
	}

	public float getMinimumDistance() {
		return minimumDistance;
	}
	public float getMaximumDistance() {
		return maximumDistance;
	}

	public LinearWander<T> setMinimumDistance(float minimumDistance) {
		this.minimumDistance = minimumDistance;
		return this;
	}
	public LinearWander<T> setMaximumDistance(float maximumDistance) {
		this.maximumDistance = maximumDistance;
		return this;
	}

//	private void reset(Steerable<T> steerable) {
//		startMovePosition.set(steerable.getPosition());
//		choiceDirection();
//		choiceDistance();
//	}

	@Override
	protected SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
//		getOwner()
		return null;
	}

//	private void choiceDistance() {
//		distanceToMove = random.nextFloat() *  (maximumDistance - minimumDistance) + minimumDistance;
//	}

	protected void choiceDirection() {
		float randomAngle = random.nextFloat()*(MathUtils.PI*2);
		owner.angleToVector(currentDirection, randomAngle).nor();
	}
}
