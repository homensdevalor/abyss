package com.hdv.abyss.ai;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.utils.Vectors;

public abstract class BaseSteerable implements Steerable<Vector2>{
	boolean tagged;

	float maxLinearSpeed;
	float maxLinearAcceleration;
	float maxAngularSpeed;
	float maxAngularAcceleration;
	
	private Object userData;
	
	protected SteeringBehavior<Vector2> steeringBehavior;

	protected final SteeringAcceleration<Vector2> steeringOutput;

	public BaseSteerable() {
		steeringOutput  = new SteeringAcceleration<Vector2>(new Vector2());
	}

	public SteeringBehavior<Vector2> getSteeringBehavior () {
		return steeringBehavior;
	}

	public void setSteeringBehavior (SteeringBehavior<Vector2> steeringBehavior) {
		this.steeringBehavior = steeringBehavior;
	}

	public void update (float deltaTime) {
		if (steeringBehavior != null) {
			// Calculate steering acceleration
			steeringBehavior.calculateSteering(steeringOutput);

			/*
			 * Here you might want to add a motor control layer filtering steering accelerations.
			 * 
			 * For instance, a car in a driving game has physical constraints on its movement: it cannot turn while stationary; the
			 * faster it moves, the slower it can turn (without going into a skid); it can brake much more quickly than it can
			 * accelerate; and it only moves in the direction it is facing (ignoring power slides).
			 */
			
			// Apply steering acceleration
			applySteering(steeringOutput, deltaTime);
		}
	}
	protected abstract void applySteering (SteeringAcceleration<Vector2> steering, float deltaTime);
	
	@Override
	public abstract Vector2 getPosition();
	@Override
	public abstract float getOrientation();
	@Override
	public abstract Vector2 getLinearVelocity();
	@Override
	public abstract float getAngularVelocity();
	@Override
	public abstract float getBoundingRadius();
	
	public Object getUserData() {
		return userData;
	}

	public void setUserData(Object userData) {
		this.userData = userData;
	}

	@Override
	public boolean isTagged() {
		return this.tagged;
	}

	@Override
	public void setTagged(boolean tagged) {
		this.tagged = tagged;
	}

	@Override
	public Vector2 newVector() {
		return new Vector2();
	}
    // Actual implementation depends on your coordinate system.
    // Here we assume the y-axis is pointing upwards.
	@Override
    public float vectorToAngle (Vector2 vector) {
//        return (float)Math.atan2(-vector.x, vector.y);
		return Vectors.vectorToAngle(vector);
    }

    // Actual implementation depends on your coordinate system.
    // Here we assume the y-axis is pointing upwards.
    @Override
    public Vector2 angleToVector (Vector2 outVector, float angle) {
//        outVector.x = -(float)Math.sin(angle);
//        outVector.y = (float)Math.cos(angle);
//        return outVector;
        return Vectors.angleToVector(outVector, angle);
    }

	//
	// Limiter implementation
	//

	@Override
	public float getMaxLinearSpeed () {
		return maxLinearSpeed;
	}

	@Override
	public void setMaxLinearSpeed (float maxLinearSpeed) {
		this.maxLinearSpeed = maxLinearSpeed;
	}

	@Override
	public float getMaxLinearAcceleration () {
		return maxLinearAcceleration;
	}

	@Override
	public void setMaxLinearAcceleration (float maxLinearAcceleration) {
		this.maxLinearAcceleration = maxLinearAcceleration;
	}

	@Override
	public float getMaxAngularSpeed () {
		return maxAngularSpeed;
	}

	@Override
	public void setMaxAngularSpeed (float maxAngularSpeed) {
		this.maxAngularSpeed = maxAngularSpeed;
	}

	@Override
	public float getMaxAngularAcceleration () {
		return maxAngularAcceleration;
	}

	@Override
	public void setMaxAngularAcceleration (float maxAngularAcceleration) {
		this.maxAngularAcceleration = maxAngularAcceleration;
	}
}
