package com.hdv.abyss.ai;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class BaseBox2dSteerable extends BaseSteerable{
	private final Body body;
	private Float boundingRadius;
	public BaseBox2dSteerable(Body body) {
		this(body, null);
	}
	public BaseBox2dSteerable(Body body, Vector2 sizeInWorld) {
		this.body = body;
		boundingRadius = calcultaBoundingRadius(sizeInWorld);
		
		setUserData(body);
	}
	private Float calcultaBoundingRadius(Vector2 sizeInWorld) {
		if(sizeInWorld == null){
			return null;
		}
		return Math.max(sizeInWorld.x, sizeInWorld.y);
	}
	@Override
	protected void applySteering(SteeringAcceleration<Vector2> steering, float deltaTime) {
	}

	@Override
	public Vector2 getPosition() {
		return body.getPosition();
	}

	@Override
	public float getOrientation() {
		return body.getAngle();
	}

	@Override
	public Vector2 getLinearVelocity() {
		return body.getLinearVelocity();
	}

	@Override
	public float getAngularVelocity() {
		return body.getAngularVelocity();
	}

	@Override
	public float getBoundingRadius() {
		if(boundingRadius == null){
			boundingRadius = calculateRadius(body);
		}
		return boundingRadius;
	}
	private float calculateRadius(Body body) {
		float maxRadius = 0;
		for(Fixture fixture : body.getFixtureList()){
			Shape shape = fixture.getShape();
			float shapeRadius = shape.getRadius();
			Gdx.app.log(getClass().getName(),"To Body: " + body + " fixture radius: " + shapeRadius);
			if(shapeRadius > maxRadius){
				maxRadius = shapeRadius;
			}
			
			checkOutThisFixture(fixture);
		}
		
		Gdx.app.log(getClass().getName(),"To Body: " + body + " calculated body radius: " + maxRadius);
		
		return maxRadius;
	}
	
	public void checkOutThisFixture(Fixture fixture) {
	    Shape fixtureShape = fixture.getShape();
	    if (fixtureShape instanceof PolygonShape) {
	        PolygonShape polygonShape = (PolygonShape) fixtureShape;
	        Float minX = null;
	        Float maxX = null;
	        Float minY = null;
	        Float maxY = null;
	        for (int i = 0; i < polygonShape.getVertexCount(); i++) {
	            Vector2 nextVertex = new Vector2();
	            polygonShape.getVertex(i, nextVertex);
	            
	            float x = nextVertex.x;
	            float y = nextVertex.y;
	            if (minX == null || x < minX) {
	                minX = x;
	            }
	            if (maxX == null || x > maxX) {
	                maxX = x;
	            }
	            if (minY == null || y < minY) {
	                minY = y;
	            }
	            if (maxY == null || y > maxY) {
	                maxY = y;
	            }
	        }
	        float width = maxX - minX;
	        float height = maxY - minY;
	        float halfWidth = width / 2;
	        float halfHeight = height / 2;
	        System.out.println("The polygon has half width & height of: " + halfWidth + " & " + halfHeight);
	    } else if (fixtureShape instanceof CircleShape) {
	        float radius = ((CircleShape) fixtureShape).getRadius();
	        System.out.println("The circle has a radius of : " + radius);
	    } else {
	        // TODO handle other shapes
	    }
	}
}
