package com.hdv.abyss.ai.controlstates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.TimeUtils;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.statemachine.ChangedStateEvent;
import com.hdv.abyss.statemachine.OnStateChangeListener;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.AttackEvent;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseState;
import com.hdv.abyss.utils.Actors;

public class FightingState extends BaseState<GameCharacter> implements OnStateChangeListener{

	private Actor target;
	private boolean waiting;
	private long waitTime, startWaiting;
	public FightingState(StateMachine<GameCharacter> machine, Actor target, long waitTime) {
		super(machine);
		this.target = target;
		waiting = false;
		this.waitTime = waitTime;
	}
	@Override
	public boolean onStart(GameEvent startEvent) {
		Gdx.app.log(getClass().getName(),"Enter FightingState!");
		GameCharacter character = getStateMachine().getEntity();
		character.addOnStateChangeListener(this);
//		attack();
		character.stop();
		waiting = true;
		//Gambiarra para fazer personagem esperar menos antes do primeiro ataque
		startWaiting = TimeUtils.millis() - waitTime/2;
		return super.onStart(startEvent);
	}
	@Override
	public void onStop() {
		Gdx.app.log(getClass().getName(),"Stop FightingState!");
		super.onStop();
		getStateMachine().getEntity().removeOnStateChangeListener(this);
	}
	@Override
	public void update(float delta) {
		super.update(delta);
		if(waiting && waitOver()){
			attack();
		}
	}
	
	private boolean waitOver() {
		long waitedTime = (TimeUtils.millis() - startWaiting);
		return waitedTime >= waitTime;
	}
	@Override
	public void onStateChange(ChangedStateEvent<?> evt) {
		if(evt.getNewStateKey() == BattleCharacterStates.ATTACKING){
			onAttackStart();
		}
		else if(evt.getPreviousStateKey() == BattleCharacterStates.ATTACKING){
			onAttackEnd();
		}
		if(evt.getPreviousStateKey() == BattleCharacterStates.DAMAGED){
			onDamageEnd();
		}
	}
	protected void onAttackStart() 
	{ }
	protected void onAttackEnd() {
		Gdx.app.log(getClass().getName(),"onAttackEnd!");
		waiting = true;
		startWaiting = TimeUtils.millis();
	}
	private void onDamageEnd() {
		attack();
	}
	protected void attack() {
		Gdx.app.log(getClass().getName(),"attack!");
		waiting = false;
		getStateMachine().getEntity().setDirection(getAttackDirection());
		getStateMachine().getEntity().fireEvent(new AttackEvent());
	}
	private Direction getAttackDirection() {
		Vector2 ownerPosition = getStateMachine().getEntity().getPositionInWorld();
		Vector2 targetPosition = new Vector2(Actors.getCenter(target));
		return Direction.roundFromVector(targetPosition.sub(ownerPosition).nor());
	}
//	
//	protected void attack() {
//		Gdx.app.log(getClass().getName(),"attack!");
//		waiting = false;
//		GameCharacter owner =getStateMachine().getEntity();
//		owner.lookAt(getAttackDirection());
////		owner.lookAtArea(Actors.getCenter(target), new Vector2(target.getWidth(),target.getHeight()));
//		getStateMachine().getEntity().fireEvent(new AttackEvent());
//	}
//	private Vector2 getAttackDirection() {
//		GameCharacter owner = getStateMachine().getEntity();
//		Vector2 targetPosition = new Vector2(Actors.getCenter(target));
//		
////		Vector2 attackPoint = getNearestPoint(actorToRectangle(owner), actorToRectangle(target));
////		return targetPosition.sub(attackPoint).nor();
//		return targetPosition.sub(owner.getPositionInWorld()).nor();
//	}
//	private Rectangle actorToRectangle(Actor actor) {
//		return new Rectangle(actor.getX(), actor.getY(), actor.getWidth(), actor.getHeight());
//	}
//	private Vector2 getNearestPoint(Rectangle ownerArea, Rectangle targetArea) {
//		Vector2 ownerCenter = ownerArea.getCenter(new Vector2());
//		Vector2 targetCenter = targetArea.getCenter(new Vector2());
//		
//		float x= targetCenter.x; //Por padrão no centro
//		if(ownerCenter.x >= targetArea.getX() + targetArea.getWidth()){ //Owner à direita
//			x = targetArea.getX() + targetArea.getWidth();
//		}
//		else if(ownerCenter.x < targetArea.getX()){ //Owner à esquerda
//			x = targetArea.getX();
//		}
//		float y= targetCenter.y; //Por padrão no centro
//		if(ownerCenter.y >= targetArea.getY() + targetArea.getHeight()){ //Owner acima
//			y = targetArea.getY() + targetArea.getHeight();
//		}
//		else if(ownerCenter.y < targetArea.getY()){ //Owner abaixo
//			y = targetArea.getY();
//		}
//		
//		
//		return new Vector2(x,y);
//	}
}
