package com.hdv.abyss.ai.controlstates;

import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance;
import com.badlogic.gdx.ai.steer.behaviors.PrioritySteering;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.ai.GameCharacterSteerable;
import com.hdv.abyss.ai.steering.LinearPathWander;
import com.hdv.abyss.ai.steering.ProximityFieldOfView;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseState;

public class MovingAroundState extends BaseState<GameCharacter>{
	final Vector2 lastPosition;
	float distanceToMove;
	float minimumDistance, maximumDistance;
	private Object nextStateKey;
	
	GameCharacter movingCharacter;
	GameCharacterSteerable characterSteerable;
	SteeringBehavior<Vector2> movingAroundBehavior;
	
	public MovingAroundState(StateMachine<GameCharacter> stateMachine, Object nextStateKey
			, float minDistance, float maxDistance) {
		super(stateMachine);
		
//		currentDirection = new Vector2();
//		startMovePosition = new Vector2();
//		random = new Random();
		this.minimumDistance = minDistance;
		this.maximumDistance = maxDistance;
		this.nextStateKey = nextStateKey;
		
		lastPosition = new Vector2();
		
		
		movingCharacter = stateMachine.getEntity();
		characterSteerable = new GameCharacterSteerable(stateMachine.getEntity());
	}

	@Override
	public boolean onStart(GameEvent startEvent) {		
		if(movingAroundBehavior == null){

			Proximity<Vector2> proximity = new ProximityFieldOfView(characterSteerable, 
					movingCharacter.getPhysicalWorld().getWorld()
					, characterSteerable.getBoundingRadius()*2,MathUtils.degreesToRadians * 90);
			
			PrioritySteering<Vector2> prioritySteeringSB = new PrioritySteering<Vector2>(
					characterSteerable, 0.0001f);
			prioritySteeringSB.add(new CollisionAvoidance<Vector2>(characterSteerable, proximity));
			prioritySteeringSB.add(new LinearPathWander<Vector2>(characterSteerable));
			movingAroundBehavior = prioritySteeringSB;
			
			characterSteerable.setSteeringBehavior(movingAroundBehavior);
		}
		
		
		movingCharacter = getStateMachine().getEntity();
		lastPosition.set(movingCharacter.getPositionInWorld());
		choiceDistance();
		return super.onStart(startEvent);
	}
	
	private void choiceDistance() {
		distanceToMove = MathUtils.random() *  (maximumDistance - minimumDistance) + minimumDistance;
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		
		characterSteerable.update(delta);
		
		if(moveEnd()){
			if(nextStateKey != null){
				getStateMachine().changeToState(nextStateKey);
			}
			else{
				getStateMachine().changeToDefaultState();
			}
		}
	}

	private boolean moveEnd() {
		Vector2 currentPosition = movingCharacter.getPositionInWorld();
		float movedDistance = currentPosition.cpy().sub(lastPosition).len();
		
		
		distanceToMove -= movedDistance;
		
		lastPosition.set(currentPosition);
		return distanceToMove <= 0;
	}
}
