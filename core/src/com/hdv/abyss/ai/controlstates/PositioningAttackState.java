package com.hdv.abyss.ai.controlstates;

import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance;
import com.badlogic.gdx.ai.steer.behaviors.PrioritySteering;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.ai.GameCharacterSteerable;
import com.hdv.abyss.ai.steering.PositioningAttackSteering;
import com.hdv.abyss.ai.steering.PursueProximity;
import com.hdv.abyss.ai.steering.SteerableUtils;
import com.hdv.abyss.ai.steering.TolerantArrive;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseState;

public class PositioningAttackState extends BaseState<GameCharacter>{
	private GameCharacterSteerable ownerSteerable;
	private Steerable<Vector2> targetSteerable;
	private GameCharacter owner, target;
	private SteeringBehavior<Vector2> behaviour;
	private Proximity<Vector2> proximity;

	public PositioningAttackState(StateMachine<GameCharacter> machine, GameCharacter target) {
		this(machine, target, null);
	}
	public PositioningAttackState(StateMachine<GameCharacter> machine, GameCharacter target, Proximity<Vector2> proximity) {
		super(machine);
		
		owner = machine.getEntity();
		this.target = target;
		ownerSteerable = new GameCharacterSteerable(owner);
		targetSteerable =  SteerableUtils.getSteerable(target);
		
		owner.setUserObject(ownerSteerable);
		
		this.proximity = proximity;
	}
	
	@Override
	public boolean onStart(GameEvent startEvent) {
		if(proximity == null){
			this.proximity = new PursueProximity(
					ownerSteerable
					, targetSteerable
					, owner.getPhysicalWorld().getWorld()
					, ownerSteerable.getBoundingRadius()*2
					, MathUtils.degreesToRadians * 90);
//			Gdx.app.log(getClass().getName(),"Proximity bounding radius: " + ownerSteerable.getBoundingRadius() * 2);
//			this.proximity = new ProximityRadius(ownerSteerable, owner.getPhysicalWorld().getWorld(), ownerSteerable.getBoundingRadius() * 2);
		}
		if(behaviour == null){
			setupBehavior();
		}
		ownerSteerable.setSteeringBehavior(behaviour);
		behaviour.setEnabled(true);
		return true;
	}
	private void setupBehavior() {
		PrioritySteering<Vector2> prioritySteeringSB = new PrioritySteering<Vector2>(
				ownerSteerable, 0.0001f);
		prioritySteeringSB.add(new CollisionAvoidance<Vector2>(ownerSteerable, proximity));
		TolerantArrive<Vector2> arrive = new TolerantArrive<Vector2>(
				ownerSteerable, targetSteerable, 1.8f, MathUtils.PI*0.75f);
		arrive.setMaxTimeMillis(800L);
		prioritySteeringSB.add(new PositioningAttackSteering(owner, target, arrive));
//		prioritySteeringSB.add(arrive);
		
		behaviour = prioritySteeringSB;
	} 
	
	@Override
	public void onStop() {
		super.onStop();
		owner.stop();
		behaviour.setEnabled(false);
	}
	@Override
	public void update(float delta) {
		super.update(delta);			
		ownerSteerable.update(delta);
	}
		
}
