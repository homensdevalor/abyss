package com.hdv.abyss.ai.controlstates;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.StopEvent;
import com.hdv.abyss.statemachine.states.TimedState;

public class WaitingState extends TimedState<GameCharacter> {

	public WaitingState(StateMachine<GameCharacter> stateMachine,
			Object nextStateKey, long waitTime) {
		super(stateMachine, nextStateKey, waitTime);
	}

	@Override
	public boolean onStart(GameEvent startEvent) {
		getStateMachine().getEntity().fireEvent(new StopEvent());
		return super.onStart(startEvent);
	}
}
