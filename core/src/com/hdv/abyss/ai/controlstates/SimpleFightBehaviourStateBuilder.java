package com.hdv.abyss.ai.controlstates;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.physics.collision.matcher.InstanceCollisionMatcher;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.StateMapBuilder;
import com.hdv.abyss.statemachine.states.State;
import com.hdv.abyss.statemachine.transitions.AndTransition;
import com.hdv.abyss.statemachine.transitions.DistanceTransition;
import com.hdv.abyss.statemachine.transitions.DistanceTransition.DistanceType;
import com.hdv.abyss.statemachine.transitions.LiveCharacterTransition;
import com.hdv.abyss.statemachine.transitions.LiveCharacterTransition.LiveState;
import com.hdv.abyss.statemachine.transitions.OnCollisionTransition;
import com.hdv.abyss.statemachine.transitions.Transition;

public class SimpleFightBehaviourStateBuilder implements StateMapBuilder<GameCharacter> {
	private GameCharacter target;
	
	private static final float DEFAULT_ENTER_ATTACK_DISTANCE = 2.5f;
	private static final float DEFAULT_LEAVING_ATTACK_DISTANCE = 2.8f;
	private static final float DEFAULT_LEAVING_PURSUING_DISTANCE = 7.5f;
	private static final float DEFAULT_ENTER_PURSUING_DISTANCE = 4f;

	private float enterPursuingDistance, leavingPursuingDistance;
	private float enterPrepareAttackDistance, leavePrepareAttackDistance;
	private float reachAttackDistanceOffset;
	/* ***************************** States ***********************************************/
	private PursuingState pursuingState;
	private WaitingState idleState;
	private MovingAroundState movingAroundState;
	private FightingState fightState;
	private PositioningAttackState prepareAttackState;

	/* ****************************** Transitions *****************************************/
	private Transition pursuitIfAlive;
	
	private AttackDistanceTransition onReachAttackDistance;
	private Transition whenTargetDieTakeAWalk;
	private Transition whenTargetRunAwayGoToIdle;
	private Transition onCollideGoToIdle;
	private Transition whenDeadGoToIdle;
	private Transition whenClosePrepareAttack, whenFarPursuit;
	private Transition whenReachAttackDistanceFight, whenLeaveAttackDistancePrepare;
	
	private void setupStatesTransitions() {
		//Sai do estado de paralisado ao iniciar perseguição ou ao acabar tempo de espera (transição interna do estado)
		idleState.addTransition(pursuitIfAlive);
		//FIXME: desativar transições de idle qnd character estiver morto
		
		movingAroundState.addTransitions(
					pursuitIfAlive
				  , onCollideGoToIdle
				  , whenDeadGoToIdle);

		pursuingState.addTransitions(
				    whenTargetDieTakeAWalk
				  , whenDeadGoToIdle
				  
				  , whenTargetRunAwayGoToIdle
				  , whenClosePrepareAttack);
		
		prepareAttackState.addTransitions(
				    whenTargetDieTakeAWalk
				  , whenDeadGoToIdle
				  
				  , whenFarPursuit
				  , whenReachAttackDistanceFight);

		fightState.addTransitions(
				    whenTargetDieTakeAWalk
				  , whenDeadGoToIdle
				  
				  , whenLeaveAttackDistancePrepare);
	}
	
	
	public SimpleFightBehaviourStateBuilder(GameCharacter target) {
		this.target = target;
		enterPursuingDistance = DEFAULT_ENTER_PURSUING_DISTANCE;
		leavingPursuingDistance = DEFAULT_LEAVING_PURSUING_DISTANCE;
		
		//TODO: obter estas distâncias com base na weapon
		enterPrepareAttackDistance = DEFAULT_ENTER_ATTACK_DISTANCE;
		leavePrepareAttackDistance = DEFAULT_LEAVING_ATTACK_DISTANCE;
	}
	
	public GameCharacter getTarget() {
		return target;
	}
	public void setTarget(GameCharacter target) {
		this.target = target;
	}
	public float getEnterPursuingDistance() {
		return enterPursuingDistance;
	}
	public void setEnterPursuingDistance(float enterPursuingDistance) {
		this.enterPursuingDistance = enterPursuingDistance;
	}
	public float getLeavingPursuingDistance() {
		return leavingPursuingDistance;
	}
	public void setLeavingPursuingDistance(float leavingPursuingDistance) {
		this.leavingPursuingDistance = leavingPursuingDistance;
	}

	@Override
	public Object getDefaultStateKey() {
		return SimpleFightBehaviour.Idle;
	}

	//FIXME: como permitir alterar target dinamicamente?
	@Override
	public Map<Object, State<? extends GameCharacter>> buildStates(StateMachine<GameCharacter> stateMachine) {
		GameCharacter character = stateMachine.getEntity();
		
		setupStates(stateMachine);
		
		setupPursuitTransitions(character);
		setupFightTransitions(character);
		setupPrepareAttackTransitions(character);
		setupPursuitAttackTransitions(character); 
		setupMovingAroundTransitions(character);

		setupStatesTransitions();
		
		return buildBehaviorStateMap();
	}

	private void setupStates(StateMachine<GameCharacter> stateMachine) {
		pursuingState      = new PursuingState(stateMachine,target);
		idleState          = new WaitingState(stateMachine, SimpleFightBehaviour.MovingAround, 1000);
		movingAroundState  = new MovingAroundState(stateMachine, SimpleFightBehaviour.Idle, 2f, 4f);
		fightState         = new FightingState(stateMachine, target, 400);
		prepareAttackState = new PositioningAttackState(stateMachine, target);
	}

	private void setupMovingAroundTransitions(GameCharacter character) {
		//Para de se mover quando character colide com algo
		onCollideGoToIdle = new OnCollisionTransition(SimpleFightBehaviour.Idle, new InstanceCollisionMatcher(character));
		whenDeadGoToIdle = new LiveCharacterTransition(
					SimpleFightBehaviour.Idle, character, LiveState.Dead);
	}
	private void setupPursuitTransitions(GameCharacter character) {
		Transition isCharacterLive = new LiveCharacterTransition(
				null, character, LiveState.Live);
		Transition isTargetLive           = new LiveCharacterTransition(null, target);
		Transition onApproachTransition   = new DistanceTransition(SimpleFightBehaviour.Pursue, 
				character, target, enterPursuingDistance);
		
		pursuitIfAlive = new AndTransition(SimpleFightBehaviour.Pursue, 
					isTargetLive, isCharacterLive, onApproachTransition);
	}


	private void setupPursuitAttackTransitions(GameCharacter character) {
		whenTargetDieTakeAWalk = new LiveCharacterTransition(SimpleFightBehaviour.MovingAround, target, LiveState.Dead);
		whenTargetRunAwayGoToIdle = new DistanceTransition(SimpleFightBehaviour.Idle, 
				character, target, leavingPursuingDistance, DistanceType.Depart);
	}
	private void setupPrepareAttackTransitions(GameCharacter character){
		whenClosePrepareAttack = new DistanceTransition(SimpleFightBehaviour.PreparingAttack, 
				character, target, enterPrepareAttackDistance, DistanceType.Approach);
		whenFarPursuit = new DistanceTransition(SimpleFightBehaviour.Pursue, 
				character, target, leavePrepareAttackDistance, DistanceType.Depart);
		AttackDistanceTransition whenReachAttackDistanceFight = new AttackDistanceTransition(
				SimpleFightBehaviour.Fighting, character, target);
		whenReachAttackDistanceFight.setDistanceOffset(reachAttackDistanceOffset);
		this.whenReachAttackDistanceFight =whenReachAttackDistanceFight;
		whenLeaveAttackDistancePrepare = new AttackDistanceTransition(
				SimpleFightBehaviour.PreparingAttack, character, target, DistanceType.Depart);
	}
	private void setupFightTransitions(GameCharacter character) {
		onReachAttackDistance = new AttackDistanceTransition(SimpleFightBehaviour.Fighting, 
				character, target); 
		onReachAttackDistance.setDistanceOffset(-0.3f);
		
	}


	private Map<Object, State<? extends GameCharacter>> buildBehaviorStateMap() {
		Map<Object, State<? extends GameCharacter>> behaviorMap = new HashMap<Object, State<? extends GameCharacter>>();
		
		behaviorMap.put(SimpleFightBehaviour.Idle, idleState);
		behaviorMap.put(SimpleFightBehaviour.MovingAround, movingAroundState);
		behaviorMap.put(SimpleFightBehaviour.Pursue, pursuingState);
		behaviorMap.put(SimpleFightBehaviour.Fighting, fightState);
		behaviorMap.put(SimpleFightBehaviour.PreparingAttack, prepareAttackState);
		return behaviorMap;
	}
}
