package com.hdv.abyss.ai.controlstates;

import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance;
import com.badlogic.gdx.ai.steer.behaviors.PrioritySteering;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.ai.GameCharacterSteerable;
import com.hdv.abyss.ai.steering.PursueProximity;
import com.hdv.abyss.ai.steering.SteerableUtils;
import com.hdv.abyss.ai.steering.TolerantArrive;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.events.MovementEvent;
import com.hdv.abyss.statemachine.states.BaseState;

public class PursuingState extends BaseState<GameCharacter>{
	private GameCharacterSteerable ownerSteerable;
	private Steerable<Vector2> targetSteerable;
	private GameCharacter owner;
	private SteeringBehavior<Vector2> behaviour;
	private Proximity<Vector2> proximity;

	public PursuingState(StateMachine<GameCharacter> machine) {
		this(machine, null);
	} 
	public PursuingState(StateMachine<GameCharacter> stateMachine, GameCharacter target) {
		this(stateMachine,target, null);
	} 
	public PursuingState(StateMachine<GameCharacter> machine, GameCharacter target, Proximity<Vector2> proximity) {
		super(machine);
		
		owner = machine.getEntity();
		ownerSteerable = new GameCharacterSteerable(owner);
		targetSteerable =  SteerableUtils.getSteerable(target);
		
		owner.setUserObject(ownerSteerable);
		
		this.proximity = proximity;
	}
	
	@Override
	public boolean onStart(GameEvent startEvent) {
		if(proximity == null){
			this.proximity = new PursueProximity(
					ownerSteerable
					, targetSteerable
					, owner.getPhysicalWorld().getWorld()
					, ownerSteerable.getBoundingRadius()*1.5f
					, MathUtils.degreesToRadians * 45);
		}
		if(behaviour == null){
			setupBehavior();
		}
		ownerSteerable.setSteeringBehavior(behaviour);
		behaviour.setEnabled(true);
		return true;
	}
	private void setupBehavior() {
		PrioritySteering<Vector2> prioritySteeringSB = new PrioritySteering<Vector2>(
				ownerSteerable, 0.0001f);
		prioritySteeringSB.add(new CollisionAvoidance<Vector2>(ownerSteerable, proximity));
		prioritySteeringSB.add(new TolerantArrive<Vector2>(
				ownerSteerable, targetSteerable, 1.8f, MathUtils.PI*0.75f) );
		
		behaviour = prioritySteeringSB;
	} 
	
	@Override
	public void onStop() {
		super.onStop();
		stopOwner();
		behaviour.setEnabled(false);
	}
	private void stopOwner() {
		owner.fireEvent(new MovementEvent(Vector2.Zero, Movement.NORMAL));
	}
	@Override
	public void update(float delta) {
		super.update(delta);			
		ownerSteerable.update(delta);
	}
		
}

