package com.hdv.abyss.ai.controlstates;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.weapon.Weapon;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.transitions.BaseTransition;
import com.hdv.abyss.statemachine.transitions.DistanceTransition.DistanceType;

public class AttackDistanceTransition extends BaseTransition {
	
	private GameCharacter owner;
	private PhysicsActor target;
	private DistanceType distanceType;
	private float distanceOffset;
	public AttackDistanceTransition(Object nextStateKey,
			GameCharacter character, PhysicsActor target) {
		this(nextStateKey, character, target,DistanceType.Approach);
	}
	public AttackDistanceTransition(Object nextStateKey,
			GameCharacter character, PhysicsActor target, DistanceType distanceType) {
		super(nextStateKey);
		this.owner = character;
		this.target = target;
		this.distanceType = distanceType;
		this.distanceOffset = 0f;
	}

	

	public float getDistanceOffset() {
		return distanceOffset;
	}
	public void setDistanceOffset(float distance) {
		distanceOffset = distance;
	}
//	@Override
//	public boolean check(GameEvent event) {
//		if(getOwner().getWeapon() == null){
//			return false;
//		}
//		boolean canReach = getOwner().getWeapon().canReach(getOwner(), getTarget(), distanceOffset);
//		
//		boolean checkTransition = distanceType == DistanceType.Approach 
//												? canReach 
//												: !canReach;
//		
////		Gdx.app.log(getClass().getName(), "Transition to " + getNextStateKey() + "? " + checkTransition);
//		
//		return checkTransition;
//	}
//	@Override
//	public boolean check(GameEvent event) {
//		Weapon weapon = getOwner().getWeapon();
//		if(weapon == null){
//			return false;
//		}
//		float attackDistance = weapon.getAttackReach();
//		float distance =Vectors.distance(getOwner().getPositionInWorld(), getTarget().getPositionInWorld());
//		distance -= (getOwner().getSizeInWorld().len()/2 + getTarget().getSizeInWorld().len()/2);
////		boolean canReach = attackDistance + distanceOffset > distance;
//		boolean canReach = attackDistance + distanceOffset > distance;
//		
//		boolean checkTransition = distanceType == DistanceType.Approach 
//												? canReach 
//												: !canReach;
//		
////		Gdx.app.log(getClass().getName(), "Transition to " + getNextStateKey() + "? " + checkTransition);
//		
//		return checkTransition;
//	}
	@Override
	public boolean check(GameEvent event) {
		Weapon weapon = getOwner().getWeapon();
		if(weapon == null){
			return false;
		}
		boolean canReach = weapon.reachAttackPosition(getOwner(),getTarget(), distanceOffset);
		
		boolean checkTransition = distanceType == DistanceType.Approach 
												? canReach 
												: !canReach;
		
		return checkTransition;
	}

	private PhysicsActor getTarget() {
		return target;
	}

	private GameCharacter getOwner() {
		return owner;
	}

}
