package com.hdv.abyss.ai.controlstates;

public enum SimpleFightBehaviour{
	Pursue, Idle, MovingAround, Fighting, PreparingAttack
}