package com.hdv.abyss.ai;

import com.badlogic.gdx.utils.TimeUtils;
import com.hdv.abyss.actors.CharacterController;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.ai.controlstates.SimpleFightBehaviourStateBuilder;
import com.hdv.abyss.statemachine.BaseStateMachine;
import com.hdv.abyss.statemachine.StateMapBuilder;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.TimeEvent;

public class BehaviourController implements CharacterController{
	private BaseStateMachine<GameCharacter> behaviourStateMachine;
	public BehaviourController(GameCharacter characterToControl, GameCharacter targetCharacter) {
		this(characterToControl, targetCharacter, new SimpleFightBehaviourStateBuilder(targetCharacter));
	}
	public BehaviourController(GameCharacter characterToControl
							, GameCharacter targetCharacter
							, StateMapBuilder<GameCharacter> builder) 
	{
		behaviourStateMachine = new BaseStateMachine<GameCharacter>(characterToControl);
		if(builder != null){
			behaviourStateMachine.setStates(builder);
		}
	}

	
	@Override
	public void setup(GameCharacter gameCharacter) {
		// Do nothing (?)
	}
	@Override
	public void update(GameCharacter character, float delta){
		this.update(delta);
		
	}
	public void update(float delta){
		GameCharacter characterControlled = behaviourStateMachine.getEntity();
		if(characterControlled != null && characterControlled.isActive()){
			behaviourStateMachine.update(delta);
			//FIXME: gambiarra para atualizar transições
			behaviourStateMachine.fireEvent(new TimeEvent(TimeUtils.millis()));
		}
	}
	@Override
	public void onEvent(GameCharacter gameCharacter, GameEvent event) {
		behaviourStateMachine.fireEvent(event);
	}
}
