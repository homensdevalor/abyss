package com.hdv.abyss.ai;

import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.events.MovementEvent;

public class GameCharacterSteerable extends BaseSteerable{
	private GameCharacter character;
	private Float boundingRadius;
	public GameCharacterSteerable(GameCharacter character) {
		this.character = character;
		boundingRadius = null;

		super.setMaxLinearSpeed(100);
		super.setMaxLinearAcceleration(100);
		
		super.setUserData(character);
	}
	
	@Override
	protected void applySteering(SteeringAcceleration<Vector2> steering, float deltaTime) {
		Vector2 newVelocity = steering.linear; // Na verdade este valor corresponde a aceleração (?)
		Vector2 currentVelocity = getLinearVelocity();
		if(moveHasChange(newVelocity, currentVelocity)){
			Vector2 direction = new Vector2(newVelocity).nor();

			MovementEvent moveEvent = new MovementEvent(direction, Movement.NORMAL);
			
			character.fireEvent(moveEvent);
		}
	}

	private boolean moveHasChange(Vector2 newLinearVelocity, Vector2 currentVelocity) {
		return (newLinearVelocity.cpy().sub(currentVelocity).len() != 0);
	}

	@Override
	public Vector2 getPosition() {
		return character.getBody().getPosition();
	}

//	private Vector2 calculateCharacterDirection(){
//		Vector2 dirVector = character.getVelocity().len2() > 0 ? character.getVelocity()
//															   : character.getDirection().toVector();
//		
//		return dirVector.nor();
//	}
	
	@Override
	public float getOrientation() {
//		return vectorToAngle(calculateCharacterDirection()); 
//		return vectorToAngle(character.getMoveDirection()); 
		return vectorToAngle(character.getMoveDirection()); 
	}

	@Override
	public Vector2 getLinearVelocity() {
		return character.getVelocity();
	}

	@Override
	public float getAngularVelocity() {
		return 0;
	}

	@Override
	public float getBoundingRadius() {
		if(boundingRadius == null){
			boundingRadius = calculateBoundingRadius();
		}
		return boundingRadius;
	}

	private Float calculateBoundingRadius() {
		double diameter = 0;
		//Calcula o diâmetro, considerando a bound box do personagem inscrita em um círculo
		//Ou seja, toda a bound box estaria contida dentro de um círculo que tenha o diâmetro calculado, 
		//e os cantos da bound box tocariam este círculo.
		//Utiliza teorema de pitágoras para calcular este diâmetro
		diameter = Math.sqrt((character.getWidth() * character.getWidth()) 
							+ (character.getHeight() * character.getHeight()));
		
		return ((float)diameter)/2f;
	}

}
