package com.hdv.abyss.scenes;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

public class BaseStage extends Stage {
	public BaseStage() {
		super();
	}
	public BaseStage(Viewport viewport, Batch batch) {
		super(viewport, batch);
	}
	public BaseStage(Viewport viewport) {
		super(viewport);
	}
	
	private ScreenController screenController;
	private Screen screen;
	
	public ScreenController getScreenController() {
		return screenController;
	}
	public void setScreenController(ScreenController screenController) {
		this.screenController = screenController;
	}
	public Screen getScreen() {
		return screen;
	}
	public void setScreen(Screen screen) {
		this.screen = screen;
	}
}
