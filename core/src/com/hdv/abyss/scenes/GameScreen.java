package com.hdv.abyss.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.configurators.SlimeBuilderConfigurator;
import com.hdv.abyss.actors.configurators.SpikeConfigurator;
import com.hdv.abyss.scenes.maps.GameMap;
import com.hdv.abyss.utils.Counter;

public class GameScreen extends BaseScreen{
	private static final String CHARACTERS_LAYER_NAME = "personagens";
	private static final String COLLISION_LAYER_NAME = "Colisões";
    private OrthographicCamera mapCamera;

    private GameStage gameStage;
    private HUDStage hudStage;

    private Texture darknessEffect;
    private Sprite darknessEffectSprite;
    
    private Texture fadeEffect;
    private Sprite fadeEffectSprite;
	private Counter fadeCounter;
    
    private SpriteBatch batch;
    
    private GameMap gameMap;
    
    public GameScreen() {    	
    	this(null);
	}
    public GameScreen(ScreenController screenController){
    	super(screenController);
    }
	protected void loadResources() {
		mapCamera = new OrthographicCamera(Configurations.GAME_VIEWPORT_X,Configurations.GAME_VIEWPORT_Y);
        mapCamera.update();
        
    	gameMap = new GameMap();
    	gameMap.setCamera(mapCamera);
        gameMap.loadMap(new TmxMapLoader(new InternalFileHandleResolver()), "maps/mapa-trap.tmx");
        gameMap.setCollisionLayer(COLLISION_LAYER_NAME);
        gameMap.setCharactersLayer(CHARACTERS_LAYER_NAME);
        
        setupMapCharacterBuilders(gameMap);

        darknessEffect = new Texture(Gdx.files.internal("effects/escuridão.png"));
        darknessEffectSprite = new Sprite(darknessEffect);
        
        fadeEffect = new Texture(Gdx.files.internal("effects/preto.png"));
        fadeEffectSprite = new Sprite(fadeEffect);
        fadeEffectSprite.setAlpha(0);
//        fadeEffectSprite.setSize(Configurations.GAME_VIEWPORT_X,Configurations.GAME_VIEWPORT_Y);
        fadeEffectSprite.setSize(Configurations.APP_WIDTH, Configurations.APP_WIDTH);
        fadeCounter = new Counter();
        
        batch = new SpriteBatch();

		darknessEffectSprite.setSize(Configurations.GAME_VIEWPORT_X,Configurations.GAME_VIEWPORT_Y);
	}
    
    private void setupMapCharacterBuilders(GameMap gameMap) {
		gameMap.addCharacterBuilder( "slime", new GameCharacterBuilder(new SlimeBuilderConfigurator()));
		gameMap.addCharacterBuilder( "spike", new GameCharacterBuilder(new SpikeConfigurator()));
	}
	@Override
    public void onCreate() {
		super.onCreate();
		
        loadResources();
        
        gameStage = new GameStage();
        hudStage = new HUDStage(gameStage.getPlayerCharacter());

        gameStage.setScreen(this);
        gameStage.setScreenController(getScreenController());
        hudStage.setScreen(this);
        hudStage.setScreenController(getScreenController());
        
        gameMap.setCharactersStage(gameStage);
        gameMap.setup();
        
        darknessEffectSprite.setAlpha(0.5f);
    }

	@Override
	public void update(float delta) {
		super.update(delta);
		this.gameStage.act(delta);
		this.hudStage.act(delta);
        updateCamera(); 
        
        if(fadeCounter.isCounting()){
        	if(!fadeCounter.finishedCount()){
        		float alpha =fadeCounter.getCountedTime()/(float)(fadeCounter.getTimeToCount());
        		
        		fadeEffectSprite.setAlpha(alpha);
        	}
        }
	}
	@Override
	public void draw(float delta) {
		super.draw(delta);
		
		gameMap.draw();
		
        batch.begin();
        darknessEffectSprite.draw(batch);
        fadeEffectSprite.draw(batch);
        batch.end();

        this.hudStage.draw();
        
	}
	
	
	private void updateCamera() {
		mapCamera.position.set(gameStage.getPlayerCharacter().getX() + gameStage.getPlayerCharacter().getWidth()/2f 
        		,gameStage.getPlayerCharacter().getY() + gameStage.getPlayerCharacter().getHeight()/2f, mapCamera.position.z);
        mapCamera.update();
	}

	@Override
	public void resize(int width, int height) {
		gameStage.getViewport().update(width, height);
		hudStage.getViewport().update(width, height);
		darknessEffectSprite.setSize(width, height);
		fadeEffectSprite.setSize(width, height);
	}

	@Override
	public void dispose() {
		darknessEffect.dispose();
		fadeEffect.dispose();
	}
	
	public void fadeToBlack(long timeMillis){
		fadeCounter.startCount(timeMillis);
	}
}
