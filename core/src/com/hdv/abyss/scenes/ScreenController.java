package com.hdv.abyss.scenes;

import com.badlogic.gdx.Screen;

public interface ScreenController {
	Screen getCurrentScreen();
	void changeToScreen(Object screenKey);
}
