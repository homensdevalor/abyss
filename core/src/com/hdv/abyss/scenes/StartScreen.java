package com.hdv.abyss.scenes;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.hdv.abyss.AbyssGame.GameScreens;
import com.hdv.abyss.Resources;
import com.hdv.abyss.input.InputController;

public class StartScreen extends BaseScreen{	
	class MenuInputController extends InputAdapter{
		@Override
		public boolean keyUp(int keycode) {
			if(keycode == Keys.ENTER){
				if(itemSelect == 0){
					startGame();
				}
				else if(itemSelect == 1){
					exitGame();
				}
			}
			else if(keycode == Keys.ESCAPE){
				exitGame();
			}
			return super.keyUp(keycode);
		}

		@Override
		public boolean keyDown(int keycode) {
			if(keycode == Keys.DOWN){
				moveSelectorDown();
			}
			else if(keycode == Keys.UP){
				moveSelectorUp();
			}
			return super.keyUp(keycode);
		}
	}

	StartStage startStage;
	BitmapFont bigFont, font;
	
	Texture selectedIndicatorTexture;
	int itemSelect;
	List<Actor> selectors;
	final MenuInputController menuController;
	Music menuMusic;

	public StartScreen(ScreenController screenController) { 
    	super(screenController);
		menuController = new MenuInputController();
    }

	protected void loadResources() {
		setupFonts();
    	itemSelect = 0;
    	selectors = new ArrayList<Actor>();
    	selectedIndicatorTexture = new Texture(Gdx.files.internal("sprites/seletor.png"));
	}

    @Override
    public void onCreate() {
    	super.onCreate();
		loadResources();

    	startStage = new StartStage();
		layoutMenu((int)startStage.getViewport().getWorldWidth(), (int)startStage.getViewport().getWorldHeight());
		
		setupMusic();
    }
    
    @Override
    public void pause() {
    	super.pause();
    	InputController.instance().removeInputListener(menuController);
    	menuMusic.pause();
    }
    @Override
    public void resume() {
    	super.resume();

    	InputController.instance().addInputListener(menuController);
    	menuMusic.play();
    }
    @Override
	public void update(float delta) {
		startStage.act();
	}
	@Override
	public void draw(float delta) { 
		super.draw(delta);
		startStage.draw();
    }

	@Override
	public void resize(int width, int height) {
		if(startStage != null){
			startStage.getViewport().update(width, height);
		}
	}
	@Override
	public void dispose() {
		startStage.dispose();
	}
	
	private void moveSelectorUp() {
		moveSelectorTo(itemSelect + 1);
	}

	private void moveSelectorDown() {
		moveSelectorTo(itemSelect - 1);
	}
	private void moveSelectorTo(int index){
		if(selectors.isEmpty()){
			return;
		}
		
		selectors.get(itemSelect).setVisible(false);
		itemSelect = index;
		if(itemSelect >= selectors.size()){
			itemSelect = 0;
		}
		else if(itemSelect < 0){
			itemSelect = selectors.size() -1;
			
		}
		selectors.get(itemSelect).setVisible(true);
	}
	
	private void exitGame() {
		Gdx.app.exit();
	}
	private void startGame() {
		getScreenController().changeToScreen(GameScreens.GameScreen);
	}
	

	private void setupFonts() {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(Resources.titleFont));
		
		FreeTypeFontParameter paramBig = new FreeTypeFontParameter();
		paramBig.size =64;
		bigFont = generator.generateFont(paramBig);
		
		FreeTypeFontParameter param = new FreeTypeFontParameter();
		param.size =32;
		font = generator.generateFont(param);
	}

	private void setupMusic() {
		menuMusic = Gdx.audio.newMusic(Gdx.files.internal("musics/AbyssTitleScreenV1.ogg")); 
		menuMusic.setLooping(true);
		menuMusic.setVolume(0.4f);
		menuMusic.play();
	}
	private void layoutMenu(int width, int height) {
		TextButtonStyle txtBtnStyle =  getSkin().get("transparentButton", TextButtonStyle.class);
		LabelStyle labelStyle =  getSkin().get(LabelStyle.class);
		labelStyle.font = bigFont;
		txtBtnStyle.font = font;
		Table uiGroup = new Table(getSkin());
		uiGroup.setFillParent(true);
        Label titleLabel = new Label( "Abyss", getSkin() );
        
        TextButton startGameButton = new TextButton( "Start", txtBtnStyle );
        TextButton exitButton = new TextButton( "Exit", txtBtnStyle );

        uiGroup.add(titleLabel).padBottom(20).row();
        
        Actor selector1, selector2;
        selector1 = new Image(selectedIndicatorTexture);
        selector2 = new Image(selectedIndicatorTexture);
        selector2.setVisible(false);
        selectors.add(selector1);
        selectors.add(selector2);
        
        Table group1 = new Table();
        group1.add(selector1).padRight(10);
        group1.add(startGameButton).row();
        group1.add(selector2).padRight(10);
        group1.add(exitButton).row();
        uiGroup.add(group1).row();
        
        startStage.addActor(uiGroup);
	}

	private Skin skin;
	private Skin getSkin() {
		if(skin == null){
			skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
		}
		return skin;
	}

}
