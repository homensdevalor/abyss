package com.hdv.abyss.scenes.maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapRenderer;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.entities.Collider;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.physics.PhysicalWorld;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.scenes.GameStage;

public class GameMap {
	private Map map;
	float unitScale = 1f/ Configurations.TILE_IN_PIXELS;
    MapRenderer mapRenderer;
	private String collisionLayerName;
	private String charactersLayerName;
    private GameStage charactersStage;
    private OrthographicCamera mapCamera;
    
    private int[] backgroundLayers;
    private int[] foregroundLayers;
    
    private final java.util.Map<String, GameCharacterBuilder> characterBuilders;
    
    public GameMap() {
    	characterBuilders = new HashMap<String, GameCharacterBuilder>();
	}
    
	public void setCamera(OrthographicCamera mapCamera) {
		this.mapCamera = mapCamera;
	}

	public void loadMap(TmxMapLoader tmxMapLoader, String mapFilename) {
		TiledMap map = tmxMapLoader.load(mapFilename);
		mapRenderer =  new OrthogonalTiledMapRenderer(map, unitScale);
		this.map = map;
	}

	public void setUnitScale(float unitScale) {
		this.unitScale = unitScale;
	}

	public void setCollisionLayer(String collisionLayerName) {
		this.collisionLayerName = collisionLayerName;
	}
	public void setCharactersLayer(String charactersLayerName) {
		this.charactersLayerName = charactersLayerName;
	}

	public void setCharactersStage(GameStage stage) {
		this.charactersStage = stage;
	}

	public void setup() {
		setupLayers();
		setupCharacters();
		setupMapColliders();
	}

//	private Collection<MapObject> getLayerObjects(){
//    	List<MapObject> mapObjects = new ArrayList<MapObject>();
//    	Iterator<MapObject> mapObjectsIterator = map.getLayers().get(collisionLayerName).getObjects().iterator();
//    	while(mapObjectsIterator.hasNext()){
//    		MapObject object = mapObjectsIterator.next();
//    		mapObjects.add(object);
//    	}
//    	
//    	return mapObjects;
//	}
//
	private Shape2D getMapObjectShape(MapObject object) {
		Shape2D shape = null;
		if(object instanceof RectangleMapObject){
			RectangleMapObject rectObj = (RectangleMapObject)object;
			shape = rectObj.getRectangle();
		}
		else if(object instanceof CircleMapObject){
			shape = ((CircleMapObject)object).getCircle();
		}
		else if(object instanceof EllipseMapObject){
			shape = ((EllipseMapObject)object).getEllipse();
		}
		else if(object instanceof PolygonMapObject){
			shape = ((PolygonMapObject)object).getPolygon();
		}
		else if(object instanceof PolylineMapObject){
			shape = ((PolylineMapObject)object).getPolyline();
		}
		else{
			Gdx.app.log(getClass().getName(), "Object " + object + " of unsupported type: " 
							+ object.getClass().getName());
		}
		
		return shape;
	}
	private void setupMapColliders() {
    	Iterator<MapObject> mapObjectsIterator = map.getLayers().get(collisionLayerName).getObjects().iterator();
    	
    	while(mapObjectsIterator.hasNext()){
    		MapObject object = mapObjectsIterator.next();
    		PhysicsActor collider = tryCreateCollider(object);
    		if(collider != null){
    			charactersStage.getPhysicalWorld().addActor(collider);
    		}
    	}
	}
	private PhysicsActor tryCreateCollider(MapObject object) {
		if(object instanceof RectangleMapObject){
			RectangleMapObject rectObj = (RectangleMapObject)object;
			Rectangle rect = convertToWorldCoordinates(rectObj.getRectangle());
			return new Collider(rect.getPosition(new Vector2()), rect.getSize(new Vector2()));
		}
		return null;
	}

	public void setMapCollidersInPixels(List<Rectangle> colliders) {
		PhysicalWorld physicalWorld = charactersStage.getPhysicalWorld();
		for(Rectangle boundBox : colliders){	
	        Vector2 groundPos = physicalWorld.convertToMeters(new Vector2(boundBox.getX(), boundBox.getY()));
	        Vector2 groundSize = physicalWorld.convertToMeters(new Vector2(boundBox.getWidth(),boundBox.getHeight()));
	        
	        physicalWorld.addActor(new Collider(groundPos, groundSize)); 
		}
	}

	private void setupCharacters() {
    	Iterator<MapObject> mapObjectsIterator = map.getLayers().get(charactersLayerName).getObjects().iterator();
    	while(mapObjectsIterator.hasNext()){
    		MapObject object = mapObjectsIterator.next();
    		
    		Gdx.app.log(getClass().getName(), "Found object at characters layer: " 
    		+ object + " with name: " + object.getName());
    		
    		if(object.getName()!= null && !object.getName().isEmpty()){
        		
    			GameCharacterBuilder characterBuilder = this.characterBuilders.get(object.getName());
    			

        		Gdx.app.log(getClass().getName(), "Character Builder: " + characterBuilder);
    			
    			if(characterBuilder != null){
	    			Vector2 pos = getObjectPosition(object);

	        		Gdx.app.log(getClass().getName(), "Character pos: " + pos);
	        		
	    			GameCharacter character = characterBuilder.build(pos);
	    			charactersStage.getPhysicalWorld().addActor(character, pos);
    			}
    		}
    	}
	}
	private Vector2 getObjectPosition(MapObject object) {
		Vector2 pos = new Vector2();
		Shape2D shape = getMapObjectShape(object);
		
		if(shape instanceof Rectangle){
			Rectangle rect = (Rectangle)shape;
			pos = charactersStage.getPhysicalWorld().convertToMeters(rect.getPosition(pos));
		}
		else if(shape instanceof CircleMapObject){
			Circle circle = (Circle)shape;

			pos.x = charactersStage.getPhysicalWorld().convertToMeters(circle.x);
			pos.y = charactersStage.getPhysicalWorld().convertToMeters(circle.y);
		}
		return pos;
	}

	private Rectangle convertToWorldCoordinates(Rectangle rectMap) {
		PhysicalWorld physicalWorld = this.charactersStage.getPhysicalWorld();
		Rectangle worldRectangle = new Rectangle();
		worldRectangle.setX(physicalWorld.convertToMeters(rectMap.getX()));
		worldRectangle.setY(physicalWorld.convertToMeters(rectMap.getY()));
		worldRectangle.setWidth(physicalWorld.convertToMeters(rectMap.getWidth()));
		worldRectangle.setHeight(physicalWorld.convertToMeters(rectMap.getHeight()));
		return worldRectangle;
	}

	private void setupLayers() {
    	List<Integer> backgroundLayers = new ArrayList<Integer>(),
    			      foregroundLayers = new ArrayList<Integer>();
    	
    	
    	boolean foundCharacterLayer = false;
    	Iterator<MapLayer> layersIterator = map.getLayers().iterator();
    	int index = 0;
    	while(layersIterator.hasNext()){
    		String layerName = layersIterator.next().getName();
    		if(!layerName.equals(this.collisionLayerName)){
        		if(!foundCharacterLayer){
        			if(!layerName.equals(this.charactersLayerName)){
        				backgroundLayers.add(index);
        			}
        			else{
        				foundCharacterLayer = true;
        			}
        		}
        		else{
        			foregroundLayers.add(index);
        		}
    		}
    		++index;
    	}

    	this.backgroundLayers = new int[backgroundLayers.size()];
    	for(int i=0; i < backgroundLayers.size(); ++i){
    		this.backgroundLayers[i] = backgroundLayers.get(i);
    	}
    	this.foregroundLayers = new int[foregroundLayers.size()];
    	for(int i=0; i < foregroundLayers.size(); ++i){
    		this.foregroundLayers[i] = foregroundLayers.get(i);
    	}
	}

	public void draw() {
		if(mapCamera != null){
			mapRenderer.setView(mapCamera);
		}
		mapRenderer.render(backgroundLayers);
        this.charactersStage.draw();
        mapRenderer.render(foregroundLayers); 
	}

	public void addCharacterBuilder(String characterName, GameCharacterBuilder gameCharacterBuilder) {
		this.characterBuilders.put(characterName, gameCharacterBuilder);
	}

}
