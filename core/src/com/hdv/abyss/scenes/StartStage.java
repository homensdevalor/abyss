package com.hdv.abyss.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.Resources;

public class StartStage extends Stage{
    Texture startBackground;
    Sprite startBackgroundSprite;
    
    SpriteBatch batch;
    
    public StartStage() {
    	super(new FitViewport(Configurations.APP_WIDTH * 2, Configurations.APP_HEIGHT * 2));

        startBackground = new Texture(Gdx.files.internal(Resources.titleBackground));
        startBackgroundSprite = new Sprite(startBackground);
        batch = new SpriteBatch();

		startBackgroundSprite.setSize(Configurations.APP_WIDTH, Configurations.APP_HEIGHT);
	}
    
	@Override
	public void draw() {
        batch.begin();
        startBackgroundSprite.draw(batch);
        batch.end();
		super.draw(); 
	}
	@Override
	public void dispose() {
		super.dispose();
		startBackground.dispose();
	}
}