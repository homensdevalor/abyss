package com.hdv.abyss.scenes.utils;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.hdv.abyss.utils.Actors;

public class CameraTracker extends Actor{
	private Camera camera;
	private Actor trackedActor;
	private boolean tracking;
	public CameraTracker(Camera camera, Actor trackedActor) {
		super();
		this.camera = camera;
		this.trackedActor = trackedActor;
		this.tracking = true;
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(isTracking()){ //TODO: talvez permitir um modo que a câmera não siga imediatamente (steering ?)
			centerAt(trackedActor);
		}
	}

	private void centerAt(Actor trackedActor) {
		Vector2 center = Actors.getCenter(trackedActor);
		camera.position.set(center.x, center.y, camera.position.z);
	}

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public Actor getTrackedActor() {
		return trackedActor;
	}

	public void setTrackedActor(Actor trackedActor) {
		this.trackedActor = trackedActor;
	}
	
}
