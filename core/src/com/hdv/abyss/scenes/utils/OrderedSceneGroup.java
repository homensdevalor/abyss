package com.hdv.abyss.scenes.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

/** Tipo de grupo ({@link Group}) que renderiza seus atores filhos de acordo com um 
 *  Comparator<Actor> ({@link Comparator}), em ordem ascendente ou decrescente.
 *  A ordenação pode levar a delays, então adicione a ele apenas os atores necessários.*/
public class OrderedSceneGroup extends Group{
	private List<Actor> orderedChildren;
	private boolean ascedingOrder;
	private Comparator<Actor> comparator;
	public OrderedSceneGroup(Comparator<Actor> actorComparator) {
		this(actorComparator, true);
	}
	public OrderedSceneGroup(Comparator<Actor> actorComparator, boolean ascendingOrder) {
		super();
		orderedChildren = new ArrayList<Actor>();
		this.ascedingOrder = ascendingOrder;
		this.comparator = actorComparator;
	}
	
	@Override
	public void addActor(Actor actor) {
		super.addActor(actor);
		orderedChildren.add(actor);
	}
	@Override
	public void addActorAfter(Actor actorAfter, Actor actor) {
		super.addActorAfter(actorAfter, actor);
		orderedChildren.add(actor);
	}
	@Override
	public void addActorAt(int index, Actor actor) {
		super.addActorAt(index, actor);
		orderedChildren.add(actor);
	}
	@Override
	public void addActorBefore(Actor actorBefore, Actor actor) {
		super.addActorBefore(actorBefore, actor);
		orderedChildren.add(actor);
	}
	@Override
	public void clearChildren() {
		super.clearChildren();
		orderedChildren.clear();
	}
	@Override
	public void clear() {
		super.clear();
		orderedChildren.clear();
	}
	@Override
	public boolean removeActor(Actor actor) {
		orderedChildren.remove(actor);
		return super.removeActor(actor);
	}
	
	public boolean isAscedingOrder() {
		return ascedingOrder;
	}
	public void setAscedingOrder(boolean ascedingOrder) {
		this.ascedingOrder = ascedingOrder;
	}
	
	@Override
	protected void drawChildren(Batch batch, float parentAlpha) {
		//TODO: buscar forma de ordenar mais eficiente
		/*Não é possível utilizar coleções ordenadas (ex.: SortedSet, 
		 * pois elas só são ordenadas durante inserção e remoção*/
		Collections.sort(this.orderedChildren, this.comparator);
		for(Actor actor : orderedChildren){
			actor.draw(batch, parentAlpha);
		}
	}
}
