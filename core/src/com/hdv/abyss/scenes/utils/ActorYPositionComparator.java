package com.hdv.abyss.scenes.utils;

import java.util.Comparator;

import com.badlogic.gdx.scenes.scene2d.Actor;

/** Classe que permite comparar atores (implementa Comparator) a partir de sua posição no eixo Y.
 * Comparação pode considerar ordem de cima para baixo ou de baixo para cima.*/
public class ActorYPositionComparator implements Comparator<Actor>{
	private static final int EQUALS = 0;
	private static final int FIRST_LOWER = -1;
	private static final int FIRST_GREATER = 1;
	
	/** Estratégia de ordenação. 
	 * Se TopToBottom, então elementos mais no topo serão os primeiros (em uma ordenação ascendente);
	 * Caso contrário, elementos mais abaixo serão os primeiros (em uma ordenação ascendente)*/
	public enum OrderOrientation{TopToBottom, BottomToTop};
	private OrderOrientation orderOrientation;
	public ActorYPositionComparator() {
		this(OrderOrientation.TopToBottom);
	}
	public ActorYPositionComparator(OrderOrientation orderOrientation) {
		this.orderOrientation = orderOrientation;
	}
	/*Returns a negative integer, zero, or a positive integer 
	 * as the first argument is less than, equal to, or greater than the second.*/
	@Override
	public int compare(Actor actor1, Actor actor2) {
		if(actor1.equals(actor2)){
			return EQUALS;
		}
		int comparation= FIRST_LOWER;
		
		//Eixo Y no libgdx inicia na parte inferior da tela, então por padrão ordena BottomToTop . 
		if(actor1.getY() < actor2.getY()){
			comparation = FIRST_LOWER;
		}
		else if (actor1.getY() > actor2.getY()){
			comparation = FIRST_GREATER;
		}
		
		//Se orientação é TopToBottom, inverte valores
		if(orderOrientation == OrderOrientation.TopToBottom){
			comparation = comparation * -1;
		}
		
		return comparation;
	}

}
