package com.hdv.abyss.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class BaseScreen implements Screen{
	ScreenController screenController;
	boolean created;
	boolean paused;
	boolean showing;
	
	public BaseScreen(ScreenController screenController) {
		this.screenController = screenController;
		created = false;
		paused = true;
		showing = false;
	}
	
	public ScreenController getScreenController() {
		return screenController;
	}	
	
	public void onCreate()
	{
		created = true;
	}
	public boolean isCreated(){
		return created;
	}
	
	@Override
	public void render(float delta) {
		if(!isPaused()){
			update(delta);
			draw(delta);
		}
	}

	public boolean isPaused() {
		return paused;
	}

	public void draw(float delta)
	{
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }
	public void update(float delta)
	{}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
//		paused = false;
		showing = true;
	}

	@Override
	public void hide() {
		paused = true;
		showing = false;
		
	}

	@Override
	public void pause() {
		paused = true;
		showing = true;
		
	}

	@Override
	public void resume() {
		paused = false;
		showing = true;		
	}

	@Override
	public void dispose() {
		
	}

	public void onDestroy() {
		// TODO Auto-generated method stub
		
	}

}
