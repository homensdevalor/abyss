package com.hdv.abyss.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.Resources;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.input.InputController;
import com.hdv.abyss.ui.HealthBar;
import com.hdv.abyss.ui.MessageBox;
import com.hdv.abyss.ui.MessageController;

public class HUDStage extends BaseStage {
	protected static final int DEBUG_KEY = Keys.I;
	private Skin skin;
	private MessageBox messageBox;
	
	private GameCharacter playerCharacter;
	private Label debugText;
	private boolean debug;
	
	public HUDStage(GameCharacter playerCharacter) {
		super(new FitViewport(Configurations.APP_WIDTH, Configurations.APP_HEIGHT));
		this.playerCharacter = playerCharacter;
		debug = false;
		createLayout();
	}

	private final void createLayout() {
		
		final Table uiRoot = new Table(getSkin());
		uiRoot.setFillParent(true);
		Table internalTable = new Table(getSkin());
		
		createHealthBar(internalTable);
        messageBox = createMessageBox(internalTable);
        MessageController.instance().setMessageBox(messageBox);
        
        uiRoot.add(internalTable).expand().fill();
        this.addActor(uiRoot);
		
		InputController.instance().addInputListener(new InputAdapter(){
			@Override
			public boolean keyUp(int keycode) {
				if(keycode == Keys.SPACE){
					MessageController.instance().skip();
				}
				else if(keycode == DEBUG_KEY){
					debug = !debug;
					
					uiRoot.setDebug(debug, true);
					debugText.setVisible(debug);
				}
				return super.keyUp(keycode);
			}
		});
	}
	
	protected Actor createHealthBar(Table container) {
		HealthBar healthBar = new HealthBar();
		if(playerCharacter != null){
			healthBar.setHealth(this.playerCharacter.getHealthController());
		}
		container.add(healthBar).top().align(Align.topLeft).expand().padLeft(10).padTop(5);
		container.row();
		
		debugText = new Label("Fps: ", getSkin());
		debugText.setVisible(false);
		container.add(debugText).left().top().row();
		
		return healthBar;
	}

	
	@Override
	public void act(float delta) {
		super.act(delta);
		
		MessageController.instance().update(delta);
		if(debug){
			debugText.setText("" + Gdx.graphics.getFramesPerSecond());
		}
	}

	protected MessageBox createMessageBox(Table uiRoot) {
		
		MessageBox messageBox = new MessageBox(getSkin(),(int)getWidth(),(int)(getHeight()*0.3f));
		uiRoot.add(messageBox).bottom().width(getWidth())
        	  .expandX()
        	  .padBottom(5)
        	  .height(getHeight()*0.3f)
        	  .row();
		return messageBox;
	}

	private Skin getSkin() {
		if(skin == null){
			skin = new Skin(Gdx.files.internal(Resources.gameSkin));
		}
		return skin;
	}
}
