package com.hdv.abyss.scenes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class SpriteActor extends Actor{
	Sprite sprite;
	public SpriteActor(Texture texture) {
		this.sprite = new Sprite(texture);
	}
	public SpriteActor(Sprite sprite) {
		this.sprite = new Sprite(sprite);
	}
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		sprite.draw(batch, parentAlpha);
	}
}