package com.hdv.abyss.scenes;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.hdv.abyss.AbyssGame.GameScreens;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.LeverBuilder;
import com.hdv.abyss.actors.OpenDoorBuilder;
import com.hdv.abyss.actors.configurators.BossConfigurator;
import com.hdv.abyss.actors.configurators.DoorConfigurator;
import com.hdv.abyss.actors.configurators.LeverBuilderConfigurator;
import com.hdv.abyss.actors.configurators.OpenDoorConfigurator;
import com.hdv.abyss.actors.configurators.PlayerBuilderConfigurator;
import com.hdv.abyss.actors.configurators.SkeletonBuilderConfigurator;
import com.hdv.abyss.actors.configurators.SlimeBuilderConfigurator;
import com.hdv.abyss.actors.configurators.SpikeConfigurator;
import com.hdv.abyss.actors.entities.Collider;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.entities.InsideColliderNotifier;
import com.hdv.abyss.actors.entities.InsideColliderNotifier.InsideListener;
import com.hdv.abyss.actors.entities.LeverActor;
import com.hdv.abyss.actors.lever.states.LeverStates;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.ai.BehaviourController;
import com.hdv.abyss.ai.BehaviourManager;
import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.global.GlobalEventCollector;
import com.hdv.abyss.global.GlobalListener;
import com.hdv.abyss.global.accumulators.GlobalAccumulator;
import com.hdv.abyss.global.accumulators.GlobalDoorAccumulator;
import com.hdv.abyss.global.events.PauseEvent;
import com.hdv.abyss.global.events.WinEvent;
import com.hdv.abyss.global.events.type.GlobalEvents;
import com.hdv.abyss.global.events.type.LeverGlobalEvents;
import com.hdv.abyss.global.events.type.LeverSwitch;
import com.hdv.abyss.input.CharacterInputProcessor;
import com.hdv.abyss.input.GlobalInputProcessor;
import com.hdv.abyss.input.InputController;
import com.hdv.abyss.physics.PhysicalWorld;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.scenes.utils.ActorYPositionComparator;
import com.hdv.abyss.scenes.utils.ActorYPositionComparator.OrderOrientation;
import com.hdv.abyss.scenes.utils.CameraTracker;
import com.hdv.abyss.scenes.utils.OrderedSceneGroup;
import com.hdv.abyss.statemachine.events.StopEvent;

public class GameStage extends BaseStage implements GlobalListener{
	static final String LOG_TAG = GameStage.class.getName();
	static final float PLAYER_START_X = 49;
	static final float PLAYER_START_Y = 22;
	
	private GameCharacterBuilder doorBuilder;
	private GameCharacterBuilder openDoorBuilder;
	private GameCharacterBuilder playerBuilder;
	private GameCharacterBuilder slimeBuilder;
	private GameCharacterBuilder skeletonBuilder;
	private LeverBuilder leverBuilder;
	private GameCharacterBuilder spikeBuilder;
	private GameCharacterBuilder bossBuilder;
	
	private  GameCharacter player1;
    private CharacterInputProcessor playerInputProcessor1;
    private GlobalInputProcessor globalInputProcessor;

    InsideColliderNotifier winCollider;
    
    private CameraTracker cameraTracker;
	
    private Box2DDebugRenderer renderer;
    private boolean debugEnabled;
	
    private PhysicalWorld physicalWorld;

    private OrderedSceneGroup orderedSceneGroup;

    private boolean pausedGame;
    
    private Music music;
    
    private GameCharacter boss;
	
    public GameStage() {
    	super(new FitViewport(Configurations.GAME_VIEWPORT_X, Configurations.GAME_VIEWPORT_Y
    			, new OrthographicCamera(Configurations.GAME_VIEWPORT_X, Configurations.GAME_VIEWPORT_Y)));
        
    	physicalWorld = new PhysicalWorld();
    	orderedSceneGroup = new OrderedSceneGroup(new ActorYPositionComparator(OrderOrientation.TopToBottom));
        this.addActor(orderedSceneGroup);
    	physicalWorld.setActorsContainer(orderedSceneGroup);
    	
    	GlobalAccumulator accumulator = new GlobalDoorAccumulator(new GlobalEvent(GlobalEvents.MAIN_DOOR_ACCUMULATOR_SWITCH, LeverGlobalEvents.LEVER_PRESSED), 
    			GlobalEvents.LEVER_BOTTOM_LEFT,
    			GlobalEvents.LEVER_BOTTOM_RIGHT,
    			GlobalEvents.LEVER_TOP_LEFT,
    			GlobalEvents.LEVER_TOP_RIGHT);
    	
    	GlobalEventCollector.getInstance().addListener(accumulator, 
    			GlobalEvents.LEVER_BOTTOM_LEFT,
    			GlobalEvents.LEVER_BOTTOM_RIGHT,
    			GlobalEvents.LEVER_TOP_LEFT,
    			GlobalEvents.LEVER_TOP_RIGHT);
    	
        setupInput();
        GlobalEventCollector.getInstance().addListener(this);
        GlobalEventCollector.getInstance().setListenerInterestTypes(this, PauseEvent.PAUSE_TYPE);
        
        setupScene();
        
        cameraTracker = new CameraTracker(getCamera(), this.player1);
        renderer = new Box2DDebugRenderer(); 
        debugEnabled = false;
        
        setupMusic();
    }


	protected void setupInput() {
        globalInputProcessor = new GlobalInputProcessor();

        InputController.instance().addInputListener(this);
        InputController.instance().addInputListener(globalInputProcessor);
	}


	public PhysicalWorld getPhysicalWorld() {
		return this.physicalWorld;
	}
	public GameCharacter getPlayerCharacter() {
		return this.player1;
	}

	public boolean isGamePaused(){
		return pausedGame;
	}
	public void pauseGame(){
		this.pausedGame = true;
		disablePlayerInput();
	}
	public void resumeGame(){
		this.pausedGame = false;
		InputController.instance().addInputListener(playerInputProcessor1);
	}
	public void togglePauseGame(){
		if(isGamePaused()){
			resumeGame();
		}
		else{
			pauseGame();
		}
	}
	public void setPauseGame(PauseEvent evt) {
		switch(evt.getPauseAction()){
		case Pause:
			pauseGame();
			break;
		case Resume:
			resumeGame();
			break;
		case Toggle:
			togglePauseGame();
			break;
		default:
			break;
		}
	}
	
    public boolean isDebugEnabled(){
    	return this.debugEnabled;
    }
    public void setDebug(boolean enabled){
    	this.debugEnabled = enabled;
    }

	/** Carregar bound boxes do cenário*/
	public void setMapCollidersInPixels(List<Rectangle> colliders) {
		for(Rectangle boundBox : colliders){	
	        Vector2 groundPos = physicalWorld.convertToMeters(new Vector2(boundBox.getX(), boundBox.getY()));
	        Vector2 groundSize = physicalWorld.convertToMeters(new Vector2(boundBox.getWidth(),boundBox.getHeight()));

	        physicalWorld.addActor(new Collider(groundPos, groundSize)); 
		}
	}
	
	private void setupMusic() {
		music = Gdx.audio.newMusic(Gdx.files.internal("musics/ExploreTheAbyss.ogg")); 
		music.setLooping(true);
		music.setVolume(0.4f);
		music.play();
	}
	
	/* ********************************************* Setup Scene ****************************************************/
    private void setupScene() {
        setupBuilders();
        
        setupPlayer();
        setupEnemys();
        setupScenario();
	}
    
    private void setupBuilders() {
    	doorBuilder = new GameCharacterBuilder(new DoorConfigurator());
    	openDoorBuilder = new OpenDoorBuilder(new OpenDoorConfigurator());
		playerBuilder = new GameCharacterBuilder(new PlayerBuilderConfigurator());
		slimeBuilder = new GameCharacterBuilder(new SlimeBuilderConfigurator());
		skeletonBuilder = new GameCharacterBuilder(new SkeletonBuilderConfigurator());
		leverBuilder = new LeverBuilder(new LeverBuilderConfigurator());
		//leverBuilder.setTrigger(LeverSwitch.MAIN_DOOR_LEVER_SWITCH);
		spikeBuilder = new GameCharacterBuilder(new SpikeConfigurator());
		bossBuilder = new GameCharacterBuilder(new BossConfigurator());
	}
	protected void setupScenario() {
		setupDoorsAndSwitchs();
        setupSpikes();
        
        winCollider = new InsideColliderNotifier();
        winCollider.getPhysicalProperties().setIsSensor(true);
        winCollider.addInsideListener(new InsideListener() {
			@Override
			public void onEnter(PhysicsActor container, PhysicsActor insideActor) {
				if(insideActor == getPlayerCharacter()){
					GlobalEventCollector.getInstance().fireEvent(new WinEvent());
				}
			}
			@Override
			public void onLeave(PhysicsActor container, PhysicsActor insideActor) 
			{}
		});
        
        GlobalListener winListener = new GlobalListener() {
			@Override
			public void onGlobalEvent(GlobalEvent evt) {
				if(evt instanceof WinEvent){
					startWinAnimation();
				}
			}
		};
        GlobalEventCollector.getInstance().addListener(winListener);
        GlobalEventCollector.getInstance().setListenerInterestTypes(winListener, WinEvent.class);
        
//        physicalWorld.addActor(winCollider,new Vector2(43f,42.5f), new Vector2(1.5f,0.8f),0);
        physicalWorld.addActor(winCollider,new Vector2(49f,68f), new Vector2(3,3),0);
	}
	protected void startWinAnimation() {
		
		disablePlayerInput();
		BehaviourManager.instance().setPaused(true);
		
		GameCharacter playerCharacter = getPlayerCharacter();
		playerCharacter.fireEvent(new StopEvent());
		playerCharacter.stop();
		playerCharacter.lookAtPoint(playerCharacter.getPositionInWorld().cpy().add(0, 5));
		playerCharacter.getStateMachine().changeToState(BattleCharacterStates.BATTLE_STILL);
		
		Screen screen =getScreen();
		if(screen != null && screen instanceof GameScreen){
			GameScreen gameScreen = (GameScreen)screen;
			gameScreen.fadeToBlack(2000);
		}
		

		Timer.instance().scheduleTask(new Task() {
			@Override
			public void run() {
				ScreenController screenController = getScreenController();
				if(screenController != null){
					screenController.changeToScreen(GameScreens.Victory);
				}
			}
		}, 2);
	}


	protected void disablePlayerInput() {
		InputController.instance().removeInputListener(playerInputProcessor1);
		playerInputProcessor1.setEnabled(false);
	}

	protected void setupDoorsAndSwitchs() {
		
		LeverActor lever = (LeverActor) leverBuilder.build(new Vector2(95, 33));
		lever.setTargetTrigger(GlobalEvents.LEVER_BOTTOM_RIGHT);
		physicalWorld.addActor(lever);
		
		lever = (LeverActor) leverBuilder.build(new Vector2(29, 27));
		lever.setTargetTrigger(GlobalEvents.LEVER_BOTTOM_LEFT);
		physicalWorld.addActor(lever);
		
		lever = (LeverActor) leverBuilder.build(new Vector2(26, 65));
		lever.setTargetTrigger(GlobalEvents.LEVER_TOP_LEFT);
		physicalWorld.addActor(lever);
		
		lever = (LeverActor) leverBuilder.build(new Vector2(95, 50));
		lever.setTargetTrigger(GlobalEvents.LEVER_TOP_RIGHT);
		physicalWorld.addActor(lever);
		
//		LeverActor lever = (LeverActor) leverBuilder.build(new Vector2(47, 37));
//		lever.setTargetTrigger(GlobalEvents.LEVER_BOTTOM_RIGHT);
//		physicalWorld.addActor(lever);
//		
//		lever = (LeverActor) leverBuilder.build(new Vector2(49, 37));
//		lever.setTargetTrigger(GlobalEvents.LEVER_BOTTOM_LEFT);
//		physicalWorld.addActor(lever);
//		
//		lever = (LeverActor) leverBuilder.build(new Vector2(51, 37));
//		lever.setTargetTrigger(GlobalEvents.LEVER_TOP_LEFT);
//		physicalWorld.addActor(lever);
//		
//		lever = (LeverActor) leverBuilder.build(new Vector2(53, 37));
//		lever.setTargetTrigger(GlobalEvents.LEVER_TOP_RIGHT);
//		physicalWorld.addActor(lever);
		
        GameCharacter door = null;
        
        // Porta principal
        door = doorBuilder.build(new Vector2(49, 52));
        GlobalEventCollector.getInstance().addListener(door, GlobalEvents.MAIN_DOOR_ACCUMULATOR_SWITCH);
        physicalWorld.addActor(door);
        
        // Porta esquerda-cima
        door = doorBuilder.build(new Vector2(30, 47));
        GlobalEventCollector.getInstance().addListener(door, GlobalEvents.LEVER_TOP_LEFT);
        physicalWorld.addActor(door);
        
        // Primeira porta a esquerda
        door = doorBuilder.build(new Vector2(63, 36));
        GlobalEventCollector.getInstance().addListener(door, GlobalEvents.LEVER_BOTTOM_RIGHT);
        physicalWorld.addActor(door);
        
        // Segunda porta a esquerda
        door = openDoorBuilder.build(new Vector2(77, 36));
        GlobalEventCollector.getInstance().addListener(door, GlobalEvents.LEVER_BOTTOM_RIGHT);
        physicalWorld.addActor(door);
        
        door = doorBuilder.build(new Vector2(82, 25));
        GlobalEventCollector.getInstance().addListener(door, GlobalEvents.LEVER_BOTTOM_RIGHT);
        physicalWorld.addActor(door);
	}
	protected void setupEnemys() {
		setupSkeletons();
        setupSlime();
        
        boss = bossBuilder.build(new Vector2(49f, 73f));
        physicalWorld.addActor(boss);
	}


	private void setupSlime() {			
		//TODO: futuramente obter isto do mapa
		Vector2 slimePositions[] = new Vector2[]{
				new Vector2(17f, 99f - 69f)
			  , new Vector2(20f, 99f - 74f)
			  , new Vector2(29f, 99f - 74f)
			  , new Vector2(28f, 99f - 63f)
			  , new Vector2(68f, 99f - 89f)
			  , new Vector2(71f, 99f - 92f)
		};
		
		for(Vector2 pos : slimePositions){
			GameCharacter slime = slimeBuilder.build(pos);
	        physicalWorld.addActor(slime);        
	        BehaviourManager.instance().addBehaviourController(new BehaviourController(slime, player1));
		}
	}
	private void setupSpikes() {
		//TODO: futuramente obter isto do mapa
		Vector2 spikePositions[] = new Vector2[]{
				new Vector2(54f, 9f)
		};
		
		for(Vector2 pos : spikePositions){
			GameCharacter spike = spikeBuilder.build(pos);
	        physicalWorld.addActor(spike);        
		}
	}

	private void setupPlayer() {
		player1 = playerBuilder.build();
        this.playerInputProcessor1 = new CharacterInputProcessor(player1);
        InputController.instance().addInputListener(playerInputProcessor1);

        physicalWorld.addActor(player1, new Vector2(PLAYER_START_X, PLAYER_START_Y));
	}
	private void setupSkeletons() { 
		//TODO: futuramente obter isto do mapa
		Vector2 positions[] = new Vector2[]{
				new Vector2(18f, 99f - 64f)
			  , new Vector2(70f, 99f - 80f)
			  , new Vector2(89f, 99f - 67f)
			  , new Vector2(77f, 99f - 29f)
		};
		
		for(Vector2 pos : positions){
			GameCharacter skel = skeletonBuilder.build(pos);
	        physicalWorld.addActor(skel);        
	        BehaviourManager.instance().addBehaviourController(new BehaviourController(skel, player1));
		}
	}

	/* ********************************************* Stage Methods ****************************************************/
    @Override
    public void act(float delta) {
    	if(!this.isGamePaused()){
	        this.physicalWorld.update(delta);
	        BehaviourManager.instance().update(delta);
	        cameraTracker.act(delta);
	        super.act(delta);
    	}
    }
    @Override
    public void draw() {
        super.draw();
        if(isDebugEnabled()){
          renderer.render(physicalWorld.getWorld(), getViewport().getCamera().combined);
        }
    }
	@Override
    public void dispose() {
    	super.dispose();
    	this.music.dispose();
    }
	
    @Override
    public boolean keyUp(int keyCode) {
    	if(keyCode == Keys.ESCAPE){
    		Gdx.app.exit();
    	}
    	else if(keyCode == Keys.P ){
    		if(music.isPlaying()){
    			music.pause();
    		}
    		else{
    			music.play();
    		}
    	}
    	else if(keyCode == Keys.O ){
    		setDebug(!isDebugEnabled());
    	}
    	return super.keyUp(keyCode);
    }

    /* ******************************************* GlobalListener ****************************************************/
	@Override
	public void onGlobalEvent(GlobalEvent evt) {
		if(evt.getType().equals(PauseEvent.PAUSE_TYPE)){
			setPauseGame((PauseEvent)evt);
		}
	}
}
