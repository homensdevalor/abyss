package com.hdv.abyss.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.Resources;

public class VictoryScreen extends BaseScreen{
	private static final String CONGRATULATIONS_TITLE = "Parabéns!";
	private static final String CONGRATULATIONS_TEXT = 
			"Você completou a versão demo do Abyss!\n\n"
			+ "Esperamos vê-lo em futuras releases! \n"
			+ ":) \n"
			;
	
	private static final CharSequence CREDITS_TITLE = "Equipe";
	private static final String CREDITS_TEXT = 
			  "Alison de Araújo Bento\n"
			+ "Bruna Camila de Menezes\n"
			+ "Bruno Leite de Almeida\n"
			+ "Filipe Daniel S. Brizolara\n"
			+ "Paulo Leonardo Souza Brizolara\n"
			+ "Phellipe Albert C. N. V. Medeiros\n"
			;
	
	public class VictoryStage extends Stage{
		Texture victoryTexture;
//		Sprite victoryImage;
		Image image;
		Music music;
		public VictoryStage() {
			super(new FitViewport(Configurations.APP_WIDTH*2, Configurations.APP_HEIGHT*2));
			victoryTexture = new Texture(Gdx.files.local(Resources.victoryTexture));
//			victoryImage = new Sprite(victoryTexture);
			image = new Image(victoryTexture);
			image.setSize(getViewport().getWorldWidth(), getViewport().getWorldHeight());
			image.addAction(Actions.alpha(0.5f));
			this.addActor(image);
			
			setupMusic();
		}
		public Image getBackgroundImage() {
			return image;
		}
		
		private void setupMusic() {
			music = Gdx.audio.newMusic(Gdx.files.internal(Resources.congratulationsMusic)); 
			music.setLooping(true);
			music.setVolume(0.4f);
			music.play();
		}
		
	}
	
	private VictoryStage stage;
	public VictoryScreen(ScreenController screenController) {
		super(screenController);
	}	
	@Override
	public void onCreate() {
		super.onCreate();
		stage = new VictoryStage();
		
		layoutMenu();
	}
	
	private void layoutMenu() {
		LabelStyle titleLabelStyle =  getSkin().get(LabelStyle.class);
		titleLabelStyle.font = Resources.instance().generateFont(Resources.titleFont, Resources.congratulationsTitleFontSize);
		LabelStyle congratulationsTextStyle =  getSkin().get(LabelStyle.class);
		congratulationsTextStyle.font = Resources.instance().generateFont(Resources.titleFont, Resources.congratulationsFontSize);

        Label titleLabel = new Label(CONGRATULATIONS_TITLE, getSkin() );
        Label congratulationsTextArea = new Label(CONGRATULATIONS_TEXT, getSkin());
        congratulationsTextArea.setAlignment(Align.center);
        congratulationsTextArea.setStyle(titleLabelStyle);
        
		Table congratulationsContainer = new Table(getSkin());
//		uiContainer.setFillParent(true);
        congratulationsContainer.add(titleLabel).padBottom(20).row();
        congratulationsContainer.add(congratulationsTextArea).center().row();  
        
        Label titleCredits = new Label(CREDITS_TITLE, getSkin() );
        Label creditsText = new Label(CREDITS_TEXT, getSkin());
        creditsText.setAlignment(Align.center);
        creditsText.setStyle(titleLabelStyle);

        congratulationsContainer.add(titleCredits).padTop(150).row();
        congratulationsContainer.add(creditsText).padTop(20).row();        
        
        Table table = new Table();
        table.setFillParent(true);
        
        table.add(congratulationsContainer).row();
        
        stage.addActor(table);

        stage.addAction(Actions.alpha(0));
        Action fadeInScene = Actions.delay(0.5f,Actions.fadeIn(3,Interpolation.fade));
        stage.addAction(fadeInScene);
        
        
        table.addAction(Actions.moveBy(0, -170));
        Action showCredits = Actions.delay(8, Actions.moveBy(0, 330,1));
        Action hideCredits = Actions.delay(5, Actions.moveBy(0, 330,1));
        Action fadeToWhiteImage = Actions.run(new Runnable() {
			
			@Override
			public void run() {
				stage.getBackgroundImage().addAction(Actions.fadeIn(3, Interpolation.fade));
			}
		});
        table.addAction(Actions.sequence(showCredits, hideCredits,fadeToWhiteImage));
	}
	
	private Skin skin;
	private Skin getSkin() {
		if(skin == null){
			skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
		}
		return skin;
	}
	
	@Override
	public void draw(float delta) {
		super.draw(delta);
		
		stage.act(delta);
		stage.draw();
	}
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		stage.getViewport().update(width, height);
	}
}
