package com.hdv.abyss;

public class Configurations {
    public static final int APP_WIDTH = 256;
    public static final int APP_HEIGHT = 192;
//    public static final int APP_WIDTH = 512;
//    public static final int APP_HEIGHT = 384;
    public static final float TILE_IN_METERS = 1f; 
    public static final int   TILE_IN_PIXELS = 24; 
    public static final float PIXELS_PER_METER = TILE_IN_METERS/TILE_IN_PIXELS; 
    public static final float METERS_PER_PIXEL = 1f/PIXELS_PER_METER;
    
    public static final float GAME_VIEWPORT_X = 20*0.8f; 
    public static final float GAME_VIEWPORT_Y = 15 *0.8f;
}
