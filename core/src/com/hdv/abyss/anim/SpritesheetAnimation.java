package com.hdv.abyss.anim;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.Direction;

public class SpritesheetAnimation implements DirectionalAnimation {
	private static final int MAX_TIME_ANIM = 100;
	private static final int DIR_NUMBER = 4;
	private Direction currentDirection;
	private final EnumMap<Direction, Animation> dirToAnimation;
	private float animationTime;
	private final AnimationParser animationParser;
	private int colNumber;
	private float fps;
	private final List<Direction> directionOrder;
	private PlayMode animationPlayMode;
	private int rowNumber;
	private Vector2 offset;
	
	public SpritesheetAnimation(String spriteesheetFile, float fps, int columns) {
		this(spriteesheetFile,fps,columns, DIR_NUMBER);
	}
	public SpritesheetAnimation(String spriteesheetFile, float fps, int columns, int rows) {
		this(new AnimationParser(spriteesheetFile, columns, rows), fps,columns, rows);
	}
	protected SpritesheetAnimation(AnimationParser parser, float fps, int columns, int rows) {
		this.animationParser = parser;
		this.animationTime = 0;
		this.colNumber = columns;
		this.rowNumber = rows;
		this.fps = fps;
		this.animationPlayMode = PlayMode.LOOP;
		
		this.directionOrder = Arrays.asList(new Direction[]{Direction.Down,Direction.Left,Direction.Up,Direction.Right});
		this.dirToAnimation = new EnumMap<Direction, Animation>(Direction.class);
		
		this.offset = null;
	}
	
	@Override
	public Object clone(){
		SpritesheetAnimation clonedAnimation = new SpritesheetAnimation(this.animationParser, fps, colNumber, rowNumber);
		clonedAnimation.setPlayMode(getPlayMode());
		clonedAnimation.setDirectionOrder((Direction[]) this.directionOrder.toArray());
		clonedAnimation.setCurrentDirection(this.getCurrentDirection());
		
		return clonedAnimation;
	}
	
	@Override
	public TextureRegion getKeyFrame(Direction direction) {
		setCurrentDirection(direction);
		Animation animation = dirToAnimation.get(direction);
		if(animation  == null)
		{
			animation = createAnimation(direction);
			animation.setPlayMode(animationPlayMode);
//			animation.setFrameDuration(this.fps);
			dirToAnimation.put(direction, animation);
		}
		return animation.getKeyFrame(animationTime);
	}

	private Animation createAnimation(Direction dir) {
		int rowIndex = dirToInt(dir);
		
		int start = rowIndex * colNumber + 1;
		int end = start + colNumber - 1;
		
		return animationParser.getAnimation(start, end, this.fps);
	}
	private int dirToInt(Direction dir) {
		if(this.rowNumber >= Direction.values().length){
			return this.directionOrder.indexOf(dir);
		}
		return rowNumber -1;
	}

	@Override
	public void update(float delta) {
		animationTime += delta;
		if(animationTime > MAX_TIME_ANIM){
			animationTime = animationTime % getAnimationDuration();
		}
	}
	
	public float getFps() {
		return fps;
	}
	public int getNumberOfFrames() {
		return colNumber;
	}
	public List<Direction> getDirectionOrder() {
		return new CopyOnWriteArrayList<Direction>(directionOrder);
	}
	public void setDirectionOrder(Direction directionOrder[]) {
		this.setDirectionOrder(Arrays.asList(directionOrder));
	}
	public void setDirectionOrder(List<Direction> directionOrder) {
		assertContainsAllDirection(directionOrder);
		if(!this.directionOrder.equals(directionOrder)){
			this.directionOrder.clear();
			this.directionOrder.addAll(directionOrder);
			// Força reordenação da direção das animações, caso estas ja tenham sido definidas
			this.dirToAnimation.clear(); 
		}
	}

	@Override
	public PlayMode getPlayMode() {
		return animationPlayMode;
	}
	@Override
	public void setPlayMode(PlayMode playMode) {
		this.animationPlayMode = playMode;
		if(!this.dirToAnimation.isEmpty()){
			for(Animation animation : dirToAnimation.values()){
				animation.setPlayMode(playMode);
			}
		}
	}
	
	@Override
	public float getAnimationTime() {
		return animationTime;
	}
	@Override
	public void setAnimationTime(float animationTime) {
		this.animationTime = animationTime;
	}
	private void assertContainsAllDirection(List<Direction> directionOrder) {
		Direction directions[] = Direction.values();
		if(directionOrder.size() != directions.length){
			throw new IllegalArgumentException("List of directions contains a different number of Directions");
		}
		for(Direction dir : directions){
			if(!directionOrder.contains(dir)){
				throw new IllegalArgumentException(
						"Direction "+ String.valueOf(dir) + " should be on list of directions.");
			}
		}
	}
	
	protected Direction getCurrentDirection() {
		return currentDirection;
	}

	protected void setCurrentDirection(Direction direction) {
		if(direction != this.currentDirection){
			this.currentDirection = direction;
			animationTime = 0;
		}
	}
	@Override
	public void setAnimationDuration(float animDurationInSeconds) {
		this.fps = getNumberOfFrames() / animDurationInSeconds;
		updateAnimationsFps();
	}

	@Override
	public float getAnimationDuration() {
		return getNumberOfFrames() / getFps();
	}
	
	private void updateAnimationsFps() {
		this.dirToAnimation.clear();
//		for(Animation anim: this.dirToAnimation.values()){
//			anim.setFrameDuration(1/fps);
//		}
	}
	@Override
	public Vector2 getOffset() {
		return offset;
	}
	@Override
	public void setOffset(Vector2 offset) {
		if(offset == null){
			this.offset = null;
		}
		else{
			this.offset = offset.cpy();
		}
	}	
}
