package com.hdv.abyss.anim;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.Direction;

public interface DirectionalAnimation extends Cloneable{
	TextureRegion getKeyFrame(Direction playerDirection);
	void update(float delta);
	PlayMode getPlayMode();
	void setPlayMode(PlayMode mode);
	/** Define o tempo da animação que será reproduzido neste momento.
	 *  Ex.: setAnimationTime(0) posicionará a animação no início 
	 *  	e setAnimationTime(animation.getAnimationDuration) posicionará a animação no instante final.*/
	void setAnimationTime(float animationTime);
	/** Retorna tempo atual da animação. Não é garantido que este valor seja inferior à duração de um ciclo da animação.
	 * @return um tempo em segundos*/
	float getAnimationTime();
	/** Define o tempo total de um ciclo da animação.*/
	void setAnimationDuration(float stateDurationInSeconds);
	/** @return duração em segundos de um ciclo da animação*/
	float getAnimationDuration();
	
	Vector2 getOffset();
	void setOffset(Vector2 offset);
}
