package com.hdv.abyss.anim;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationParser {
	
	private TextureRegion[] linearSpriteSheet;
	private int columns;
	private int rows;
	
	public AnimationParser(String spritemap, int columns, int rows){
		this.columns = columns;
		this.rows = rows;
		
		Texture animationSheet = new Texture(Gdx.files.internal(spritemap)); // #9
        TextureRegion[][] spriteSheet = TextureRegion.split(animationSheet, animationSheet.getWidth()/columns, animationSheet.getHeight()/rows);
        linearSpriteSheet = new TextureRegion[columns * rows];
        int index = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                linearSpriteSheet[index++] = spriteSheet[i][j];
            }
        }
	}
	
	public TextureRegion[] getAnimationRegion(int start, int end){
		if(start <= 0 || end > (rows * columns)){
			throw new IndexOutOfBoundsException();
		}
		
		start -= 1;
		end -= 1;
		
		int interval = (end - start) + 1;
		int index = 0;
		TextureRegion[] region = new TextureRegion[interval];
		
		for(int i = start; i <= end; i++){
			region[index++] = this.linearSpriteSheet[i];
		}
		
		return region;
	}

	public Animation getAnimation(int start, int end, float fps){
		TextureRegion[] region = this.getAnimationRegion(start, end);
		float frameDuration =  1 / fps;
		return new Animation(frameDuration, region);
	}
	
}
