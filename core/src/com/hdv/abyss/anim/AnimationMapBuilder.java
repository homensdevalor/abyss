package com.hdv.abyss.anim;

import java.util.Map;

public interface AnimationMapBuilder {
	Map<? extends Object, ? extends DirectionalAnimation> buildMap();
}
