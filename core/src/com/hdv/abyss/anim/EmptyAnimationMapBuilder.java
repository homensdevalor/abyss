package com.hdv.abyss.anim;

import java.util.Collections;
import java.util.Map;


public class EmptyAnimationMapBuilder implements AnimationMapBuilder{

	@Override
	public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
		return Collections.emptyMap();
	}

}
