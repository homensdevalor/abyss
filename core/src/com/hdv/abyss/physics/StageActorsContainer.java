package com.hdv.abyss.physics;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class StageActorsContainer implements ActorsContainer{
	private final Stage stage;
	public StageActorsContainer(Stage stage){
		this.stage = stage;
	}
	@Override
	public void addActor(Actor actor) {
		stage.addActor(actor);
	}
	@Override
	public void removeActor(Actor actor) {
		actor.remove();
	}
}