package com.hdv.abyss.physics.collision;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.hdv.abyss.statemachine.events.GameEvent;

public class CollisionEvent implements GameEvent{
	private final Object collider1, collider2;
	private final Contact contact;
	public CollisionEvent(Contact contact) {
		this(getUserData(contact, 0), getUserData(contact, 1),contact);
	}
	public CollisionEvent(Object collider1, Object collider2) {
		this(collider1,collider2, null);
	}
	public CollisionEvent(Object collider1, Object collider2, Contact contact) {
		this.collider1 = collider1;
		this.collider2 = collider2;
		this.contact = contact;
	}

	private static Object getUserData(Contact contact, int i) {
		Fixture fixture = (i == 0 ? contact.getFixtureA() : contact.getFixtureB());
		return fixture.getBody().getUserData();
	}
	public Object getCollider1() {
		return collider1;
	}
	public Object getCollider2() {
		return collider2;
	}
	public Contact getContact() {
		return contact;
	}

	public Object getColliderOfType(Class<?> someClass) {
		if(someClass.isInstance(collider1)){
			return collider1;
		}
		else if(someClass.isInstance(collider2)){
			return collider2;
		}
		return null;
	}

	public boolean containsInstanceOf(Class<?> someClass) {		
		return someClass.isInstance(collider1) ||	someClass.isInstance(collider2);
	}

	public boolean contains(Object collider) {
		return collider1 == collider || collider2 == collider;
	}

	public Object getOther(Object collider) {
		if(collider1 == collider){
			return collider2;
		}
		else if(collider2 == collider){
			return collider1;
		}
		return null;
	}
}
