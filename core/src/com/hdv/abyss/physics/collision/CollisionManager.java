package com.hdv.abyss.physics.collision;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.hdv.abyss.physics.PhysicsActor;


public class CollisionManager implements ContactListener{
	public interface TouchListener {
		void onBeginTouch(CollisionEvent evt);
		void onEndTouch(CollisionEvent evt);
	}

	public interface CollisionListener{
		public void onCollision(CollisionEvent evt);
	}

	private final Map<PhysicsActor, Set<CollisionListener> > actorCollisionListeners;
	private final Map<PhysicsActor, Set<TouchListener>> touchListeners;
	
	public CollisionManager() {
		actorCollisionListeners = new HashMap<PhysicsActor, Set<CollisionListener>>();
		touchListeners = new HashMap<PhysicsActor, Set<TouchListener>>();
	}
	
	/***/
	public void addCollisionListener(CollisionListener listener, 
			PhysicsActor actorCollider) 
	{
		if(!actorCollisionListeners.containsKey(actorCollider)){
			actorCollisionListeners.put(actorCollider, new HashSet<CollisionManager.CollisionListener>());
		}
		this.actorCollisionListeners.get(actorCollider).add(listener);
	
	}
	public void removeCollisionListener(CollisionListener listener, 
			PhysicsActor actorCollider){
		Set<CollisionListener> listeners = this.actorCollisionListeners.get(actorCollider);
		if(listeners != null){
			listeners.remove(listener);
			if(listeners.isEmpty()){
				actorCollisionListeners.remove(actorCollider);
			}
		}
	}

	public void addTouchListener(TouchListener touchListener, PhysicsActor actorCollider) {

		if(!touchListeners.containsKey(actorCollider)){
			touchListeners.put(actorCollider, new HashSet<CollisionManager.TouchListener>());
		}
		this.touchListeners.get(actorCollider).add(touchListener);
	}
	public void removeTouchListener(TouchListener touchListener) {
		for(Entry<PhysicsActor, Set<TouchListener>> entry : this.touchListeners.entrySet()){
			if(entry.getValue().contains(touchListener)){
				entry.getValue().remove(touchListener);
			}
			//TODO: remover chave se lista estiver vazia
		}
	}

	@Override
	public void beginContact(Contact contact) {
		CollisionEvent evt = new CollisionEvent(contact);
		fireCollisionEvent(evt);
		fireBeginTouchEvent(evt);
	}

	@Override
	public void endContact(Contact contact) {
		CollisionEvent evt = new CollisionEvent(contact);
		fireEndTouchEvent(evt);
	}

	private void fireCollisionEvent(final CollisionEvent evt) {
		if(this.actorCollisionListeners.containsKey(evt.getCollider1())){
			fireEvent(evt, actorCollisionListeners.get(evt.getCollider1()));
		}
		if(this.actorCollisionListeners.containsKey(evt.getCollider2())){
			fireEvent(evt, actorCollisionListeners.get(evt.getCollider2()));
		}
	}

	protected void fireBeginTouchEvent(final CollisionEvent evt) {
		if(this.touchListeners.containsKey(evt.getCollider1())){
			fireTouchEvent(evt, touchListeners.get(evt.getCollider1()), true);
		}
		if(this.touchListeners.containsKey(evt.getCollider2())){
			fireTouchEvent(evt, touchListeners.get(evt.getCollider2()), true);
		}
	}
	private void fireEndTouchEvent(CollisionEvent evt) {
		if(this.touchListeners.containsKey(evt.getCollider1())){
			fireTouchEvent(evt, touchListeners.get(evt.getCollider1()), false);
		}
		if(this.touchListeners.containsKey(evt.getCollider2())){
			fireTouchEvent(evt, touchListeners.get(evt.getCollider2()), false);
		}
	}
	
	private void fireTouchEvent(CollisionEvent evt, Collection<TouchListener> listeners, boolean isBegin) {
		for(TouchListener listener : listeners){
			if(isBegin){
				listener.onBeginTouch(evt);
			}
			else{
				listener.onEndTouch(evt);
			}
		}
	}

	private void fireEvent(final CollisionEvent evt, Collection<CollisionListener> listeners) {
		for(CollisionListener listener : listeners){
			listener.onCollision(evt);
		}
	}
	
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

}
