package com.hdv.abyss.physics.collision;

import java.util.HashMap;
import java.util.Map;

public enum CollisionCategory {
	None(1),
	General(2),
	Player(3),
	PlayerAttack(4), 
	Enemy(5), 
	EnemyAttack(6);
	private short categoryBits;
	private static Map<CollisionCategory, Short> masks;
	
	private CollisionCategory(int categoryIndex){
		categoryBits = (short) (1 << categoryIndex);
	}
	public short getCategoryBits(){
		return categoryBits;
	}
	public short getCollisionMask(){
		final Map<CollisionCategory, Short> masks = CollisionCategory.getMasks();
		return masks.get(this);
	}

	private static Map<CollisionCategory, Short> getMasks() {
		if(masks == null){
			masks = new HashMap<CollisionCategory, Short>();
			//Categoria "geral" colide com todos
			masks.put(General, (short)~0);
			masks.put(None, createMask());
			masks.put(Player, createMask(Enemy, EnemyAttack, General));
			masks.put(PlayerAttack, createMask(Enemy, General));
			masks.put(Enemy, createMask(Player, PlayerAttack, General));
			masks.put(EnemyAttack, createMask(Player, General));
		}
		return masks;
	}
	private static Short createMask(CollisionCategory ... categoriesToCollide) {
		Short mask = 0;
		for(CollisionCategory category : categoriesToCollide){
			mask = (short) (mask | category.getCategoryBits());
		}
		return mask;
	}
}
