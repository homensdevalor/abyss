package com.hdv.abyss.physics.collision.matcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hdv.abyss.physics.collision.CollisionEvent;


/** Matcher composto. É ativado quando todos seus "filhos" componentes são ativados simultaneamente.*/
public class CompoundCollisionMatcher implements CollisionMatcher{
	private final List<CollisionMatcher> filters;
	
	public CompoundCollisionMatcher(CollisionMatcher ... filters) {
		this.filters = new ArrayList<CollisionMatcher>(Arrays.asList(filters));
	}

	@Override
	public boolean match(CollisionEvent evt) {
		for(CollisionMatcher filter : filters){
			if(!filter.match(evt)){
				return false;
			}
		}
		return true;
	}
}
