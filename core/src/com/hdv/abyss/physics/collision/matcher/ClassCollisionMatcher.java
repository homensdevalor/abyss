package com.hdv.abyss.physics.collision.matcher;

import com.hdv.abyss.physics.collision.CollisionEvent;

public class ClassCollisionMatcher implements CollisionMatcher {
	private Class<?> classToMatch;
	
	public ClassCollisionMatcher(Class<?> colliderClass) {
		this.classToMatch = colliderClass;
	}
	
	@Override
	public boolean match(CollisionEvent evt) {
		return evt.containsInstanceOf(classToMatch);
	}

}
