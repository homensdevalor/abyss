package com.hdv.abyss.physics.collision.matcher;

import com.hdv.abyss.physics.collision.CollisionEvent;

public class InstanceCollisionMatcher implements CollisionMatcher {
	final Object instance;
	
	public InstanceCollisionMatcher(Object obj) {
		this.instance = obj;
	}
	
	@Override
	public boolean match(CollisionEvent evt) {
		return evt.contains(instance);
	}

}
