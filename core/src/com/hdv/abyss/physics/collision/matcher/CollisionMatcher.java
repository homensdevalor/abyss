package com.hdv.abyss.physics.collision.matcher;

import com.hdv.abyss.physics.collision.CollisionEvent;

public interface CollisionMatcher {
	boolean match(CollisionEvent evt);
}
