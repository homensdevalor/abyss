package com.hdv.abyss.physics;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import com.badlogic.gdx.utils.TimeUtils;

/** Classe responsável por controlar a remoção dos PhysicalActors no tempo apropriado.*/
public class TimedDeathManager {
	private final PriorityQueue<ActorLiveTime> timedDeathList;
	private final PhysicalWorld physicalWorld;
	
	public TimedDeathManager(PhysicalWorld world) {
		timedDeathList = new PriorityQueue<ActorLiveTime>();
		this.physicalWorld = world;
	}

	public void update() {
		checkActorsLifeTime();
	}	
	
	/** Percorre todos os atores marcados para morrer e verifica se o tempo de vida deles chegou ao fim.*/
	private void checkActorsLifeTime() {
		if(!timedDeathList.isEmpty()){
			long currentTimeMillis = TimeUtils.millis();
			List<ActorLiveTime> actorLiveToRemove = new ArrayList<ActorLiveTime>();
			for(ActorLiveTime actorLive : timedDeathList){
				if(actorLive.getTimeToDie() <= currentTimeMillis){
					actorLiveToRemove.add(actorLive);
				}
			}
			for(ActorLiveTime actor: actorLiveToRemove){
//				destroyActor(actor.getActorToDestroy());
				timedDeathList.remove(actor);
				physicalWorld.removeActor(actor.getActorToDestroy());
			}
		}
	}

	public void addToDeathList(PhysicsActor physicsActor, Long deathTime) {
		timedDeathList.add(new ActorLiveTime(deathTime, physicsActor));
	}
	public void removeFromTimedDeathList(PhysicsActor physicsActor) {
		List<ActorLiveTime> toRemove = new ArrayList<ActorLiveTime>();
		for(ActorLiveTime actorLiveTime : timedDeathList){			
			if(actorLiveTime.getActorToDestroy() == physicsActor){
				toRemove.add(actorLiveTime);
			}
		}
		//Fazendo isto pois não da para remover durante a iteração (?)
		for(ActorLiveTime actorsToRemove : toRemove){
			timedDeathList.remove(actorsToRemove);
		}
		
		ensureNoteInTimedDeathList(physicsActor);
	}

	private void ensureNoteInTimedDeathList(PhysicsActor physicsActor) {
		for(ActorLiveTime actorLiveTime : timedDeathList){
			if(actorLiveTime.getActorToDestroy() == physicsActor){
				throw new IllegalStateException("Actor " + String.valueOf(physicsActor) + " should not be on timedDeathList");
			}
		}
	}

}
