package com.hdv.abyss.physics;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class GroupActorsContainer implements ActorsContainer{
	private final Group group;
	public GroupActorsContainer(Group group){
		this.group = group;
	}
	@Override
	public void addActor(Actor actor) {
		group.addActor(actor);
	}
	@Override
	public void removeActor(Actor actor) {
		group.removeActor(actor);
	}
}