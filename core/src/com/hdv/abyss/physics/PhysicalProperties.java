package com.hdv.abyss.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hdv.abyss.physics.collision.CollisionCategory;

public class PhysicalProperties {
	Vector2 startPosition;
	Vector2 size;
	float startAngle;
	BodyType bodyType;
	CollisionCategory collisionCategory;
	
	boolean sensor;
	float density;
	float restitution;
	float linearDamping;
	boolean fixedRotation;
	private float friction;

	public PhysicalProperties() {
		this(Vector2.Zero,new Vector2(1,1), 0, BodyType.StaticBody, CollisionCategory.General);
	}
	
	public PhysicalProperties(Vector2 startPosition,
			Vector2 size, float startAngle, BodyType bodyType, CollisionCategory collisionCategory) {
		super();
		this.startPosition     = startPosition.cpy();
		this.size 		       = size.cpy();
		this.startAngle        = startAngle;
		this.bodyType          = bodyType;
		this.collisionCategory = collisionCategory;
		this.sensor			   = false;
		this.density 		   = 1f;
		this.restitution       = 0f;
		this.linearDamping     = 0f;
		this.fixedRotation	   = true;
		this.friction		   = 0.3f;
	}
	public void set(PhysicalProperties physicalProperties) {
		if(physicalProperties != null){
			this.startPosition.set(physicalProperties.getStartPosition());
			this.size.set(physicalProperties.getSize());
			this.startAngle 	   = physicalProperties.getStartAngle();
			this.bodyType 		   = physicalProperties.getBodyType();
			this.collisionCategory = physicalProperties.getCollisionCategory();
			this.sensor			   = physicalProperties.isSensor();
			this.density 		   = physicalProperties.getDensity();
			this.restitution       = physicalProperties.getRestitution();
			this.linearDamping     = physicalProperties.getLinearDamping();
			this.fixedRotation	   = physicalProperties.isFixedRotation();
			this.friction		   = physicalProperties.getFriction();
		}
	}
	
	public Vector2 getStartPosition() {
		return startPosition;
	}
	public float getStartAngle() {
		return startAngle;
	}
	public Vector2 getSize() {
		return size.cpy();
	}
	public BodyType getBodyType() {
		return bodyType;
	}
	public CollisionCategory getCollisionCategory() {
		return collisionCategory;
	}
	public boolean isSensor() {
		return sensor;
	}
	public float getDensity() {
		return density;
	}
	public float getRestitution() {
		return restitution;
	}	
	public float getLinearDamping() {
		return linearDamping;
	}
	public boolean isFixedRotation() {
		return fixedRotation;
	}
	public float getFriction() {
		return friction;
	}
	
	public void setStartPosition(Vector2 startPosition) {
		this.startPosition = startPosition.cpy();
	}
	public void setStartAngle(float startAngle) {
		this.startAngle = startAngle;
	}
	public void setSize(Vector2 size) {
		this.size = size.cpy();
	}
	public void setBodyType(BodyType bodyType) {
		this.bodyType = bodyType;
	}
	public void setCollisionCategory(CollisionCategory collisionCategory) {
		this.collisionCategory = collisionCategory;
	}
	public void setIsSensor(boolean isSensor) {
		this.sensor = isSensor;
	}
	public void setDensity(float density) {
		this.density = density;
	}
	public void setRestitution(float restitution) {
		this.restitution = restitution;
	}
	public void setLinearDamping(float linearDamping) {
		this.linearDamping = linearDamping;
	}
	public void setFixedRotation(boolean fixedRotation) {
		this.fixedRotation = fixedRotation;
	}
	public void setFriction(float friction) {
		this.friction = friction;
	}
	
}
