package com.hdv.abyss.physics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointEdge;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.physics.collision.CollisionManager;

public class PhysicalWorld {
	private final float TIME_STEP = 1 / 300f;
    private float accumulator = 0f;
	
	private World world;
	private float pixelsPerMeters;
	private ActorsContainer actorsContainer;
	private final TimedDeathManager deathManager;
	private final Set<PhysicsActor> actorsInWorld;
	private final Set<PhysicsActor> immediateDeathList;
	private final CollisionManager collisionManager;
	private final List<Runnable> afterStepRunnables;
	
	public PhysicalWorld(){
		this(new Vector2().setZero(), true);
	}
	public PhysicalWorld(Vector2 gravity, boolean doSleep){
		world = new World(gravity, doSleep);
		this.pixelsPerMeters = Configurations.PIXELS_PER_METER;
		actorsContainer = null;
		actorsInWorld = new HashSet<PhysicsActor>();
		immediateDeathList = new HashSet<PhysicsActor>();
		deathManager = new TimedDeathManager(this);
		
		collisionManager = new CollisionManager();
		world.setContactListener(collisionManager);
		
		afterStepRunnables = new ArrayList<Runnable>();
	}
	
	public void update(float delta){
        // Garante que o time step do mundo seja fixo (o delta pode ser variável)
        accumulator += delta;
        while (accumulator >= TIME_STEP) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }
        deathManager.update();
		destroyMarkActors();
		
		for(Runnable runnable : afterStepRunnables){
			runnable.run();
		}
		afterStepRunnables.clear();
	}
	
	public World getWorld() {
		return this.world;
	}
	public CollisionManager getCollisionManager() {
		return this.collisionManager;
	}
	/* ************************************* Conversão pixels e metros ****************************************/
	public float getPixelsPerMeters() {
		return pixelsPerMeters;
	}
	public void setPixelsPerMeters(float pixelsPerMeters) {
		this.pixelsPerMeters = pixelsPerMeters;
	}
	public Vector2 convertToPixels(Vector2 worldCoordinates){
		return worldCoordinates.cpy().scl(1f/pixelsPerMeters);
	}
	public Vector2 convertToMeters(Vector2 screenCoordinates){
		return screenCoordinates.cpy().scl(pixelsPerMeters);
	}
	public float convertToMeters(float screenCoordinate) {
		return screenCoordinate * pixelsPerMeters;
	}


	/* ************************************* ActorsContainer ************************************************/
	public ActorsContainer getActorsContainer() {
		return actorsContainer;
	}
	public void setActorsContainer(ActorsContainer actorsContainer) {
		this.actorsContainer = actorsContainer;
	}
	public void setActorsContainer(Stage actorsContainer) {
		this.actorsContainer = new StageActorsContainer(actorsContainer);
	}
	public void setActorsContainer(Group actorsContainer) {
		this.actorsContainer = new GroupActorsContainer(actorsContainer);
	}

	/* ************************************* Criação e destruição de atores ************************************************/
	public void spawn(PhysicsActor physicsActor, Vector2 pos) {
		this.spawn(physicsActor, pos, physicsActor.getPhysicalProperties().getSize(), 0, null);
	}
	public void spawn(PhysicsActor physicsActor, Vector2 pos, Long liveTimeMillis) {
		this.spawn(physicsActor, pos, physicsActor.getPhysicalProperties().getSize(), 0, liveTimeMillis);
	}
	public void spawn(PhysicsActor physicsActor, Vector2 pos, Vector2 size, float angle) {
		this.spawn(physicsActor, pos, size, angle, null);
	}
	public void spawn(PhysicsActor physicsActor, Vector2 pos, Vector2 size, float angle, Long liveTimeMillis) {
		assertParameterNotNull(physicsActor, "parameter (PhysicsActor) should not be null;");
		if(liveTimeMillis != null){
			assertParameterIsPositive(liveTimeMillis, "parameter (Long) should not be negative;");
		}
		
		addActor(physicsActor, pos, size, angle);
		if(liveTimeMillis != null){
			Long deathTime = TimeUtils.millis() + liveTimeMillis;
			deathManager.addToDeathList(physicsActor, deathTime);
		}
	}

	public void addActor(PhysicsActor actor, Vector2 pos, Vector2 size, float angle) {
		actor.getPhysicalProperties().setSize(size);
		actor.getPhysicalProperties().setStartAngle(angle);
		this.addActor(actor, pos);
	}
	public void addActor(PhysicsActor actor, Vector2 position) {
		actor.getPhysicalProperties().setStartPosition(position);
		this.addActor(actor);
	}
	public void addActor(PhysicsActor actor) {
		if(!actorsInWorld.contains(actor)){
	        actor.createBody(world);
	        actorsInWorld.add(actor);
	        actor.attachToWorld(this);
	        
	        if(actorsContainer != null){
	        	actorsContainer.addActor(actor);
	        }
		}
	}
	
	/* ************************************* Destruição de atores ************************************************/
	public void removeActor(PhysicsActor physicsActor) {
		if(actorsInWorld.contains(physicsActor)){
			markToDestroy(physicsActor);
			actorsInWorld.remove(physicsActor);

	        if(actorsContainer != null){
	        	actorsContainer.removeActor(physicsActor);
	        }
		}
	}

	/** Marca atores para serem destruídos.*/
	private void markToDestroy(PhysicsActor actorToDie) {
		if(immediateDeathList.contains(actorToDie)){
			throw new IllegalArgumentException("Argument of type PhysicsActor already marked to destruction.");
		}
		deathManager.removeFromTimedDeathList(actorToDie);
		this.immediateDeathList.add(actorToDie);
	}
	/** Destroi atores marcados para serem destruídos.
	 * Este método deve ser chamado fora do 'step' do world.*/
	private void destroyMarkActors() {
		for(PhysicsActor actor : immediateDeathList){
			destroyActor(actor);
		}
		immediateDeathList.clear();
	}
	private void destroyActor(PhysicsActor actorToDie) {
		//Remove ator do stage
		actorToDie.remove();
		//chama o método 'dettach' 
		actorToDie.dettachFromWorld();
		removeBodySafely(actorToDie.getBody());
	}

	private void removeBodySafely(Body body) {
		final Array<JointEdge> list = body.getJointList();
		while (list.size > 0) {
			world.destroyJoint(list.get(0).joint);
		}
		world.destroyBody(body);
	}

	/* ************************************* Utils ************************************************/
	private void assertParameterIsPositive(Number number, String errorMessage) {
		if(number != null && number.longValue() < 0){
			throw new IllegalArgumentException(errorMessage);
		}
	}
	private void assertParameterNotNull(Object obj, String errorMessage) {
		if(obj == null){
			throw new IllegalArgumentException(errorMessage);
		}
	}
	public void afterStep(Runnable runnable) {
		this.afterStepRunnables.add(runnable);
	}
}
