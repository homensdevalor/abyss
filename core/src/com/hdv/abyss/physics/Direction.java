package com.hdv.abyss.physics;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.utils.Vectors;

public enum Direction {
	Up(0,1), Down(0,-1), Left(-1,0), Right(1,0);
	private Vector2 dirVector;
	private Direction(float x, float y){
		dirVector = new Vector2(x,y);
	}
	public Vector2 toVector() {
		return dirVector.cpy();
	}
	/** @param orthogonalDirection an orthogonal vector which will be converted to a Direction. */
	public static Direction fromVector(Vector2 orthogonalDirection) {
		Vector2 normDir = orthogonalDirection.cpy().nor();
		if(normDir.x * normDir.y == 0 && Math.abs(normDir.x + normDir.y) == 1){
			for(Direction dir : values()){
				if(equals(dir, normDir)){
					return dir;
				}
			}
		}
		return null;
	}
	public static boolean equals(Direction dir, Vector2 dirVector){
		return dir.dirVector.x == Math.round(dirVector.x) && dir.dirVector.y == Math.round(dirVector.y);
	}
//	public static Direction roundFromVector(Vector2 direction) {
//		
//		Vector2 normalized = direction.cpy().nor();
//		if(normalized.x != 0 && normalized.y != 0){
//			Vector2 roundedDir; 
//			if(Math.abs(normalized.x) > Math.abs(normalized.y)){
//				roundedDir = new Vector2(normalized.x, 0).nor();
//			}
//			else{
//				roundedDir = new Vector2(0,normalized.y).nor();
//			}
//			normalized = roundedDir;
//		}
//
//		return Direction.fromVector(normalized);
//	}
	public static Direction roundFromVector(Vector2 direction) {
		if(direction.isZero()){
			return null;
		}
		float angle = Vectors.vectorToAngle(direction);

		return Direction.fromRadians(angle);
	}
//	public static Direction roundFromVector(Vector2 direction) {
//		float absX = Math.abs(direction.x);
//		float absY = Math.abs(direction.y);
//		
//		if(direction.isZero() || absX == absY){
//			return null;
//		}
//		if(absX > absY){
//			return direction.x > 0 ? Direction.Right : Direction.Left;
//		}
//		else{
//			return direction.y > 0 ? Direction.Up : Direction.Down;
//		}
//	}
	public static Direction fromRadians(float angle) {
		return Direction.fromDegrees(MathUtils.radiansToDegrees * angle);
	}
	public static Direction fromDegrees(float angle) {
		float positiveAngle = degreesToPositive(angle);
		if(positiveAngle <= 45){
			return Direction.Right;
		}
		else if(positiveAngle <= 90 + 45){
			return Direction.Up;
		}
		else if(positiveAngle <= 180 + 45){
			return Direction.Left;
		}
		else if(positiveAngle <= 360 - 45){
			return Direction.Down;
		}
		else{
			return Direction.Right;
		}
	}
	private static float degreesToPositive(float angle) {
		if(angle <= 360 && angle >= 0){
			return angle;
		}

		int intDiv = (int)(angle / 360);
		angle = angle - (intDiv * 360);

		return angle >= 0 ? angle : angle + 360;
	}
	public static Direction getInverse(Direction dir) {
		if(dir == null){
			return null;
		}

		Direction inverseDirection = null;
		switch (dir) {
		case Left:
			inverseDirection = Right;
			break;
		case Right:
			inverseDirection = Left;
			break;
		case Up:
			inverseDirection = Down;
			break;
		case Down:
			inverseDirection = Up;
			break;
		default:
			break;
		}
		return inverseDirection;
	}
}
