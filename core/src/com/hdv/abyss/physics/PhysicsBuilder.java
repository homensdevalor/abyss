package com.hdv.abyss.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class PhysicsBuilder {
	static PhysicsBuilder singleton;
	public static PhysicsBuilder instance() {
		if(singleton == null){
			singleton = new PhysicsBuilder();
		}
		return singleton;
	}

	@Deprecated
	public Body createBody(World world,Vector2 posInMeters,float angle, BodyType bodyType){
        BodyDef bodyDef = new BodyDef(); 
        bodyDef.type = bodyType;
        bodyDef.position.set(posInMeters.x,posInMeters.y);
        bodyDef.angle=angle;
        return world.createBody(bodyDef);
    }
	public Body createBody(World world,PhysicalProperties physicalProperties){
        BodyDef bodyDef = new BodyDef(); 
        Vector2 pos = physicalProperties.getStartPosition();
        bodyDef.position.set(pos.x,pos.y);
        bodyDef.type 		  = physicalProperties.getBodyType();
        bodyDef.angle		  = physicalProperties.getStartAngle();
        bodyDef.linearDamping = physicalProperties.getLinearDamping();
        bodyDef.fixedRotation = physicalProperties.isFixedRotation();
        
        return world.createBody(bodyDef);
    }

    public void createRectFixture(Body body, PhysicalProperties properties)
    {
	     Vector2 size = properties.getSize();
	     float w=size.x/2f;
	     float h=size.y/2f;
	     
	     PolygonShape bodyShape = new PolygonShape();
	     bodyShape.setAsBox(w,h);

	     FixtureDef fixtureDef = createFixtureDefinition(properties, bodyShape);

	     body.createFixture(fixtureDef);
	     bodyShape.dispose();
	 }

	private FixtureDef createFixtureDefinition(PhysicalProperties properties, PolygonShape bodyShape) {
		FixtureDef fixtureDef			= new FixtureDef();
		fixtureDef.density = properties.getDensity();
		fixtureDef.restitution = properties.getRestitution();
		fixtureDef.friction = properties.getFriction();
		fixtureDef.shape = bodyShape;
		fixtureDef.isSensor = properties.isSensor();
		fixtureDef.filter.categoryBits = properties.getCollisionCategory().getCategoryBits();
		fixtureDef.filter.maskBits = properties.getCollisionCategory().getCollisionMask();
		return fixtureDef;
	}
	public Body build(World world, PhysicalProperties properties) {
		Body body = createBody(world,properties);
		createRectFixture(body, properties);
		return body;
	}

	
}
