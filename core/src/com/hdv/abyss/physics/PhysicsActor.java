package com.hdv.abyss.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class PhysicsActor extends Actor implements Cloneable{
	private PhysicalWorld pWorld;
	Body body;
	Vector2 size;
	final PhysicalProperties physicalProperties;
	
	public PhysicsActor() {
		super();
		physicalProperties = new PhysicalProperties();
	}
	
	public void createBody(World world){
		this.body = generateBody(world);
		this.body.setUserData(this);
		this.size = physicalProperties.getSize().cpy();
	}
	protected Body generateBody(World world){
		return PhysicsBuilder.instance().build(world, this.physicalProperties);
	}
	
	/** Clona este ator, mas não adiciona ele ao mundo.*/
	@Override
	public Object clone(){
		PhysicsActor cloneActor = new PhysicsActor();
		cloneActor.set(this);
		return cloneActor;
	}
	
	public void set(PhysicsActor physicsActor) {
		this.getPhysicalProperties().set(physicsActor.getPhysicalProperties());
	}

	@Override
	public float getX() {
		return body == null ? super.getX() : this.body.getPosition().x;
	}
	@Override
	public float getY() {
		return body == null ? super.getX() : this.body.getPosition().y;
	}
//	@Override
	public float getCenterX() {
		return body == null ? super.getX() + getWidth()/2f : this.body.getWorldCenter().x;
	}
//	@Override
	public float getCenterY() {
		return body == null ? super.getX() + getHeight()/2f : this.body.getWorldCenter().y;
	}
	
	@Override
	public float getWidth() {
		return body == null ? super.getWidth() : this.getSizeInWorld().x;
	}
	@Override
	public float getHeight() {
		return body == null ? super.getWidth() : this.getSizeInWorld().y;
	}
	
	public Body getBody() {
		return body;
	}
	public Vector2 getSizeInWorld(){
		return size;
	}
	public Vector2 getPositionInWorld(){
		return body.getPosition();
	}
	public Vector2 getVelocity(){
		return body.getLinearVelocity().cpy();
	}
	
	@Override
	public void setPosition(float x, float y) {
		body.setTransform(x, y, body.getAngle());
	}
	
	public Direction getDirection(){
		Vector2 velocityDirection = body.getLinearVelocity().cpy();
		return Direction.roundFromVector(velocityDirection);
	}

	public PhysicalProperties getPhysicalProperties() {
		return physicalProperties;
	}
	public void setPhysicalProperties(PhysicalProperties physicalProperties) {
		this.physicalProperties.set(physicalProperties);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
	}
	
	public PhysicalWorld getPhysicalWorld() {
		return pWorld;
	}
	public void attachToWorld(PhysicalWorld world){
		pWorld = world;
		onAttachToWorld();
	}
	public void dettachFromWorld(){
		if(isAttachedToWorld()){
			this.onPrepareDettachToWorld();
			//Garante que ator não estará mais no mundo
			pWorld.removeActor(this);
			this.pWorld = null;
		}
	}
	public boolean isAttachedToWorld(){
		return (pWorld != null);
	}
	/** Este método é chamado logo após um PhysicsActor ser adicionado ao mundo*/
	protected void onAttachToWorld(){
		//Para ser implementado por classes filhas (opcional)
	}
	/** Este método é chamado logo antes de um PhysicsActor ser removido do mundo*/
	protected void onPrepareDettachToWorld(){
		//Para ser implementado por classes filhas (opcional)
	}
}
