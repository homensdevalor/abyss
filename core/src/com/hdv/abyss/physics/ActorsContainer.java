package com.hdv.abyss.physics;

import com.badlogic.gdx.scenes.scene2d.Actor;

public interface ActorsContainer{
	void addActor(Actor actor);
	void removeActor(Actor actor);
}