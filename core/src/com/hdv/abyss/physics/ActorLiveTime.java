package com.hdv.abyss.physics;

/** Esta classe permite manter uma associação entre um PhysicsActor e o tempo em que 
 * ele deve ser removido do mundo.*/
public class ActorLiveTime implements Comparable<ActorLiveTime>{
	long timeToDie;
	PhysicsActor actorToDestroy;
	public ActorLiveTime(long timeToDie, PhysicsActor actorToDestroy) {
		super();
		this.timeToDie = timeToDie;
		this.actorToDestroy = actorToDestroy;
	}
	public long getTimeToDie() {
		return timeToDie;
	}
	public void setTimeToDie(long timeToDie) {
		this.timeToDie = timeToDie;
	}
	public PhysicsActor getActorToDestroy() {
		return actorToDestroy;
	}
	public void setActorToDestroy(PhysicsActor actorToDestroy) {
		this.actorToDestroy = actorToDestroy;
	}
	
	@Override
	public int compareTo(ActorLiveTime other) {
		if(this.getTimeToDie() < other.getTimeToDie()){
			return -1;
		}
		else if(this.getTimeToDie() > other.getTimeToDie()){
			return 1;
		}
		return 0;
	}
}