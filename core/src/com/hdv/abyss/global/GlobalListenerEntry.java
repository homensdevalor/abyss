package com.hdv.abyss.global;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GlobalListenerEntry {

	private GlobalListener listener;
	private final List<Object> interestIDs;
	private final List<Object> interestTypes;

	public GlobalListenerEntry(GlobalListener listener){
		this(listener, Collections.emptyList(), Collections.emptyList());
	}
	public GlobalListenerEntry(GlobalListener listener, List<Object> interestIDs){
		this(listener,interestIDs, Collections.emptyList());
	}
	public GlobalListenerEntry(GlobalListener listener, List<Object> interestIDs, List<Object> interestTypes){
		this.listener = listener;
		this.interestIDs = new ArrayList<Object>(interestIDs);
		this.interestTypes = new ArrayList<Object>(interestTypes);
	}

	public GlobalListener getListener() {
		return listener;
	}
	public void setListener(GlobalListener listener) {
		this.listener = listener;
	}

	public List<Object> getInterestIDS() {
		return interestIDs;
	}
	public void setInterestIDs(List<Object> interestIDs) {
		this.interestIDs.clear();
		this.interestIDs.addAll(interestIDs);
	}
	public List<Object> getInterestTypes() {
		return interestTypes;
	}
	public void setInterestTypes(List<Object> interestTypes) {
		this.interestTypes.clear();
		this.interestTypes.addAll(interestTypes);
	}
	
}
