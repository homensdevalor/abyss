package com.hdv.abyss.global;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalEventCollector {
	private static GlobalEventCollector instance;
	
	private final Map<GlobalListener, GlobalListenerEntry> listeners;
	
	private GlobalEventCollector(){
		listeners = new HashMap<GlobalListener, GlobalListenerEntry>();
	}

	public void addListener(GlobalListener listener){
		this.addListener(listener, new Object[0]);
	}
	public void addListener(GlobalListener listener, Object ... eventIDs){
		GlobalListenerEntry entry = new GlobalListenerEntry(listener, convertTriggerArguments(eventIDs));
		listeners.put(listener, entry);
	}
	public void setListenerInterestIDS(GlobalListener listener, Object ... eventIDs){
		listeners.get(listener).setInterestIDs(Arrays.asList(eventIDs));
	}
	public void setListenerInterestTypes(GlobalListener listener, Object ... types){
		listeners.get(listener).setInterestTypes(Arrays.asList(types));
	}
	
	public void fireEvent(GlobalEvent evt){
		System.out.println("EVENTO GLOBAL DETECTADO " + evt.getEventID());
		for(GlobalListenerEntry entry: this.listeners.values()){
			List<Object> interests = entry.getInterestIDS();
			List<Object> interestTypes = entry.getInterestTypes();
			if(interests.contains(evt.getEventID()) || interestTypes.contains(evt.getType())){
				GlobalListener listener = entry.getListener();
				listener.onGlobalEvent(evt);
			}
		}
	}
	
	public static GlobalEventCollector getInstance(){
		if(instance == null){
			instance = new GlobalEventCollector();
		}
		return instance;
	}
	
	private List<Object> convertTriggerArguments(Object[] arguments){
		List<Object> result = new ArrayList<Object>();
		
		for(int a = 0; a < arguments.length; a++){
			result.add(arguments[a]);
		}
		
		return result;
	}
	
}
