package com.hdv.abyss.global;

import com.hdv.abyss.statemachine.events.GameEvent;

public class GlobalEvent implements GameEvent{

	public GlobalEvent(Object eventID, Object type){
		this.eventID = eventID;
		this.type = type;
	}
	
	private Object type;
	private Object eventID;
	
	public Object getType(){
		return this.type;
	}
	
	public Object getEventID(){
		return this.eventID;
	}
	
}
