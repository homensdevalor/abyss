package com.hdv.abyss.global;

public interface GlobalListener {
	
	public void onGlobalEvent(GlobalEvent evt);
	
}
