package com.hdv.abyss.global.events.type;

public enum GlobalEvents {
	LEVER_TOP_LEFT,
	LEVER_TOP_RIGHT,
	LEVER_BOTTOM_LEFT,
	LEVER_BOTTOM_RIGHT,
	MAIN_DOOR_ACCUMULATOR_SWITCH
}
