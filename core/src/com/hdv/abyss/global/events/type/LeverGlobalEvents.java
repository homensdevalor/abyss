package com.hdv.abyss.global.events.type;

public enum LeverGlobalEvents {
	LEVER_PRESSED,
	LEVER_RELEASED
}
