package com.hdv.abyss.global.events;

import com.hdv.abyss.global.GlobalEvent;

public class PauseEvent extends GlobalEvent {
	public enum PauseAction {Toggle, Pause, Resume};
	public static final Object PAUSE_TYPE = PauseEvent.class;
	
	public PauseEvent(PauseAction pauseId) {
		super(pauseId, PauseEvent.class);
	}
	
	public PauseAction getPauseAction(){
		return (PauseAction)super.getEventID();
	}
}
