package com.hdv.abyss.global.events;

import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.global.GlobalEventCollector;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.physics.collision.CollisionManager;
import com.hdv.abyss.physics.collision.CollisionManager.CollisionListener;

public class CollisionNotifier implements CollisionListener {
	public interface OnCollisionEventGenerator{
		GlobalEvent createEvent();
	}
	private PhysicsActor collider;
	private OnCollisionEventGenerator onCollisionEventGenerator;
	public CollisionNotifier() {
		this(null,null);
	}
	public CollisionNotifier(PhysicsActor collider, OnCollisionEventGenerator generator) {
		this.collider = collider;
		this.onCollisionEventGenerator = generator;
	}
	
	public PhysicsActor getCollider() {
		return collider;
	}
	public void setCollider(PhysicsActor collider) {
		this.collider = collider;
	}
	public OnCollisionEventGenerator getOnCollisionEventGenerator() {
		return onCollisionEventGenerator;
	}
	public void setOnCollisionEventGenerator(
			OnCollisionEventGenerator onCollisionEventGenerator) {
		this.onCollisionEventGenerator = onCollisionEventGenerator;
	}
	
	public void addToCollisionManager(CollisionManager manager){
		if(collider != null){
			manager.addCollisionListener(this, collider);
		}
	}
	public void removeFromCollisionManager(CollisionManager manager){
		if(collider != null){
			manager.removeCollisionListener(this, collider);
		}
	}
	@Override
	public void onCollision(CollisionEvent evt) {
		if(onCollisionEventGenerator != null){
			GlobalEventCollector.getInstance().fireEvent(onCollisionEventGenerator.createEvent());
		}
	}
}
