package com.hdv.abyss.global.accumulators;

import java.util.ArrayList;
import java.util.List;

import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.global.GlobalListener;

public abstract class GlobalAccumulator implements GlobalListener{

	protected List<Object> eventTypes;
	protected GlobalEvent toSendAtComplete;
	
	public GlobalAccumulator(GlobalEvent toSendAtComplete){
		this.toSendAtComplete = toSendAtComplete;
		eventTypes = new ArrayList<Object>();
	}
	
	
	
}
