package com.hdv.abyss.global.accumulators;

import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.global.GlobalEventCollector;
import com.hdv.abyss.global.events.type.LeverGlobalEvents;

public class GlobalDoorAccumulator extends GlobalAccumulator{

	private Object[] required;
	
	public GlobalDoorAccumulator(GlobalEvent toSendAtComplete, Object ... required){
		super(toSendAtComplete);
		this.required = required;
	}
	
	@Override
	public void onGlobalEvent(GlobalEvent evt) {
		if (evt.getType() == LeverGlobalEvents.LEVER_PRESSED){
			// Adicionar
			this.eventTypes.add(evt.getEventID());
		}else if(evt.getType() == LeverGlobalEvents.LEVER_RELEASED){
			// Remover
			if (this.eventTypes.contains(evt.getEventID())){
				this.eventTypes.remove(evt.getEventID());
			}
		}
		
		checkValid();
	}
	
	private void checkValid(){
		if(eventTypes.size() == 0){
			return;
		}
		
		boolean containsAll = true;
		for(Object req : this.required){
			containsAll = containsAll && this.eventTypes.contains(req);
		}
		
		if(containsAll){
			System.out.println("Launching global event on accumulator");
			GlobalEventCollector.getInstance().fireEvent(this.toSendAtComplete);
		}
	}
	
}
