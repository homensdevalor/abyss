package com.hdv.abyss;

import com.badlogic.gdx.ApplicationListener;

public class GameChooser {

	public static ApplicationListener createGame() {
		return new AbyssGame();
	}

}
