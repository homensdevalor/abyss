package com.hdv.abyss.statemachine;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseState;
import com.hdv.abyss.statemachine.states.BasicStates;
import com.hdv.abyss.statemachine.states.State;
import com.hdv.abyss.statemachine.transitions.Transition;

public class BaseStateMachine <Entity> implements StateMachine<Entity>{
	private final Map<Object, State<Entity> > states;
	private State<Entity> currentState;
	private Entity entity;
	private final List<OnStateChangeListener> stateChangeListeners;
	private Object defaultStateKey;
	private final BaseState<Entity> nullState;
	
	public final Object nullStateKey = new Object();
	
	public BaseStateMachine(Entity entity) 
	{
		stateChangeListeners = new ArrayList<OnStateChangeListener>();
		states = new HashMap<Object, State<Entity> >();
		this.entity = entity;
		nullState = new BaseState<Entity>(this);
		states.put(nullStateKey, nullState);
		currentState = nullState;
		defaultStateKey = null;
	}
	

	@Override
	public void update(float delta) {
		getCurrentState().update(delta);
	}

	@Override
	public void fireEvent(GameEvent event) {
		for(Transition t : currentState.getTransitions()){

			if(t.check(event)){
				Object nextStateKey = t.getNextStateKey();
				if(nextStateKey == BasicStates.DEFAULT){
					this.changeToDefaultState();
				}
				else{
					this.changeToState(nextStateKey, event);
				}
			}
		}
		//TODO: talvez não enviar evento para estado caso transição tenha ocorrido (pois ele já recebe no onStart);
		this.currentState.onEvent(event);
	}

	@Override
	public State<Entity> getCurrentState() {
		if(currentState == nullState && defaultStateKey != null){
			changeToDefaultState();
		}
		return currentState;
	}
	@Override
	public Object getDefaultStateKey() {
		return defaultStateKey;
	}
	@Override
	public void setDefaultState(Object defaultStateKey) {
		if(!this.states.containsKey(defaultStateKey)){
			throw new InvalidParameterException("There is no state with key " + defaultStateKey);
		}
		this.defaultStateKey = defaultStateKey;
	}
	@Override
	public State<Entity> getState(Object stateKey){
		return states.get(stateKey);
	}
	@Override
	public Object getStateKey(State<Entity> state){
		for(Entry<Object, State<Entity> > entry: states.entrySet()){
			if(entry.getValue().equals(state)){
				return entry.getKey();
			}
		}
		return null;
	}
	@Override
	public void addState(Object stateKey, State<Entity> state){
		assertParameterNotNull(stateKey, "stateKey");
		assertParameterNotNull(state, "state");
		this.states.put(stateKey, state);	
		state.setKey(stateKey);
		if(this.defaultStateKey == null){
			defaultStateKey = stateKey;
		}	
	}
	@Override
	public void removeState(Object stateKey){
		assertParameterNotNull(stateKey, "stateKey");

		this.states.remove(stateKey);
		
		if(states.isEmpty()){
			defaultStateKey = null;
		}
	}
	@Override
	public Collection<State<Entity> > getStates(){
		return new CopyOnWriteArrayList<State<Entity> >(this.states.values());
	}
	private void assertParameterNotNull(Object parameter, String paramName) {
		if(parameter == null){
			throw new InvalidParameterException("Parameter "+ paramName + " should not be null.");
		}
		
	}

	@Override
	public boolean changeToDefaultState() {
		return changeToState(BasicStates.DEFAULT);
	}
	@Override
	public boolean changeToState(Object stateKey) {
		return this.changeToState(stateKey, null);
	}
	@Override
	public boolean changeToState(Object stateKey, GameEvent event) {
		if(stateKey == BasicStates.DEFAULT){
			stateKey = defaultStateKey;
		}
		if(states.containsKey(stateKey)){
			State<Entity> previousState = currentState;
			State<Entity> newState = states.get(stateKey);
			if(newState != currentState && newState.onStart(event)){
				currentState.onStop();
				currentState = newState;
				fireChangeStateEvent(getStateKey(previousState), stateKey);
				
				return true;
			}
		}
		return false;
	}

	private void fireChangeStateEvent(Object previousStateKey, Object newStateKey) {
		for(OnStateChangeListener listener : stateChangeListeners){
			listener.onStateChange(new ChangedStateEvent<Entity>(this, previousStateKey, newStateKey, entity));
		}
	}

	@Override
	public Entity getEntity() {
		return this.entity;
	}

	@Override
	public void addOnStateChangeListener(OnStateChangeListener stateChangeListener) {
		this.stateChangeListeners.add(stateChangeListener);
	}

	@Override
	public void removeOnStateChangeListener(OnStateChangeListener stateChangeListener) {
		this.stateChangeListeners.remove(stateChangeListener);
	}


	@SuppressWarnings("unchecked")
	@Override
	public void setStates( Map<Object, State<? extends Entity>> statesMap, Object defaultStateKey) {
		this.clearStates();
		for(Entry<Object, State<? extends Entity>> entry : statesMap.entrySet()){
			this.addState(entry.getKey(), (State<Entity>)entry.getValue());
		}
		this.setDefaultState(defaultStateKey);
	}
	public void setStates( StateMapBuilder<Entity> mapBuilder) {
		this.setStates(mapBuilder.buildStates(this), mapBuilder.getDefaultStateKey());
	}
	private void clearStates() {
		states.clear();
		defaultStateKey = null;
		currentState = nullState;
	}
}
