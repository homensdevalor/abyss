package com.hdv.abyss.statemachine.states;

import com.badlogic.gdx.utils.TimeUtils;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;

public class TimedState<Entity> extends BaseState<Entity> {
	private long waitTime;
	private long startTime;
	private long waitedTime;
	private Object nextStateKey;
	public TimedState(StateMachine<Entity> machine, Object nextStateKey, long waitTime) {
		super(machine);
		this.waitTime = waitTime;
		this.nextStateKey = nextStateKey;
	}
	
	public long getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(long waitTime) {
		this.waitTime = waitTime;
	}
	public Object getNextStateKey() {
		return nextStateKey;
	}
	public void setNextStateKey(Object nextStateKey) {
		this.nextStateKey = nextStateKey;
	}

	@Override
	public boolean onStart(GameEvent startEvent) {
		this.startTime = TimeUtils.millis();
		waitedTime = 0;
		return super.onStart(startEvent);
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		waitedTime += (delta * 1000);
//		if(TimeUtils.millis() - startTime >= waitTime){
		if(waitedTime >= waitTime){
			onTimeOver();
			getStateMachine().changeToState(nextStateKey);
		}
	}

	/** Callback chamado quando tempo para transição acaba*/
	protected void onTimeOver()
	{
		/** Método a ser implementado por subclasses*/
	}
}
