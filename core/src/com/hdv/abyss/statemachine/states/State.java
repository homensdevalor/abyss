package com.hdv.abyss.statemachine.states;

import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.transitions.Transition;


public interface State<Entity> {
	void update(float delta);
	void onStop();
	boolean onStart(GameEvent startEvent);
	void onEvent(GameEvent event);

	Object getKey();
	void setKey(Object key);
	void addTransition(Transition transition);
	void addTransitions(Transition ...transitions);
	void removeTransition(Transition transition);
	void clearTransitions();
	Iterable<Transition> getTransitions();
}
