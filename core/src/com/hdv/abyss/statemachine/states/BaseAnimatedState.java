package com.hdv.abyss.statemachine.states;

import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.statemachine.StateMachine;

public class BaseAnimatedState<Entity> extends BaseState<Entity> implements AnimatedState<Entity>{
	private DirectionalAnimation animation;
	public BaseAnimatedState(StateMachine<Entity> machine) {
		super(machine);
	}
	@Override
	public DirectionalAnimation getAnimation() {
		return animation;
	}
	@Override
	public void setAnimation(DirectionalAnimation animation) {
		this.animation = animation;
	}
}
