package com.hdv.abyss.statemachine.states;

import com.hdv.abyss.anim.DirectionalAnimation;

public interface AnimatedState<Entity> extends State<Entity>{
	public DirectionalAnimation getAnimation();
	public void setAnimation(DirectionalAnimation animation);
}
