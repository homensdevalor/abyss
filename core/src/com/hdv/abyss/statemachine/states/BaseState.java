package com.hdv.abyss.statemachine.states;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.transitions.Transition;

public class BaseState<Entity> implements State<Entity> {
	private Object stateKey;
	private final List<Transition> transitions;
	private StateMachine<Entity> stateMachine;
	
	public BaseState(StateMachine<Entity> machine) {
		transitions = new ArrayList<Transition>();
		this.stateMachine = machine;
	}
	@Override
	public void addTransitions(Transition... transitions) {
		this.transitions.addAll(Arrays.asList(transitions));
	}
	public void addTransition(Transition transition){
		this.transitions.add(transition);
	}
	public void removeTransition(Transition transition){
		this.transitions.remove(transition);
	}
	@Override
	public void clearTransitions() {
		this.transitions.clear();
	}
	
	protected StateMachine<Entity> getStateMachine() {
		return this.stateMachine;
	}
	
	@Override
	public void update(float delta) 
	{ /* do nothing here.*/ }
	@Override
	public void onStop() 
	{ /* do nothing here.*/ }
	@Override
	public boolean onStart(GameEvent startEvent)
	{ /* do nothing here.*/ 
		return true;
	}
	@Override
	public void onEvent(GameEvent event) 
	{ /* do nothing here.*/}
	
	@Override
	public Iterable<Transition> getTransitions() {
		return Collections.unmodifiableList(this.transitions);
	}
	@Override
	public Object getKey() {
		return stateKey;
	}
	@Override
	public void setKey(Object key) {
		stateKey = key;
	}

}
