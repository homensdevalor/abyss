package com.hdv.abyss.statemachine.states;

import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;

public class TimedAnimatedState<Entity> extends TimedState<Entity> implements AnimatedState<Entity> {
	private DirectionalAnimation animation;
	public TimedAnimatedState(StateMachine<Entity> machine,
			Object nextStateKey, long waitTime) {
		super(machine, nextStateKey, waitTime);
	}
	@Override
	public boolean onStart(GameEvent startEvent) {
		return super.onStart(startEvent);
	}
	@Override
	public DirectionalAnimation getAnimation() {
		return animation;
	}

	@Override
	public void setAnimation(DirectionalAnimation animation) {
		this.animation = animation;
	}

}
