package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.statemachine.events.GameEvent;

public interface Transition {

	boolean check(GameEvent event);

	Object getNextStateKey();

}
