package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.MovementEvent;

public class OnStopTransition extends BaseTransition {
	public OnStopTransition(Object nextState) {
		super(nextState);
	}

	@Override
	public boolean check(GameEvent event) {
		if(event != null && event instanceof MovementEvent){
			MovementEvent evt = (MovementEvent)event;
			//Se direção tiver magnitude 0, significa que personagem parou
			return evt.getDirection().len2() == 0;
		}
		return false;
	}

}
