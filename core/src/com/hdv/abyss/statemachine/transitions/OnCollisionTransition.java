package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.physics.collision.matcher.CollisionMatcher;
import com.hdv.abyss.statemachine.events.GameEvent;

public class OnCollisionTransition extends BaseTransition {
	private OnEventTransition checkCollisionEvent;
	private CollisionMatcher matcher;
	public OnCollisionTransition(Object nextState, CollisionMatcher matcher) {
		super(nextState);
		checkCollisionEvent = new OnEventTransition(CollisionEvent.class, null);
		this.matcher = matcher;
	}

	@Override
	public boolean check(GameEvent event) {
		if(checkCollisionEvent.check(event)){
			return matcher.match((CollisionEvent)event);
		}
		return false;
	}

}
