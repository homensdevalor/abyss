package com.hdv.abyss.statemachine.transitions;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.hdv.abyss.statemachine.events.GameEvent;

public class AndTransition extends BaseTransition{
	final List<Transition> transitions;
	
	public AndTransition(Object nextState, Transition ... transitions) {
		super(nextState);
		this.transitions = Arrays.asList(transitions);
	}

	public List<Transition> getTransitions(){
		return new CopyOnWriteArrayList<Transition>(transitions);
	}
	public void setTransitions(Transition ... transitions){
		this.setTransitions(Arrays.asList(transitions));
	}
	public void setTransitions(List<Transition> transitions){
		this.transitions.clear();
		this.transitions.addAll(transitions);
	}
	public void addTransition(Transition ... transitions){
		this.transitions.addAll(Arrays.asList(transitions));
	}
	public void removeTransition(Transition ... transitions){
		this.transitions.removeAll(Arrays.asList(transitions));
	}
	
	@Override
	public boolean check(GameEvent event) {
		for(Transition t: transitions){
			if(!t.check(event)){
				return false;
			}
		}
		return true;
	}

}
