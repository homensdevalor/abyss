package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.statemachine.events.GameEvent;

public abstract class BaseTransition implements Transition {
	Object nextState;
	public BaseTransition(Object nextState) {
		this.nextState = nextState;
	}
	@Override
	public abstract boolean check(GameEvent event);

	@Override
	public Object getNextStateKey() {
		return nextState;
	}

}
