package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.statemachine.events.GameEvent;

public class OnEventTransition extends BaseTransition {
	Class<? extends Object> type;
	public OnEventTransition(Class<? extends Object> eventType, Object nextState) {
		super(nextState);
		this.type = eventType;
	}

	@Override
	public boolean check(GameEvent event) {
		return event != null && type.isInstance(event);
	}

}
