package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.events.MovementEvent;

public class OnMoveTransition extends BaseTransition {
	private boolean allowStopMovement;
	
	private Movement moveType;
	public OnMoveTransition(Object nextState) {
		this(nextState, null);
	}

	public OnMoveTransition(Object nextState, Movement moveType) {
		this(nextState,moveType,false);
	}
	public OnMoveTransition(Object nextState, Movement moveType, boolean allowStopMovement) {
		super(nextState);
		this.moveType = moveType;
		this.allowStopMovement = allowStopMovement;
	}

	@Override
	public boolean check(GameEvent event) {
		if(event != null && event instanceof MovementEvent){
			MovementEvent evt = (MovementEvent)event;
			if(!allowStopMovement){
				//Se direção tiver magnitude diferente de 0, significa que personagem se moveu
				return (evt.getDirection().len2() != 0) && checkMoveType(evt);
			}
			else{
				return checkMoveType(evt);
			}
		}
		return false;
	}

	private boolean checkMoveType(MovementEvent evt) {
		if(this.moveType == null){
			return true;
		}
		return this.moveType == evt.getMovementType();
	}

	public boolean isAllowStopMovement() {
		return allowStopMovement;
	}

	public void setAllowStopMovement(boolean allowStopMovement) {
		this.allowStopMovement = allowStopMovement;
	}

}
