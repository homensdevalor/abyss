package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.utils.Vectors;

public class DistanceTransition extends BaseTransition {
	public enum DistanceType{Approach, Depart};
	private float distance;
	private PhysicsActor owner, target;
	private DistanceType distanceType;
	public DistanceTransition(Object nextState, PhysicsActor owner, PhysicsActor target, float approachDistance) {
		this(nextState, owner, target, approachDistance, DistanceType.Approach);
	}
	public DistanceTransition(Object nextState, PhysicsActor owner, PhysicsActor target, float approachDistance
			, DistanceType type) {
		super(nextState);
		this.owner = owner;
		this.target = target;
		this.distance = approachDistance;
		this.distanceType = type;
	}

	@Override
	public boolean check(GameEvent event) {
		float currentDistance = Vectors.distance(owner.getPositionInWorld(), target.getPositionInWorld());
		boolean changed =  distanceType == DistanceType.Approach ? currentDistance <= distance 
														: currentDistance > distance;						
		return changed;
	}
}
