package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.actors.weapon.Attack;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.statemachine.events.GameEvent;

public class OnReceivedAttackTransition extends OnEventTransition{
	private Object receiver;
	public OnReceivedAttackTransition(Object receiver, Object nextState) {
		super(CollisionEvent.class, nextState);
		this.receiver = receiver;
	}
	@Override
	public boolean check(GameEvent event) {
		if(super.check(event)){
			CollisionEvent evt = (CollisionEvent)event;
			if(evt.containsInstanceOf(Attack.class) && evt.contains(receiver)){
				return ((Attack)evt.getColliderOfType(Attack.class)).getAttackCreator() != receiver;
			}
		}
		return false;
	}
}
