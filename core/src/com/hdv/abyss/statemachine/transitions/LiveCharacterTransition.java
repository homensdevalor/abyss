package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.events.GameEvent;

public class LiveCharacterTransition extends BaseTransition {
	public enum LiveState{
		Live, Dead
	}
	
	GameCharacter target;
	LiveState liveState;
	
	public LiveCharacterTransition(Object nextState, GameCharacter target) {
		this(nextState, target, LiveState.Live);
	}
	public LiveCharacterTransition(Object nextState, GameCharacter target, LiveState liveState) {
		super(nextState);
		this.target = target;
		this.liveState = liveState;
	}

	@Override
	public boolean check(GameEvent event) {
		return liveState == LiveState.Live 
				? target.getHealthValue().getValue() > 0
				: target.getHealthValue().getValue() <= 0;
	}

}
