package com.hdv.abyss.statemachine.transitions;

import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.statemachine.events.GameEvent;

public class OnGlobalTransition extends OnEventTransition{

	private Object eventType;
	
	public OnGlobalTransition(Object nextState, Object eventType) {
		super(GlobalEvent.class, nextState);
		this.eventType = eventType;
	}
	
	@Override
	public boolean check(GameEvent event) {
		if(super.check(event)){
			GlobalEvent evt = (GlobalEvent) event;
			return evt.getType().equals(this.eventType);
		}
		return false;
	}

}
