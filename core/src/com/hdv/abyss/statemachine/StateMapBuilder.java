package com.hdv.abyss.statemachine;

import java.util.Map;

import com.hdv.abyss.statemachine.states.State;

public interface StateMapBuilder <T> {
	Object getDefaultStateKey();
	Map<Object, State<? extends T>> buildStates(StateMachine<T> stateMachine);
}
