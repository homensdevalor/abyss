package com.hdv.abyss.statemachine.events;


public class TimeEvent implements GameEvent {
	private long currentTimeMillis;
	
	public TimeEvent(long millis) {
		currentTimeMillis = millis;
	}

	public long getCurrentTimeMillis() {
		return currentTimeMillis;
	}
}
