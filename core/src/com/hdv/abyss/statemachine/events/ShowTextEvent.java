package com.hdv.abyss.statemachine.events;

import com.hdv.abyss.global.GlobalEvent;

public class ShowTextEvent extends GlobalEvent{
	public final static String ShowTextEventType = ShowTextEvent.class.getName();
	
	public enum ShowTextType{Animate, Show, Skip, Hide};
	
	private ShowTextType showTextType;
	private String text;
	private long timeToStart;

	public ShowTextEvent(String text, ShowTextType type) {
		this(text, type, 0);
	}
	public ShowTextEvent(String text, ShowTextType type, long timeToStart) {
		super(ShowTextEventType,ShowTextEventType);
		this.text = text;
		this.showTextType = type;
		this.timeToStart = timeToStart;
	}

	public ShowTextType getShowTextType() {
		return showTextType;
	}
	public void setShowTextType(ShowTextType showTextType) {
		this.showTextType = showTextType;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public long getTimeToStart() {
		return timeToStart;
	}	
}
