package com.hdv.abyss.statemachine.events;

import com.badlogic.gdx.math.Vector2;

public class StopEvent extends MovementEvent{
	public StopEvent() {
		super(new Vector2(0,0), Movement.NORMAL);
	}
}
