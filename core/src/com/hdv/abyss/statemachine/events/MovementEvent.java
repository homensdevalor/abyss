package com.hdv.abyss.statemachine.events;

import com.badlogic.gdx.math.Vector2;

public class MovementEvent implements GameEvent {
	private final Vector2 direction;
	private final Movement movementType;
	public MovementEvent(Vector2 direction, Movement movement) {
		this.direction = direction.cpy();
		this.movementType = movement;
	}
	public Vector2 getDirection() {
		return direction.cpy();
	}
	public Movement getMovementType() {
		return movementType;
	}
}
