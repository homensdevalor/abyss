package com.hdv.abyss.statemachine;

import java.util.Collection;
import java.util.Map;

import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.State;

public interface StateMachine <Entity> {
	void update(float delta);
	void fireEvent(GameEvent event);
	Entity getEntity();

	State<Entity> getCurrentState();
	Object getDefaultStateKey();
	void  setDefaultState(Object defaultStateKey);
	Object getStateKey(State<Entity> state);
	State<Entity> getState(Object stateKey);
	Collection<State<Entity> > getStates();
	
	
	void removeState(Object stateKey);
	void addState(Object stateKey, State<Entity> state);
	
	boolean changeToState(Object stateKey);
	boolean changeToState(Object stateKey, GameEvent event);
	boolean changeToDefaultState();	
	
	void removeOnStateChangeListener(OnStateChangeListener stateChangeListener);
	void addOnStateChangeListener(OnStateChangeListener stateChangeListener);
	
	void setStates(StateMapBuilder<Entity> mapBuilder);
	void setStates(Map<Object, State<? extends Entity>> buildStates, Object defaultStateKey);
}
