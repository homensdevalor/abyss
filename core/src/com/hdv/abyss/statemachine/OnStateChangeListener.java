package com.hdv.abyss.statemachine;

public interface OnStateChangeListener {
	void onStateChange(ChangedStateEvent<?> evt);
}
