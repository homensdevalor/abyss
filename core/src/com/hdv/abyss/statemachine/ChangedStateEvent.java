package com.hdv.abyss.statemachine;

import com.hdv.abyss.statemachine.states.State;

public class ChangedStateEvent<Entity> {
	final State<Entity> previousState;
	final State<Entity> newState;
	final Object newStateKey, previousStateKey;
	final Entity entity;
	public ChangedStateEvent(StateMachine<Entity> stateMachine, Object previousStateKey, Object newStateKey, Entity entity) {
		super();
		this.previousState = stateMachine.getState(previousStateKey);
		this.newState = stateMachine.getState(newStateKey);
		this.newStateKey = newStateKey;
		this.previousStateKey = previousStateKey;
		this.entity = entity;
	}
	public State<Entity> getPreviousState() {
		return previousState;
	}
	public State<Entity> getNewState() {
		return newState;
	}
	public Object getPreviousStateKey(){
		return previousStateKey;
	}
	public Object getNewStateKey(){
		return newStateKey;
	}
	public Entity getEntity() {
		return entity;
	}
}
