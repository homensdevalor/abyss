package com.hdv.abyss.health;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.hdv.abyss.health.event.HealthStateEvent;
import com.hdv.abyss.health.event.HealthValueEvent;
import com.hdv.abyss.health.event.HitEvent;

public class HealthController {
	
	private boolean isDisabled;
	private HealthValue healthValue;
	private List<HealthState> healthStates;
	private List<HealthValueListener> healthValueListeners;
	private List<HealthStateListener> healthStateListeners;
	
	public HealthController(int maxHealth){
		this.isDisabled = false;
		this.healthValue = new HealthValue(maxHealth);
		this.healthStates = new ArrayList<HealthState>();
		this.healthValueListeners = new ArrayList<HealthValueListener>();
	}
	
	public HealthValue getHealthValue(){
		return this.healthValue;
	}
	
	public void addStateListener(HealthStateListener listener){
		healthStateListeners.add(listener);
	}
	public void removeStateListener(HealthStateListener healthStateListener) {
		this.healthStateListeners.remove(healthStateListener);
	}
	
	public void addValueListener(HealthValueListener listener){
		this.healthValueListeners.add(listener);
	}
	public void removeValueListener(HealthValueListener healthValueListener) {
		this.healthValueListeners.remove(healthValueListener);
	}
	
	public void addState(HealthState state){
		this.healthStates.add(state);
		HealthStateEvent evt = new HealthStateEvent(state, HealthStateEvent.HealthStateType.ADD);
		
		// Lançando evento de estado alterado
		notifyStateListeners(evt);
	}
	
	public void removeState(HealthState state){
		this.healthStates.remove(state);
		HealthStateEvent evt = new HealthStateEvent(state, HealthStateEvent.HealthStateType.REMOVE);
		
		// Lançando evento de estado alterado
		notifyStateListeners(evt);
	}
	
	public void onHit(HitEvent event){
		if(isDisabled){
			return;
		}
		Gdx.app.log(getClass().getName(), "Evento de hit detectado");
		
		int healthDamage = event.getHitValue();
		int previousValue = healthValue.getValue();
		int newHpValue = previousValue - healthDamage;
		
		// Definindo novo valor
		healthValue.setValue(newHpValue);
		
		// Lançando evento de modificação de valor para o HP
		HealthValueEvent evt = new HealthValueEvent(previousValue, newHpValue);
		notifyValueListeners(evt);
	}
	
	private void notifyStateListeners(HealthStateEvent event){
		for(HealthStateListener listener : healthStateListeners){
			listener.onHealthStateEvent(event);
		}
	}
	
	private void notifyValueListeners(HealthValueEvent event){
		for(HealthValueListener listener : healthValueListeners){
			listener.onHealthValueEvent(event);
		}
	}
	
	public void setEnabled(boolean isEnabled){
		this.isDisabled = !isEnabled;
	}
}
