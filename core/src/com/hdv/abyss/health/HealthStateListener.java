package com.hdv.abyss.health;

import com.hdv.abyss.health.event.HealthStateEvent;

public interface HealthStateListener {

	public void onHealthStateEvent(HealthStateEvent event);
	
}
