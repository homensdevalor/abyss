package com.hdv.abyss.health;

public interface HealthState {
	
	public void modify(HealthController controller, float deltaTime);
	public Object getKey();
	
}
