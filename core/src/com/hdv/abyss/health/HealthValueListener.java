package com.hdv.abyss.health;

import com.hdv.abyss.health.event.HealthValueEvent;

public interface HealthValueListener {
	
	public void onHealthValueEvent(HealthValueEvent event);
	
}
