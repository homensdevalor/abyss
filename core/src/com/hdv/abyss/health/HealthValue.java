package com.hdv.abyss.health;

public class HealthValue {

	private int value;
	private int maxValue;
	
	public HealthValue(int maxValue){
		this.maxValue = maxValue;
		value = maxValue;
	}

	public void set(HealthValue other) {
		this.maxValue = other.getMaxValue();
		this.value = other.getMaxValue();
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		if(value < 0){
			this.value = 0;
		}
		
		if(value > this.maxValue){
			this.value = this.maxValue;
		}else{
			this.value = value;
		}
	}
	
	public int getMaxValue() {
		return maxValue;
	}
	
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
		//Limitando valor de saúde para nunca ser superior ao valor máximo
		if(value > maxValue){
			this.value = maxValue;
		}
	}

	
	
	
}
