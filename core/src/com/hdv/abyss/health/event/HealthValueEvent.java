package com.hdv.abyss.health.event;

public class HealthValueEvent {
	
	private int previousValue;
	private int actualValue;
	
	public HealthValueEvent(int previousValue, int actualValue){
		this.previousValue = previousValue;
		this.actualValue = actualValue;
	}

	public int getPreviousValue() {
		return previousValue;
	}

	public int getActualValue() {
		return actualValue;
	}
	
}
