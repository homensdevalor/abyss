package com.hdv.abyss.health.event;

import com.hdv.abyss.health.HealthState;

public class HealthStateEvent {
	
	public enum HealthStateType{
		ADD,
		REMOVE
	}
	
	private HealthState state;
	private HealthStateType type;
	
	public HealthStateEvent(HealthState state, HealthStateType stateType){
		this.state = state;
		this.type = stateType;
	}

	public HealthState getState() {
		return state;
	}

	public HealthStateType getType() {
		return type;
	}
	
}
