package com.hdv.abyss.health.event;

public class HitEvent{

	private int hitValue;
	
	public HitEvent(int hitValue){
		this.hitValue = hitValue;
	}

	public int getHitValue() {
		return hitValue;
	}
	
}
