package com.hdv.abyss.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;

public class Vectors {
	public static <T extends Vector<T> > float distance(T vec0, T  vec1){
		return vec0.cpy().sub(vec1).len();
	}
	public static float vectorToAngle (Vector2 vector) {
        return (float)Math.atan2(vector.y, vector.x);
    }
	public static Vector2 angleToVector (Vector2 outVector, float angle) {
        outVector.x = (float)Math.cos(angle);
        outVector.y = (float)Math.sin(angle);
        return outVector.nor();
    }
	public static float angleBetween(Vector2 vec1, Vector2 vec2) {
		float angle1 = vectorToAngle(vec1.cpy().nor());
		float angle2 = vectorToAngle(vec2.cpy().nor());
		float deltaAngle = Math.max(angle1, angle2) - Math.min(angle1,angle2);
		
		return deltaAngle > MathUtils.PI 
									? (MathUtils.PI*2) - deltaAngle 
								    : deltaAngle;
	}
}
