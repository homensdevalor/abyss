package com.hdv.abyss.utils;

import com.badlogic.gdx.utils.TimeUtils;

public class Counter {
	private long startTime;
	private long countTime;
	private boolean counting;
	
	public void startCount(long timeToCountInMillis){
		startTime = TimeUtils.millis();
		countTime = timeToCountInMillis;
		counting = true;
	}
	public long getCountedTime(){
		return TimeUtils.millis() - startTime;
	}
	public long getTimeToCount(){
		return countTime;
	}
	public boolean isCounting(){
		return counting;
	}
	public boolean finishedCount(){
		if(isCounting()){
			boolean finished = TimeUtils.millis() >= startTime + countTime;
			if(finished){
				counting = false;
			}
			return finished;
		}
		return true;
	}
}
