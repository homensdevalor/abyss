package com.hdv.abyss.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Actors {
	public static Vector2 getCenter(Actor actor){
		float centerX = actor.getX() + actor.getWidth()/2f;
		float centerY = actor.getY() + actor.getHeight()/2f;
		
		return new Vector2(centerX, centerY);
	}
	public static float outerDistance(Vector2 pos, Actor actor){
		float leftX = actor.getX();
		float rightX = leftX + actor.getWidth();
		float centerX = leftX + actor.getWidth()/2f;
		float x = (pos.x < leftX) ? leftX 
								  : (pos.x > rightX ? rightX : centerX);
		
		float down    = actor.getY();
		float up      = down + actor.getHeight();
		float centerY = down + actor.getHeight()/2f;
		float y = (pos.y < down) ? down 
								 : (pos.y > up ? up : centerY);
		
		return Vectors.distance(pos, new Vector2(x,y));
	}
}
