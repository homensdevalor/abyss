package com.hdv.abyss.actors;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.configurators.CharacterBuilderConfigurator;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.entities.LeverActor;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.global.GlobalEventCollector;
import com.hdv.abyss.global.events.type.LeverSwitch;
import com.hdv.abyss.statemachine.StateMapBuilder;

public class LeverBuilder extends GameCharacterBuilder{

	private LeverSwitch trigger;
	
	public LeverBuilder(CharacterBuilderConfigurator configurator){
		super(configurator);
	}
	
	public LeverSwitch getTrigger() {
		return trigger;
	}

	public void setTrigger(LeverSwitch trigger) {
		this.trigger = trigger;
	}

//	public LeverBuilder(){
//		super(new LeverBuilderConfigurator());
//	}
//	
//	public LeverActor buildLever(Object targetTrigger){
//		return buildLeverWithObject(build(), targetTrigger);
//	}
//	
//	public LeverActor buildLever(Object targetTrigger, Vector2 pos){
//		return buildLeverWithObject(build(pos), targetTrigger);
//	}
//	
//	public LeverActor buildLever(Object targetTrigger, Vector2 pos, Vector2 size){
//		return buildLeverWithObject(build(pos, size), targetTrigger);
//	}
//	
//	private LeverActor buildLeverWithObject(GameCharacter character, Object targetTrigger){
//		LeverActor lever = (LeverActor) character;
//		
//		lever.setTargetTrigger(targetTrigger);
//		return lever;
//	}
	
	@Override
	public GameCharacter build(Vector2 pos, Vector2 size) {
		LeverActor lever = (LeverActor) super.build(pos, size);
		lever.setTargetTrigger(this.trigger);
		GlobalEventCollector.getInstance().addListener(lever, lever.getTargetTrigger());
		return lever;
	}
	
	@Override
	protected GameCharacter buildNewCharacter(AnimationMapBuilder animMapBuilder, StateMapBuilder<GameCharacter> stateMapBuilder) {
		GameCharacter character = new LeverActor(animMapBuilder.buildMap(), stateMapBuilder);
		return character;
	}
	
}
