package com.hdv.abyss.actors.door.states;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.physics.PhysicalWorld;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class OpenDoorState extends BaseAnimatedState<GameCharacter>{

	public OpenDoorState(StateMachine<GameCharacter> machine) {
		super(machine);
	}
	
	
	@Override
	public boolean onStart(GameEvent startEvent) {
		final GameCharacter character = getStateMachine().getEntity();
		character.setColor(0, 0, 0, 0);
		character.setCurrentAnimation(this.getAnimation());
		
		// GlobalEventCollector.getIntance().fireEvent(new LeverPressedGlobalEvent(LeverSwitchs.MAIN_DOOR_LEVER_SWITCH));
		
		PhysicalWorld world = character.getPhysicalWorld();
		world.afterStep(new Runnable(){
			final GameCharacter characterToInativate = character;
			@Override
			public void run() {
				characterToInativate.getBody().setActive(false);
			}
		});
		return true;
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		
		final GameCharacter character = getStateMachine().getEntity();
		character.setColor(1, 1, 1, 1);
		character.getPhysicalWorld().afterStep(new Runnable(){
			final GameCharacter characterToInativate = character;
			@Override
			public void run() {
				characterToInativate.getBody().setActive(true);
			}
		});
	}
	
	
}
