package com.hdv.abyss.actors.door.states;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.states.CharacterStillState;
import com.hdv.abyss.global.events.type.LeverGlobalEvents;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.StateMapBuilder;
import com.hdv.abyss.statemachine.states.State;
import com.hdv.abyss.statemachine.transitions.OnGlobalTransition;
import com.hdv.abyss.statemachine.transitions.Transition;

public class OpenedDoorStateMapBuilder implements StateMapBuilder<GameCharacter> {

	final Map<Object, State<? extends GameCharacter> > stateMap;
	
	public OpenedDoorStateMapBuilder() {
		stateMap = new HashMap<Object, State<? extends GameCharacter>>();
	}
	
	@Override
	public Object getDefaultStateKey() {
		return DoorStates.OPEN;
	}

	@Override
	public Map<Object, State<? extends GameCharacter>> buildStates(
			StateMachine<GameCharacter> stateMachine) {
		
		Transition receivedGlobalTransitionAndIsClosed = new OnGlobalTransition(DoorStates.OPEN, LeverGlobalEvents.LEVER_RELEASED);
		Transition receivedGlobalTransitionAndIsOpen = new OnGlobalTransition(DoorStates.CLOSED, LeverGlobalEvents.LEVER_PRESSED);

		this.createState(DoorStates.OPEN, new CharacterStillState(stateMachine), 
				receivedGlobalTransitionAndIsOpen);
		this.createState(DoorStates.CLOSED, new ClosedDoorState(stateMachine), 
				receivedGlobalTransitionAndIsClosed);
		
		return stateMap;
	}

	
	
	protected void createState(Object stateKey, State<GameCharacter> state, Transition ... transitions) {
		state.addTransitions(transitions);
		stateMap.put(stateKey, state);
	}
	
}
