package com.hdv.abyss.actors.door.states;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class ClosedDoorState extends BaseAnimatedState<GameCharacter>{

	public ClosedDoorState(StateMachine<GameCharacter> machine) {
		super(machine);
	}
	
	@Override
	public boolean onStart(GameEvent startEvent) {
		final GameCharacter character = getStateMachine().getEntity();
		character.setColor(1, 1, 1, 1);
		character.setCurrentAnimation(this.getAnimation());
		
		character.getPhysicalWorld().afterStep(new Runnable(){
			final GameCharacter characterToInativate = character;
			@Override
			public void run() {
				characterToInativate.getBody().setActive(true);
			}
		});
		
		return true;
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		final GameCharacter character = getStateMachine().getEntity();
		character.setColor(0, 0, 0, 0);
		character.setCurrentAnimation(this.getAnimation());
		
		character.getPhysicalWorld().afterStep(new Runnable(){
			final GameCharacter characterToInativate = character;
			@Override
			public void run() {
				characterToInativate.getBody().setActive(false);
			}
		});
	}
	
	
}
