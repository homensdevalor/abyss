package com.hdv.abyss.actors.statemachine.statebuilders;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.actors.statemachine.states.CharacterDamageState;
import com.hdv.abyss.actors.statemachine.states.CharacterDodgingState;
import com.hdv.abyss.actors.statemachine.states.CharacterDyingState;
import com.hdv.abyss.actors.statemachine.states.CharacterMovingState;
import com.hdv.abyss.actors.statemachine.states.CharacterStillState;
import com.hdv.abyss.actors.statemachine.states.CharacterWeaponAttackingState;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.StateMapBuilder;
import com.hdv.abyss.statemachine.events.AttackEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.states.BasicStates;
import com.hdv.abyss.statemachine.states.State;
import com.hdv.abyss.statemachine.transitions.OnEventTransition;
import com.hdv.abyss.statemachine.transitions.OnMoveTransition;
import com.hdv.abyss.statemachine.transitions.OnReceivedAttackTransition;
import com.hdv.abyss.statemachine.transitions.OnStopTransition;
import com.hdv.abyss.statemachine.transitions.Transition;

public class BattleStateMapBuilder implements StateMapBuilder<GameCharacter> {
	final Map<Object, State<? extends GameCharacter> > stateMap;
	
	public BattleStateMapBuilder() {
		stateMap = new HashMap<Object, State<? extends GameCharacter>>();
	}
	
	@Override
	public Object getDefaultStateKey() {
		return BattleCharacterStates.STILL;
	}

	@Override
	public Map<Object, State<? extends GameCharacter>> buildStates(StateMachine<GameCharacter> stateMachine) {

		Transition walkTransition = new OnMoveTransition(BattleCharacterStates.MOVING, Movement.NORMAL);
		Transition dodgeTransition = new OnMoveTransition(BattleCharacterStates.DODGING, Movement.DODGE);
		Transition attackTransition = new OnEventTransition(AttackEvent.class, BattleCharacterStates.ATTACKING);
		Transition receivedAttackTransition = new OnReceivedAttackTransition(stateMachine.getEntity(), BattleCharacterStates.DAMAGED);
		Transition stopTransition = new OnStopTransition(BasicStates.DEFAULT);
		
		this.createState(BattleCharacterStates.STILL, new CharacterStillState(stateMachine), 
				walkTransition, dodgeTransition, attackTransition, receivedAttackTransition);
		this.createState(BattleCharacterStates.MOVING, new CharacterMovingState(stateMachine), 
				stopTransition, dodgeTransition, attackTransition, receivedAttackTransition);
		this.createState(BattleCharacterStates.ATTACKING, new CharacterWeaponAttackingState(stateMachine), 
				receivedAttackTransition);
		this.createState(BattleCharacterStates.DAMAGED, new CharacterDamageState(stateMachine));
		this.createState(BattleCharacterStates.DODGING, new CharacterDodgingState(stateMachine),
				receivedAttackTransition);
		this.createState(BattleCharacterStates.DEAD, new CharacterDyingState(stateMachine));
		
		return stateMap;
	}


	protected void createState(Object stateKey, State<GameCharacter> state, Transition ... transitions) {
		state.addTransitions(transitions);
		stateMap.put(stateKey, state);
	}
}
