package com.hdv.abyss.actors.statemachine.statebuilders;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.actors.statemachine.states.CharacterWeaponAttackingState;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.StateMapBuilder;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.State;
import com.hdv.abyss.statemachine.states.TimedAnimatedState;
import com.hdv.abyss.statemachine.transitions.Transition;

/** Construtor de um conjunto de estados para uma ator que emite ataques em sequência.
 * Ex.: um atirador de flechas ou um espinho.*/
public class AttackMachineStateBuilder implements StateMapBuilder<GameCharacter>{
	//Tempo em milisegundos
	private static final long DEFAULT_WAITING_TIME = 300;
	final Map<Object, State<? extends GameCharacter> > stateMap;
	private long waitingTime;

	public AttackMachineStateBuilder() {
		this(DEFAULT_WAITING_TIME);
	}
	public AttackMachineStateBuilder(long waitingTime) {
		stateMap = new HashMap<Object, State<? extends GameCharacter>>();
		this.waitingTime = waitingTime;
	}
	
	public long getWaitingTime() {
		return waitingTime;
	}
	public void setWaitingTime(long waitingTime) {
		this.waitingTime = waitingTime;
	}

	@Override
	public Object getDefaultStateKey() {
		return BattleCharacterStates.STILL;
	}

	@Override
	public Map<Object, State<? extends GameCharacter>> buildStates(StateMachine<GameCharacter> stateMachine) {
		this.createState(BattleCharacterStates.STILL, new TimedAnimatedState<GameCharacter>(stateMachine
				, BattleCharacterStates.ATTACKING, waitingTime){
			@Override
			public boolean onStart(GameEvent startEvent) {
				if(this.getAnimation() != null){
					getStateMachine().getEntity().setCurrentAnimation(this.getAnimation());
				}
				return super.onStart(startEvent);
			}
		});
		this.createState(BattleCharacterStates.ATTACKING, new CharacterWeaponAttackingState(stateMachine));
		
		return stateMap;
	}


	protected void createState(Object stateKey, State<GameCharacter> state, Transition ... transitions) {
		state.addTransitions(transitions);
		stateMap.put(stateKey, state);
	}

}
