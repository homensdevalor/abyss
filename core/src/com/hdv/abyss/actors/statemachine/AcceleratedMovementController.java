package com.hdv.abyss.actors.statemachine;

import com.hdv.abyss.actors.MovementController;
import com.hdv.abyss.physics.PhysicsActor;

public class AcceleratedMovementController extends MovementController{
		float currentVelocity;
		boolean accelerating;
		private float startVelocity, maxVelocity;
		private float movementDuration;
		private float decelerationRelativePoint;
		private float acceleration;

		public AcceleratedMovementController() {
			this(1f, 0, 1);
		}
		public AcceleratedMovementController(float moveDistance, float startVelocity, float maxVelocity) {
			super();
			super.setDistanceToMove(moveDistance);
			this.startVelocity = startVelocity;
			this.maxVelocity = maxVelocity;
			this.decelerationRelativePoint = 0.5f;
		}
		
		public float getMaxVelocity() {
			return maxVelocity;
		}
		public void setMaxVelocity(float maxVelocity) {
			this.maxVelocity = maxVelocity;
		}
		public float getStartVelocity() {
			return startVelocity;
		}
		public void setStartVelocity(float startVelocity) {
			this.startVelocity = startVelocity;
		}
		public float getAccelerationRelativeDistance() {
			return decelerationRelativePoint;
		}
		public void setDecelerationRelativePoint(float decelerationRelativePoint) {
			if(decelerationRelativePoint > 1f || decelerationRelativePoint < 0){
				String errMessage = "Argument decelerationRelavitePoint should be between 0f and 1f."
						+ " But value received was:" + decelerationRelativePoint;
				throw new IllegalArgumentException(errMessage);
			}
			this.decelerationRelativePoint = decelerationRelativePoint;
		}

		@Override
		public float getMaxMovementDuration() {
			return movementDuration*1.1f;
		}
		public float getExpectedMovementDuration(){
			return movementDuration;
		}
		public float getCurrentVelocity() {
			return currentVelocity;
		}

		@Override
		public void start() {			
			accelerating = true;
			acceleration = calculateAcceleration(accelerating);
			this.movementDuration = calculateMoveDuration();
			currentVelocity = startVelocity;
			
			super.start();
		}
		
		@Override
		public void update(float deltaTime, PhysicsActor character) {
			super.update(deltaTime, character);
			if(isTimeToDecelerate()){
				decelerate();
			}
			currentVelocity = calculateVelocity(deltaTime);
		}

		protected float calculateMoveDuration() {
			float accelerationTime = calculateAccelerationTime();
			float decelerationTime = calculateDecelerationTime();
			return accelerationTime + decelerationTime;
		}
		protected float calculateAccelerationTime() {
			float accelerationDistance = getDistanceToMove()*getAccelerationRelativeDistance();
			return calculateTimeFromVelocityAndDistanceToMove(
					this.getStartVelocity(), this.getMaxVelocity(),accelerationDistance);
		}
		protected float calculateDecelerationTime() {
			float decelerationDistance = getDistanceToMove()*(1f- getAccelerationRelativeDistance());
			return calculateTimeFromVelocityAndDistanceToMove(
					this.getMaxVelocity(), this.getStartVelocity(),decelerationDistance) ;
		}
		protected float calculateTimeFromVelocityAndDistanceToMove(
				float initialVelocity, float finalVelocity, float distanceToMove) {
			/** A partir das equações da aceleração uniforme (https://en.wikipedia.org/wiki/Acceleration#Uniform_acceleration)
			 *  (I)  v = v0 + a*t
			 *  (II) s = v0*t + (a*t*t/2)
			 *  Substituindo "a*t" em (II) a partir de (I): 
			 *     s = t*(v0+v)/2
			 *  Isolando t:
			 *     t = 2*s/ (v0 + v)
			 *  */
			return (2f*distanceToMove) /(finalVelocity + initialVelocity);
		}
		protected float calculateAcceleration(boolean accelerating) {
			float accelerationTime;
			float deltaVel;
			//Calcula aceleração em função da distância para o 
			if(accelerating){
				accelerationTime = calculateAccelerationTime();
				deltaVel = (maxVelocity - startVelocity);
			}
			else{//desacelerando
				accelerationTime = calculateDecelerationTime();
				deltaVel = (startVelocity - maxVelocity);
			}
			
			return deltaVel / accelerationTime;
		}
		
		protected float calculateVelocity(float deltaTime) {
			float vel =  this.currentVelocity + acceleration * deltaTime;
			
			return vel;
		}
		protected void decelerate() {
			accelerating = false;
			acceleration = calculateAcceleration(accelerating);
		}
		protected boolean isTimeToDecelerate() {
			if(accelerating){
				return getDistanceMoved() > this.getAccelerationRelativeDistance()*getDistanceToMove();
			}
			return false;
		}
		
		
	}