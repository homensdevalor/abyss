package com.hdv.abyss.actors.statemachine.states;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.events.MovementEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class CharacterMovingState extends BaseAnimatedState<GameCharacter> {
	Vector2 currentDirection;
	Movement movementType;
	
	public CharacterMovingState(StateMachine<GameCharacter> machine) {
		super(machine);
	}	
	
	@Override
	public boolean onStart(GameEvent startEvent) { 
		//TODO: mover isto para classe acima
		GameCharacter character = getStateMachine().getEntity();
		character.setCurrentAnimation(this.getAnimation());		
		return true;
	}
	@Override
	public void onStop() {
		//Invalida direção e movimento
		currentDirection = null;
		movementType = null;
	}
	
	@Override
	public void onEvent(GameEvent event) {
		if(event != null && event instanceof MovementEvent){
			MovementEvent moveEvent = (MovementEvent)event;
			currentDirection = moveEvent.getDirection();
			movementType = moveEvent.getMovementType();
		}
	};
	
	@Override
	public void update(float delta) {
		super.update(delta);
		if(currentDirection != null && movementType != null){
			GameCharacter character = getStateMachine().getEntity();
			character.move(currentDirection, movementType);
		}
	}
	
}
