package com.hdv.abyss.actors.statemachine.states;

import com.badlogic.gdx.utils.TimeUtils;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.RestartTimeEvent;
import com.hdv.abyss.statemachine.states.TimedAnimatedState;

public class CharacterBattleStillState extends TimedAnimatedState<GameCharacter>{

	private static final long DEFAULT_WAIT_TIME = 8000; //5 segundos
	private Long startStateTime;
	private long timeToWait;
	private Object previousDefaultStateKey;
	public CharacterBattleStillState(StateMachine<GameCharacter> machine) {
		this(machine, BattleCharacterStates.STILL, DEFAULT_WAIT_TIME);
	}
	public CharacterBattleStillState(StateMachine<GameCharacter> machine, Object nextState, long waitTime) {
		super(machine, nextState, waitTime);
		this.timeToWait = waitTime;
	}

	@Override
	public long getWaitTime() {
		return this.timeToWait;
	}
	@Override
	public void setWaitTime(long timeMillis) {
		this.timeToWait = timeMillis;
	}
	
	@Override
	public boolean onStart(GameEvent startEvent) {
		if(super.onStart(startEvent)){
			if(startStateTime == null || (startEvent != null && startEvent instanceof RestartTimeEvent)){
				startStateTime = TimeUtils.millis();
				super.setWaitTime(this.timeToWait);
				Object currentDefaultStateKey = getStateMachine().getDefaultStateKey();
				Object thisStateKey = getStateMachine().getStateKey(this);
				if(currentDefaultStateKey != thisStateKey){
					previousDefaultStateKey = currentDefaultStateKey;
					this.setNextStateKey(previousDefaultStateKey);
					getStateMachine().setDefaultState(thisStateKey);
				}
			}
			else{
				long timeLeft = (startStateTime + timeToWait) - TimeUtils.millis();
				super.setWaitTime(Math.max(timeLeft,0));
			}
			DirectionalAnimation battleStillAnim = getStateMachine().getEntity().getWeapon().getStillAttackAnimation();
			if(battleStillAnim == null){
				finishState();
				return false;
			}
			super.setAnimation(battleStillAnim);
			getStateMachine().getEntity().setCurrentAnimation(battleStillAnim);
			return true;
		}
		return false;
	}
	
	@Override
	protected void onTimeOver() {
		finishState();
	}
	private void finishState() {
		if(previousDefaultStateKey != null){
			getStateMachine().setDefaultState(previousDefaultStateKey);
		}
		previousDefaultStateKey = null;
		startStateTime = null;
	}

}
