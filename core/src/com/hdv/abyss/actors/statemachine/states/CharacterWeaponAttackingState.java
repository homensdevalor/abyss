package com.hdv.abyss.actors.statemachine.states;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.actors.weapon.Weapon;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.RestartTimeEvent;
import com.hdv.abyss.statemachine.states.AnimatedState;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class CharacterWeaponAttackingState extends BaseAnimatedState<GameCharacter> {
	GameCharacter character;
	
	public CharacterWeaponAttackingState(StateMachine<GameCharacter> machine) {
		super(machine);
		character = machine.getEntity();
	}
	
	@Override
	public void setAnimation(DirectionalAnimation animation) {
		super.setAnimation(animation);
		//FIXME: refatorar isto (fazer pull up, mantendo variável PlayMode no AnimatedState)
		if(animation != null){
			animation.setPlayMode(PlayMode.NORMAL);
		}
	}
	
	@Override
	public boolean onStart(GameEvent startEvent) {		
		if(character.getWeapon() == null){ //Não inicia estado se personagem não possui uma arma
			return false;
		}
		DirectionalAnimation attackAnim = character.getWeapon().getAttackAnimation();
		if(attackAnim == null){
			attackAnim = this.getAnimation();
		}
		if(attackAnim != null){
			character.setCurrentAnimation(attackAnim);
			attackAnim.setAnimationTime(0);
		}
		
		character.stop();		
		character.getWeapon().startAttack(character);
		
		return true;
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		Weapon weapon = character.getWeapon();
		weapon.update(character, delta);
		
		if(weapon.isFinished()){
			AnimatedState<GameCharacter> battleStillState = (AnimatedState<GameCharacter>) getStateMachine().getState(BattleCharacterStates.BATTLE_STILL);
			if(battleStillState != null){
				getStateMachine().changeToState(BattleCharacterStates.BATTLE_STILL, new RestartTimeEvent());
			}
			else{
				getStateMachine().changeToDefaultState();
			}
		}
	}
	@Override
	public void onStop() {
		super.onStop();
		character.getWeapon().onFinishAttack(character);
	}
}
