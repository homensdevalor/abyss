package com.hdv.abyss.actors.statemachine.states;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class CharacterStillState extends BaseAnimatedState<GameCharacter>{
	public CharacterStillState(StateMachine<GameCharacter> machine) {
		super(machine);
	}

	@Override
	public boolean onStart(GameEvent startEvent) {
		GameCharacter character = getStateMachine().getEntity();
		character.stop();
		character.setCurrentAnimation(this.getAnimation());
		
		return true;
	}
}
