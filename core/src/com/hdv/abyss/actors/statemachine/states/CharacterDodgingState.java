package com.hdv.abyss.actors.statemachine.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.AcceleratedMovementController;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.MovementEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class CharacterDodgingState extends BaseAnimatedState<GameCharacter> {
	//TODO: susbtituir constantes por atributos e permitir parametrizá-las!
	private static final float DEFAULT_DODGING_DISTANCE = 3f;
//	private static final float DEFAULT_START_DODGE_VELOCITY = 9f;
//	private static final float DEFAULT_DODGE_MAX_VELOCITY = 9.5f;
	
	private GameCharacter character;
	private Vector2 dodgeDirection;
	private final AcceleratedMovementController dodgeMovement;
	
	public CharacterDodgingState(StateMachine<GameCharacter> machine) {
		this(machine, DEFAULT_DODGING_DISTANCE);
	}

	//TODO: rever estratégia de definição da duração do estado
	public CharacterDodgingState(StateMachine<GameCharacter> machine, float dodgeDistance) {
		super(machine);
		character = machine.getEntity();
		dodgeDirection = new Vector2();
		
		dodgeMovement = new AcceleratedMovementController();
		dodgeMovement.setDistanceToMove(dodgeDistance);
		dodgeMovement.setDecelerationRelativePoint(0.5f);//inicia desaceleração na metade do movimento
	}

	public void setDodgeDistance(float distance){
		this.dodgeMovement.setDistanceToMove(distance);
	}
	public float getDodgeDistance(){
		return this.dodgeMovement.getDistanceToMove();
	}
	
	@Override
	public void setAnimation(DirectionalAnimation animation) {
		super.setAnimation(animation);
		if(animation != null){
			animation.setPlayMode(PlayMode.NORMAL);
		}
	}
	
	Vector2 startPos;//Debug
	
	@Override
	public boolean onStart(GameEvent startEvent) {		
		character.setCurrentAnimation(this.getAnimation());
		character.stop();

		dodgeMovement.setStartVelocity(character.getNormalVelocity());
		dodgeMovement.setMaxVelocity(character.getNormalVelocity()*1.3f);
		
		startPos = character.getPositionInWorld().cpy();
		dodgeDirection = getDodgeDirection(startEvent);
		startDodge(dodgeDirection);
		
		if(getAnimation() != null){
			getAnimation().setAnimationTime(0);
			getAnimation().setAnimationDuration(dodgeMovement.getMaxMovementDuration());
		}
		
		return true;
	}

	private Vector2 getDodgeDirection(GameEvent startEvent) {
		Vector2 dodgeDirection = null;
		if(startEvent != null && startEvent instanceof MovementEvent){
			dodgeDirection = ((MovementEvent)startEvent).getDirection();
		}
		
		if(dodgeDirection == null || dodgeDirection.len2() == 0){
			dodgeDirection = character.getDirection().toVector();
		}
		
		return dodgeDirection.nor();
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		
		dodgeMovement.update(delta, character);
		float vel	= dodgeMovement.getCurrentVelocity();
		character.getBody().setLinearVelocity(dodgeDirection.cpy().scl(vel));
		
		if(dodgeMovement.hasFinished()){
			getStateMachine().changeToDefaultState();
		}
	}

	private void startDodge(Vector2 dodgeDirection) {
		dodgeMovement.resetMovement(startPos);
		dodgeMovement.start();
		character.stop();
		character.setDirection(Direction.roundFromVector(dodgeDirection));
	}

	@Override
	public void onStop() {
		super.onStop();
		character.stop();
		
		Vector2 finalPos = character.getPositionInWorld().cpy();
		Vector2 deltaPos = finalPos.cpy().sub(startPos);
		
		Gdx.app.log(getClass().getName(), "End Dodge! Moved " + deltaPos + " meters");
	}
}
