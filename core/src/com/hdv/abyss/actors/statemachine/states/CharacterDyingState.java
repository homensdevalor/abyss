package com.hdv.abyss.actors.statemachine.states;

import com.badlogic.gdx.Gdx;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class CharacterDyingState extends BaseAnimatedState<GameCharacter> {

	float timeCounter;
	boolean stateEnded;
	public CharacterDyingState(StateMachine<GameCharacter> machine) {
		super(machine);
		stateEnded = false;
	}

	@Override
	public boolean onStart(GameEvent startEvent) {
		timeCounter = 0;
		
		Gdx.app.log(getClass().getName(), "Started dye event!");
		
//		super.getAnimation().setAnimationTime(super.getAnimation().getAnimationDuration());
		final GameCharacter character = getStateMachine().getEntity();
		if(this.getAnimation() != null){
			character.setCurrentAnimation(this.getAnimation());
		}
		character.getPhysicalWorld().afterStep(new Runnable(){
			final GameCharacter characterToInativate = character;
			@Override
			public void run() {
				Gdx.app.log(CharacterDyingState.class.getName(), "Call inative body!");
				characterToInativate.getBody().setActive(false);
			}
		});
		return true;
	}

//	private boolean animationFinished(float nextTime) {
//		return nextTime >= this.getAnimation().getAnimationDuration() &&
//				timeCounter < this.getAnimation().getAnimationDuration();
//	}
}
