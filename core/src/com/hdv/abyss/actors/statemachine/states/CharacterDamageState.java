package com.hdv.abyss.actors.statemachine.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.weapon.Attack;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class CharacterDamageState extends BaseAnimatedState<GameCharacter>{
	private static final float DEFAULT_DAMAGE_DURATION = 0.8f;
	private static final float KNOCKBACK_FORCE = 10f;
	float stateDurationInSeconds;
	float timeCounter;

	GameCharacter character;

	//TODO: rever estratégia de definição da duração do estado
	public CharacterDamageState(StateMachine<GameCharacter> machine) {
		this(machine, DEFAULT_DAMAGE_DURATION);
	}

	//TODO: rever estratégia de definição da duração do estado
	public CharacterDamageState(StateMachine<GameCharacter> machine, float duration) {
		super(machine);
		stateDurationInSeconds = duration;
		character = machine.getEntity();
	}
	
	@Override
	public void setAnimation(DirectionalAnimation animation) {
		super.setAnimation(animation);
		if(animation != null){
			animation.setPlayMode(PlayMode.NORMAL);
			animation.setAnimationDuration(this.stateDurationInSeconds);
		}
	}
	
	@Override
	public boolean onStart(GameEvent startEvent) {	
		character.setCurrentAnimation(this.getAnimation());
		character.stop();
		timeCounter = 0;
		if(getAnimation() != null){
			getAnimation().setAnimationTime(0);
		}
		
		if(startEvent != null && startEvent instanceof CollisionEvent){
			Vector2 attackDirection = getAttackedFrom((CollisionEvent)startEvent);
			
			Gdx.app.log(getClass().getName(), "Character attacked from: " + attackDirection);
			
			if(attackDirection != null){
				applyKnockback(attackDirection);
				Direction dirToLook = Direction.getInverse(Direction.roundFromVector(attackDirection));
				character.setDirection(dirToLook);
			}
			
		}
		
		return true;
	}
	
	private void applyKnockback(Vector2 attackDirection) {
		Gdx.app.log(getClass().getName(), "Apply knockback in direction " + attackDirection + " with force: " + KNOCKBACK_FORCE);
		character.getBody().applyLinearImpulse(attackDirection.cpy().scl(KNOCKBACK_FORCE), character.getBody().getWorldCenter() , true);
	}

	private Vector2 getAttackedFrom(CollisionEvent startEvent) {
		Vector2 attackDirection = null;
		
		Object attackObj = startEvent.getOther(this.character);
		Gdx.app.log(getClass().getName(),"Attacked by: " + attackObj.getClass().getName());
		if(attackObj instanceof Attack){
			Attack attack = ((Attack) attackObj);
			attackDirection = attack.getDirection().toVector();
		}
		return attackDirection;
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		
		//FIXME: encapsular comportamento de mudança de estado baseada em tempo em uma classe de Transição
		timeCounter += delta;
		if(timeCounter >= stateDurationInSeconds){
			getStateMachine().changeToDefaultState();
		}
	}
}
