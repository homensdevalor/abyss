package com.hdv.abyss.actors.statemachine.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.MovementEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class OldCharacterDodgingState extends BaseAnimatedState<GameCharacter> {
	//TODO: susbtituir constantes por atributos e permitir parametrizá-las!
	private static final float DEFAULT_DODGING_DURATION = 0.4f; //tempo em segundos
	private static final float DEFAULT_DELTA_DODGE_VELOCITY = 0.75f;
	private static final float START_DODGE_VELOCITY = 5f;
	private float stateDurationInSeconds;
	private float timeCounter;
	private GameCharacter character;
	private Vector2 dodgeDirection;
	private float dodgeVelocity;
	private float dodgeDeltaVelocity;
	
	public OldCharacterDodgingState(StateMachine<GameCharacter> machine) {
		this(machine, DEFAULT_DODGING_DURATION);
	}

	//TODO: rever estratégia de definição da duração do estado
	public OldCharacterDodgingState(StateMachine<GameCharacter> machine, float duration) {
		super(machine);
		stateDurationInSeconds = duration;
		character = machine.getEntity();
		dodgeDirection = new Vector2();
		dodgeVelocity = 0;
	}
	
	@Override
	public void setAnimation(DirectionalAnimation animation) {
		super.setAnimation(animation);
		if(animation != null){
			animation.setPlayMode(PlayMode.NORMAL);
			animation.setAnimationDuration(this.stateDurationInSeconds);
		}
	}
	
	Vector2 startPos;//Debug
	
	@Override
	public boolean onStart(GameEvent startEvent) {		
		character.setCurrentAnimation(this.getAnimation());
		character.stop();
		timeCounter = 0;
		if(getAnimation() != null){
			getAnimation().setAnimationTime(0);
		}

		startPos = character.getPositionInWorld().cpy();
		
		dodgeDirection = getDodgeDirection(startEvent);
		startDodge(dodgeDirection);
		
		return true;
	}

	private Vector2 getDodgeDirection(GameEvent startEvent) {
		Vector2 dodgeDirection = null;
		if(startEvent != null && startEvent instanceof MovementEvent){
			dodgeDirection = ((MovementEvent)startEvent).getDirection();
		}
		
		if(dodgeDirection == null || dodgeDirection.len2() == 0){
			dodgeDirection = character.getDirection().toVector();
		}
		
		return dodgeDirection.nor();
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		if(itsTimeToDecelerate(delta))
		{
			startDeceleration();
		}
		updateVelocity();

		//FIXME: encapsular comportamento de mudança de estado baseada em tempo em uma classe de Transição
		timeCounter += delta;		
		if(timeCounter >= stateDurationInSeconds){
			getStateMachine().changeToDefaultState();
		}
	}


	private void startDodge(Vector2 dodgeDirection) {
		character.setDirection(Direction.roundFromVector(dodgeDirection));
		character.getBody().setLinearVelocity(0, 0);
		dodgeDeltaVelocity = DEFAULT_DELTA_DODGE_VELOCITY;
		dodgeVelocity = START_DODGE_VELOCITY;
	}
	
	private void updateVelocity() {
		dodgeVelocity += dodgeDeltaVelocity;
		character.getBody().setLinearVelocity(dodgeDirection.cpy().scl(dodgeVelocity));
	}

	private boolean itsTimeToDecelerate(float delta) {
		//Começa desacelerar a partir da segunda metade do movimento
		return timeCounter < stateDurationInSeconds/2f 
				&& timeCounter + delta >= stateDurationInSeconds/2f;
	}
	private void startDeceleration() {
		//Inverte "aceleração"
		dodgeDeltaVelocity = -dodgeDeltaVelocity;
	}

	@Override
	public void onStop() {
		super.onStop();
//		character.getBody().setLinearVelocity(0, 0);
		character.stop();
		
		Vector2 finalPos = character.getPositionInWorld().cpy();
		Vector2 deltaPos = finalPos.cpy().sub(startPos);
		
		Gdx.app.log(getClass().getName(), "End Dodge! Moved " + deltaPos + " meters");
	}
}
