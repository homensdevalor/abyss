package com.hdv.abyss.actors.statemachine;

public enum BattleCharacterStates{
	STILL, MOVING, ATTACKING, DAMAGED, DODGING, DEAD, BATTLE_STILL;
}