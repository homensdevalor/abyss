package com.hdv.abyss.actors.lever.states;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.global.events.type.LeverGlobalEvents;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.StateMapBuilder;
import com.hdv.abyss.statemachine.states.State;
import com.hdv.abyss.statemachine.transitions.OnGlobalTransition;
import com.hdv.abyss.statemachine.transitions.OnReceivedAttackTransition;
import com.hdv.abyss.statemachine.transitions.Transition;

public class LeverStateMapBuilder implements StateMapBuilder<GameCharacter>{

	final Map<Object, State<? extends GameCharacter> > stateMap;
	
	public LeverStateMapBuilder() {
		stateMap = new HashMap<Object, State<? extends GameCharacter>>();
	}
	
	@Override
	public Object getDefaultStateKey() {
		return LeverStates.STILL;
	}

	@Override
	public Map<Object, State<? extends GameCharacter>> buildStates(
			StateMachine<GameCharacter> stateMachine) {
		
		Transition receivedAttackTransitionAndIsPressed = new OnReceivedAttackTransition(stateMachine.getEntity(), LeverStates.STILL);
		Transition receivedAttackTransitionAndIsStill = new OnReceivedAttackTransition(stateMachine.getEntity(), LeverStates.PRESSED);

		Transition receivedGlobalAndIsPressed = new OnGlobalTransition(LeverStates.STILL, LeverGlobalEvents.LEVER_RELEASED);
		Transition receivedGlobalAndIsReleased = new OnGlobalTransition(LeverStates.PRESSED, LeverGlobalEvents.LEVER_PRESSED);
		
		this.createState(LeverStates.PRESSED, new PressedLeverState(stateMachine), 
				receivedAttackTransitionAndIsPressed, receivedGlobalAndIsPressed);
		this.createState(LeverStates.STILL, new ReleasedLeverState(stateMachine), 
				receivedAttackTransitionAndIsStill, receivedGlobalAndIsReleased);
		
		return stateMap;
		
	}

	protected void createState(Object stateKey, State<GameCharacter> state, Transition ... transitions) {
		state.addTransitions(transitions);
		stateMap.put(stateKey, state);
	}
	
}
