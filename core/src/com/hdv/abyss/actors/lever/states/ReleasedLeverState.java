package com.hdv.abyss.actors.lever.states;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.entities.LeverActor;
import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.global.GlobalEventCollector;
import com.hdv.abyss.global.events.type.LeverGlobalEvents;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.states.BaseAnimatedState;

public class ReleasedLeverState extends BaseAnimatedState<GameCharacter>{

	public ReleasedLeverState(StateMachine<GameCharacter> machine) {
		super(machine);
	}

	@Override
	public boolean onStart(GameEvent startEvent) {
		LeverActor lActor = (LeverActor) this.getStateMachine().getEntity();
		lActor.setCurrentAnimation(this.getAnimation());
		if (!(startEvent instanceof GlobalEvent)){
			GlobalEventCollector.getInstance().fireEvent(new GlobalEvent(lActor.getTargetTrigger(), LeverGlobalEvents.LEVER_RELEASED));
		}
		return true;
	}
	
}
