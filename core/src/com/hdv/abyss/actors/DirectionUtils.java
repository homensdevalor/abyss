package com.hdv.abyss.actors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.utils.Vectors;

public class DirectionUtils {
	/** diferença mínima entre o eixo x e y de um vetor para considerá-los iguais.
	 * Este valor é utilizado durante a atualização de direção de um GameCharacter.
	 * Um valor muito pequeno pode gerar variações muito bruscas de direção (em personagens 
	 * automatizados) e um valor muito grande pode gerar direções falsas.*/
	private static final float DIR_EPSILON = 0.11f;
	
	public static Direction updateDirection(Vector2 moveDirection, Direction playerDirection, Vector2 previousMoveDirection) {
		Direction newDirection = playerDirection;
		if(!moveDirection.isZero()){ //Quando movimento é nulo mantém a direção atual
			if(moveDirection.x ==0 || moveDirection.y == 0){ //Direção ortogonal
				newDirection = Direction.fromVector(moveDirection.cpy().nor());
				assertNotNullDirection(newDirection, moveDirection, playerDirection);
			}
			else if(isZeroOrDiagonal(moveDirection)){//Em diagonal, tenta determinar direção de acordo com anterior
				newDirection = getDirectionFromPrevious(moveDirection, playerDirection, previousMoveDirection);
				assertNotNullDirection(newDirection, moveDirection, playerDirection);
			}
			else{ //Um eixo não ortogonal e não diagonal, arredonda
				newDirection = Direction.roundFromVector(moveDirection);
				assertNotNullDirection(newDirection, moveDirection, playerDirection);
			}
		}
		
		return newDirection;
	}

	public static void assertNotNullDirection(Direction dir, Vector2 moveDir, Direction playerDirection) {
		if(dir == null){
			throw new IllegalStateException("Direction is null when moving in direction: " + moveDir + " and current dir is " + playerDirection);
		}
	}
	
	public static Direction getDirectionFromPrevious(Vector2 moveDirection, Direction playerDirection, Vector2 previousDirection) {
		Vector2 unaryNewVector = new Vector2(Math.round(moveDirection.x), Math.round(moveDirection.y));
//		Vector2 unaryCurrentVector = new Vector2(Math.round(previousDirection.x), Math.round(previousDirection.y));

//		if(!isOpposite(unaryNewVector, unaryCurrentVector)){
		if(!isOpposite(unaryNewVector, playerDirection.toVector())){
			return playerDirection;
		}
		else{//Retorna vetor oposto a direção atual
			return Direction.fromVector(playerDirection.toVector().cpy().scl(-1));
		}
	}
	public static boolean isOpposite(Vector2 unaryNewVector,
			Vector2 unaryCurrentVector) {
		return Vectors.angleBetween(unaryNewVector, unaryCurrentVector) > MathUtils.PI/2f;
	}
	public static boolean isPositive(float value) {
		return value > 0;
	}
	public static boolean isZeroOrDiagonal(Vector2 moveDirection) {
		float delta = Math.abs(moveDirection.x) - Math.abs(moveDirection.y);
		float deltaAbs = Math.abs(delta);
		
		return deltaAbs <= DIR_EPSILON;
	}
}
