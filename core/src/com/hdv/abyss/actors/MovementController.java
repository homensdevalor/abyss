package com.hdv.abyss.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.PhysicsActor;

public class MovementController{
	private final Vector2 startPosition;
	private final Vector2 currentPos;
	private Float timeMoved;
	private float distanceToMove;
	private float maxTimeToMove;
	private float velocity;
	
	public MovementController() {
		startPosition = new Vector2();
		currentPos = new Vector2();
		timeMoved = null;
	}

	public float getDistanceToMove() {
		return distanceToMove;
	}
	public void setDistanceToMove(float distanceToMove) {
		this.distanceToMove = distanceToMove;
	}
	public float getVelocity() {
		return velocity;
	}
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}

	public void resetMovement(Vector2 positionInWorld) {
		timeMoved = null;
		startPosition.set(positionInWorld);
		currentPos.set(positionInWorld);
	}
	public boolean hasStarted() {
		return timeMoved != null;
	}
	public void start() {
		timeMoved = 0f;
		this.maxTimeToMove = getMaxMovementDuration();
	}

	/** @return tempo em segundos para limitar fim do movimento*/
	public float getMaxMovementDuration() {
		return distanceToMove/getVelocity();
	}
	public float getDistanceMoved() {
		return currentPos.dst(startPosition);
	}
	public float getTimeElapsed() {
		return timeMoved;
	}
	public void update(float deltaTime, PhysicsActor character){
		timeMoved += deltaTime;
		this.currentPos.set(character.getPositionInWorld());
	}
	public boolean hasFinished() {	
		boolean timeOver = (timeMoved >= maxTimeToMove);
		boolean distanceOver = (getDistanceMoved() > distanceToMove);
		
		//Debug
		if(timeOver || distanceOver){
			Gdx.app.log(getClass().getName(), "Movement finished! "+ 
						(timeOver ? "Time Over!" :"")
						+ (distanceOver ? "Distance Over!":""));
		}
		
		return timeOver || distanceOver;
	}

	protected Vector2 getStartPosition() {
		return currentPos.cpy();
	}	
}