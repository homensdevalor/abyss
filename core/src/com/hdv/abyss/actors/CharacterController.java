package com.hdv.abyss.actors;

import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.statemachine.events.GameEvent;

public interface CharacterController {
	void update(GameCharacter gameCharacter, float delta);
	void setup(GameCharacter gameCharacter);
	void onEvent(GameCharacter gameCharacter, GameEvent event);
}
