package com.hdv.abyss.actors.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hdv.abyss.physics.PhysicsActor;

public class Collider extends PhysicsActor{
	public Collider() {
		this(Vector2.Zero, new Vector2(1, 1));
	}

	public Collider(Vector2 leftBottomPosition, Vector2 size) {
		super();
		getPhysicalProperties().setBodyType(BodyType.StaticBody);
		//Move corpo para posicionar corretamente
		getPhysicalProperties().setStartPosition(leftBottomPosition.cpy().add(size.cpy().scl(0.5f)));
		getPhysicalProperties().setSize(size);
	}
}
