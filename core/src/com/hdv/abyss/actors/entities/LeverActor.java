package com.hdv.abyss.actors.entities;

import java.util.Map;

import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.statemachine.StateMapBuilder;

public class LeverActor extends GameCharacter{

	private Object targetTrigger;
	
	public LeverActor(Map<? extends Object, ? extends DirectionalAnimation> map, StateMapBuilder<GameCharacter> states) {
		super(map, states);
		// TODO Auto-generated constructor stub
	}
	
	public void setTargetTrigger(Object targetTrigger){
		this.targetTrigger = targetTrigger;
	}
	
	public Object getTargetTrigger(){
		return this.targetTrigger;
	}
	
}
