package com.hdv.abyss.actors.entities;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.physics.collision.CollisionManager.TouchListener;

public class InsideColliderNotifier extends Collider implements TouchListener{
	public interface InsideListener{
		public void onEnter(PhysicsActor container, PhysicsActor insideActor);
		public void onLeave(PhysicsActor container, PhysicsActor insideActor);
	}
	/** Armazena outros atores que estão contidos neste objeto*/
	private final Set<PhysicsActor> insideColliders;
	/** Armazena atores que estão colidindo com este objeto*/
	private final Set<PhysicsActor> touchingColliders;
	/** Número de loops até nova atualização seja realizada*/
	private int updateInterval;
	private int frameCount;
	
	private final Set<InsideListener> insideListeners;
	
	public InsideColliderNotifier() {
		super();
		this.getPhysicalProperties().setIsSensor(true);
		insideColliders = new HashSet<PhysicsActor>();
		touchingColliders = new HashSet<PhysicsActor>();
		insideListeners = new HashSet<InsideColliderNotifier.InsideListener>();
		
		updateInterval = 30; 
		frameCount= MathUtils.random(updateInterval); //inicia valor aleatório para reduzir instâncias atualizarem no mesmo frame
	}

	public void addInsideListener(InsideListener listener){
		this.insideListeners.add(listener);
	}
	public void removeInsideListener(InsideListener listener){
		this.insideListeners.remove(listener);
	}
	
	public int getUpdateInterval() {
		return updateInterval;
	}
	public void setUpdateInterval(int updateInterval) {
		this.updateInterval = updateInterval;
	}

	@Override
	protected void onAttachToWorld() {
		super.onAttachToWorld();
		getPhysicalWorld().getCollisionManager().addTouchListener(this, this);
	}
	@Override
	protected void onPrepareDettachToWorld() {
		super.onPrepareDettachToWorld();
		getPhysicalWorld().getCollisionManager().removeTouchListener(this);
	}
	@Override
	public void act(float delta) {
		super.act(delta);
		if(!insideColliders.isEmpty() || !touchingColliders.isEmpty()){
			++frameCount;
			if(frameCount % updateInterval == 0){
				updateInsideColliders(touchingColliders, insideColliders);
			}
			frameCount = frameCount % updateInterval;
		}
	}
	
	protected void updateInsideColliders(Set<PhysicsActor> touchingColliders, Set<PhysicsActor> insideColliders) {
		Set<PhysicsActor> actorsToLeave = new HashSet<PhysicsActor>();
		Set<PhysicsActor> actorsToEnter = new HashSet<PhysicsActor>();
		
		for(PhysicsActor actor : insideColliders){
			if(!contains(actor)){
				actorsToLeave.add(actor);
			}
		}
		for(PhysicsActor actor : touchingColliders){
			if(contains(actor)){
				actorsToEnter.add(actor);
			}
		}		
		for(PhysicsActor actor: actorsToLeave){
			insideColliders.remove(actor);
			touchingColliders.add(actor);
			notifyActorLeave(actor);
		}
		for(PhysicsActor actor: actorsToEnter){
			insideColliders.add(actor);
			touchingColliders.remove(actor);
			notifyActorEntry(actor);
		}
	}
	
	private void notifyActorEntry(PhysicsActor actor) {
		for(InsideListener insideListener : this.insideListeners){
			insideListener.onEnter(this, actor);
		}
	}
	private void notifyActorLeave(PhysicsActor actor) {
		for(InsideListener insideListener : this.insideListeners){
			insideListener.onLeave(this, actor);
		}
	}
	private boolean contains(PhysicsActor otherActor) {
		Rectangle otherRect = rectangleFromActor(otherActor);
		Rectangle thisRect = rectangleFromActor(this);
		boolean contains =  thisRect.contains(otherRect);
		
		return contains;
	}
	private Rectangle rectangleFromActor(PhysicsActor otherActor) {
		Vector2 centerPos = otherActor.getPositionInWorld();
		Vector2 size = otherActor.getSizeInWorld();
		Vector2 bottomLeft = centerPos.cpy().sub(size.cpy().scl(0.5f));
		Rectangle rect = new Rectangle(bottomLeft.x, bottomLeft.y, size.x, size.y);
		return rect;
	}
	@Override
	public void onBeginTouch(CollisionEvent evt) {
		if(evt.contains(this)){
			Object other = evt.getOther(this);
			if(other instanceof PhysicsActor){
				this.touchingColliders.add((PhysicsActor) other);
			}
		}
	}
	@Override
	public void onEndTouch(CollisionEvent evt) {
		if(evt.contains(this)){
			Object other = evt.getOther(this);
			if(other instanceof PhysicsActor){
				PhysicsActor actor = (PhysicsActor)other;
				this.touchingColliders.remove(actor);
				if(insideColliders.contains(actor)){
					this.insideColliders.remove(actor);
					notifyActorLeave(actor);
				}
			}
		}
	}
}
