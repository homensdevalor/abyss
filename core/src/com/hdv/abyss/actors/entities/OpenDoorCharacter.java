package com.hdv.abyss.actors.entities;

import java.util.Map;

import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.statemachine.StateMapBuilder;

public class OpenDoorCharacter extends GameCharacter{

	public OpenDoorCharacter(
			Map<? extends Object, ? extends DirectionalAnimation> map, StateMapBuilder<GameCharacter> stateMap) {
		super(map, stateMap);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onAttachToWorld() {
		super.onAttachToWorld();
		this.getPhysicalWorld().afterStep(new Runnable(){
			final GameCharacter characterToInativate = OpenDoorCharacter.this;
			@Override
			public void run() {
				characterToInativate.getBody().setActive(false);
			}
		});
	}
	
}
