package com.hdv.abyss.actors.entities;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hdv.abyss.actors.AnimatedEntity;
import com.hdv.abyss.actors.CharacterController;
import com.hdv.abyss.actors.DirectionUtils;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.actors.statemachine.statebuilders.BattleStateMapBuilder;
import com.hdv.abyss.actors.weapon.Attack;
import com.hdv.abyss.actors.weapon.Weapon;
import com.hdv.abyss.actors.weapon.WeaponQuickSlot;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.global.GlobalEvent;
import com.hdv.abyss.global.GlobalListener;
import com.hdv.abyss.health.HealthController;
import com.hdv.abyss.health.HealthValue;
import com.hdv.abyss.health.HealthValueListener;
import com.hdv.abyss.health.event.HealthValueEvent;
import com.hdv.abyss.health.event.HitEvent;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.physics.collision.CollisionCategory;
import com.hdv.abyss.physics.collision.CollisionEvent;
import com.hdv.abyss.physics.collision.CollisionManager.CollisionListener;
import com.hdv.abyss.statemachine.BaseStateMachine;
import com.hdv.abyss.statemachine.OnStateChangeListener;
import com.hdv.abyss.statemachine.StateMachine;
import com.hdv.abyss.statemachine.StateMapBuilder;
import com.hdv.abyss.statemachine.events.ChangeItemCommandEvent;
import com.hdv.abyss.statemachine.events.GameEvent;
import com.hdv.abyss.statemachine.events.Movement;
import com.hdv.abyss.statemachine.states.AnimatedState;
import com.hdv.abyss.statemachine.states.State;

public class GameCharacter extends PhysicsActor 
							implements CollisionListener, HealthValueListener, GlobalListener,AnimatedEntity{

	private final Map<Object, DirectionalAnimation> statesToAnimation;

	private final Vector2 moveDirection;
	private float fastVelocity;
	private float normalVelocity;
	
	private final Sprite currentSprite;
	/** Direção para a qual o sprite do personagem está apontando*/
	private Direction playerDirection;

	private DirectionalAnimation animation;
	/** Distância relativa ao centro do ator no qual estará posicionado o centro do sprite.*/
	private final Vector2 currentSpriteOffset;
	private final Vector2 defaultSpriteOffset;
	
	private final HealthController healthController;
	
	private WeaponQuickSlot weaponQuickSlot;
	
	private StateMachine<GameCharacter> stateMachine;
	
	private CharacterController characterController;

	private boolean active;

	/* ******************************************* Constructors ******************************************************/
	public GameCharacter(Map<? extends Object, ? extends DirectionalAnimation> map) 
	{
		this(map, new BattleStateMapBuilder());//TODO: rever dependência de GameCharacter com BattleStateMapBuilder
	}
	public GameCharacter(Map<? extends Object, ? extends DirectionalAnimation> map, StateMapBuilder<GameCharacter> mapBuilder) 
	{
		this.active = true;
		this.weaponQuickSlot = new WeaponQuickSlot();
		currentSprite = new Sprite();
		
		normalVelocity = 1.5f;
		fastVelocity   = 2*normalVelocity;

		playerDirection = Direction.Down;
		moveDirection = playerDirection.toVector();
		
		healthController = new HealthController(5);
		healthController.addValueListener(this);
		
		stateMachine = new BaseStateMachine<GameCharacter>(this);

		this.getPhysicalProperties().setBodyType(BodyType.DynamicBody);
		//Isto permite que após uma colisão com um corpo dinâmico, o personagem reduza a velocidade rapidamente
		this.getPhysicalProperties().setLinearDamping(30);
		
		if(mapBuilder != null){
			stateMachine.setStates(mapBuilder.buildStates(stateMachine), mapBuilder.getDefaultStateKey());
		}
		
		currentSpriteOffset = new Vector2(0,0);
		defaultSpriteOffset = new Vector2(0,0);
		
		statesToAnimation = new HashMap<Object, DirectionalAnimation>();
		setAnimationsImplementation(map);
		stateMachine.changeToDefaultState();
	}
	
	/* ******************************************* Lifecycle ******************************************************/
	@Override
	protected void onAttachToWorld() {
		super.onAttachToWorld();
		if(this.animation != null){
			adjustSpriteSize(this.currentSprite, animation.getKeyFrame(this.playerDirection));
		}
		registerCollisionListener();
		startCharacterController();
	}
	@Override
	protected void onPrepareDettachToWorld() {
		super.onPrepareDettachToWorld();
		unregisterCollisionListener();
	}
	@Override
	public void act(float delta) {
		if(getPhysicalProperties().getCollisionCategory() == CollisionCategory.Player){
		
		}

		super.act(delta);
		
		if(this.isActive()){
			updateCharacterController(delta);
			stateMachine.update(delta);
		}
		updateAnimation(delta);
	}
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if(currentSprite.getTexture() != null && isAttachedToWorld()){
			Vector2 pos = getPositionInWorld();
			Vector2 offset = getCurrentSpriteOffset();
			currentSprite.setCenter(pos.x + offset.x, pos.y + offset.y);
			currentSprite.draw(batch,parentAlpha);
		}
	}
	@Override
	public void setColor(Color color) {
		super.setColor(color);
		currentSprite.setColor(color);
	}
	/* ******************************************* Animations ******************************************************/
	public Sprite getSprite() {
		return currentSprite;
	}
	protected Vector2 getCurrentSpriteOffset() {
		return this.currentSpriteOffset;
	}
	public Vector2 getDefaultSpriteOffset() {
		return defaultSpriteOffset;
	}
	public void setDefaultSpriteOffset(Vector2 defaultSpriteOffset) {
		this.defaultSpriteOffset.set(defaultSpriteOffset);
		updateAnimationOffset();
	}
	public DirectionalAnimation getCurrentAnimation(){
		return this.animation;
	}
	public void setAnimations(AnimationMapBuilder animMapBuilder){
		this.setAnimations(animMapBuilder.buildMap());
	}
	public void setAnimations( Map<? extends Object, ? extends DirectionalAnimation> animMap) {
		this.setAnimationsImplementation(animMap);
	}
	/** Este método é final para poder ser utilizado no construtor*/
	private final void setAnimationsImplementation(
			Map<? extends Object, ? extends DirectionalAnimation> map) {
		if(map != null){
			statesToAnimation.putAll(map);
		}
		setupStatesAnimation();
	}
	private void setupStatesAnimation() {
		for (Map.Entry<Object, DirectionalAnimation> entry : statesToAnimation.entrySet()) {
		    State<GameCharacter> state = stateMachine.getState(entry.getKey());
		    if(state != null && state instanceof AnimatedState){
		    	((AnimatedState<GameCharacter>)state).setAnimation(entry.getValue());
		    }
		}
	}
	@Override
	public void setCurrentAnimation(DirectionalAnimation animation) {
		if(animation == null){
			return;
		}
		this.animation = animation;
		
		updateAnimationOffset();
		
		if(isAttachedToWorld()){
			adjustSpriteSize();
		}
		
		updateAnimation(0);
	}
	protected void updateAnimationOffset() {
		DirectionalAnimation animation = getCurrentAnimation();
		if(animation != null && animation.getOffset() != null){
			this.currentSpriteOffset.set(animation.getOffset());
		}
		else{
			this.currentSpriteOffset.set(getDefaultSpriteOffset());
		}
	}
	private void updateAnimation(float delta) {	
		if(animation != null){
			animation.update(delta);
			TextureRegion frame = animation.getKeyFrame(playerDirection);
			currentSprite.setRegion(frame);
		}
	}
	private void adjustSpriteSize() {
		TextureRegion region = animation.getKeyFrame(playerDirection);
		this.adjustSpriteSize(this.currentSprite, region);
	}
	private void adjustSpriteSize(Sprite sprite, TextureRegion region) {
		Vector2 textureSize = new Vector2(region.getRegionWidth(), region.getRegionHeight());
		Vector2 size = getPhysicalWorld().convertToMeters(textureSize.cpy());
		
		sprite.setSize(size.x, size.y);
	}
	/* ******************************************* Movement ******************************************************/
	public Vector2 getMoveDirection() {
		return moveDirection.cpy();
	}
	public void move(Vector2 direction, Movement movementType) {
		this.movement(direction, (movementType == Movement.FAST ? fastVelocity : normalVelocity));
	}
	public void stop() {
		this.movement(Vector2.Zero, 0);
	}
	
	protected void movement(Vector2 direction, float velocityMagnitude) {
		lookAt(direction);
		Vector2 velocity = new Vector2(0,0);
		
		if(!direction.isZero()){
			this.moveDirection.set(direction);
			velocity.set(direction).nor().scl(velocityMagnitude);
		}
		if(isAttachedToWorld()){
			this.getBody().setLinearVelocity(velocity);
		}
	}
	public void lookAt(Vector2 direction){
		Direction newDir = DirectionUtils.updateDirection(direction, playerDirection, this.moveDirection);
		DirectionUtils.assertNotNullDirection(newDir, moveDirection, playerDirection);
		playerDirection = newDir;
	}
	public void lookAtPoint(Vector2 position){
		Vector2 dir = position.cpy().sub(getPositionInWorld());
		lookAt(dir.nor());
	}
	public void lookAtArea(Vector2 centerPos, Vector2 size){
		Vector2 myPos = getPositionInWorld(); 
		Vector2 mySize = getPositionInWorld();
		
		
		Rectangle otherArea = new Rectangle(
				  centerPos.x -size.x/2f
				, centerPos.y - size.y/2f
				, size.x
				, size.y);

		float myTop = myPos.y + mySize.y/2f;
		float myDown = myPos.y - mySize.y/2f;
		float myLeft = myPos.x - mySize.x/2f;
		float myRight = myPos.x + mySize.x/2f;
		
		
		if(otherArea.contains(myLeft, centerPos.y) 
				|| otherArea.contains(myRight, centerPos.y))
		{ //Acima ou abaixo
			playerDirection = myPos.y < centerPos.y ? Direction.Up : Direction.Down;
		}
		else if(otherArea.contains(centerPos.x, myTop) 
				|| otherArea.contains(centerPos.x, myDown))
		{//Esquerda ou direita
			playerDirection = myPos.x < centerPos.x ? Direction.Right : Direction.Left;
		}
		else{ //Diagonais
			lookAt(centerPos);
		}
	}

	public float getFastVelocity() {
		return fastVelocity;
	}
	public void setFastVelocity(float fastVelocity) {
		this.fastVelocity = fastVelocity;
	}
	public float getNormalVelocity() {
		return normalVelocity;
	}
	public void setNormalVelocity(float normalVelocity) {
		this.normalVelocity = normalVelocity;
	}
	@Override
	public Direction getDirection() {
		return playerDirection;
	}
	public void setDirection(Direction direction) {
		this.playerDirection = direction;
	}

	/* ******************************************* States ******************************************************/
	public StateMachine<? extends GameCharacter> getStateMachine() {
		return this.stateMachine;
	}
	public void fireEvent(GameEvent event) {
		if(!this.isActive()){
			return;
		}
		
		stateMachine.fireEvent(event);
		if(event instanceof ChangeItemCommandEvent){
			this.getWeaponQuickSlot().nextWeapon();
		}
		else if(event instanceof CollisionEvent){
			CollisionEvent collisionEvent = (CollisionEvent) event;
			if(collisionEvent.containsInstanceOf(Attack.class) && collisionEvent.contains(this)){
				Attack attack = (Attack) collisionEvent.getColliderOfType(Attack.class); 
				if( attack.getAttackCreator() != this){
					HitEvent hit = new HitEvent(attack.getHitValue());
					this.healthController.onHit(hit);	
				}
			}
		}
		notifyEventToController(event);
	}
	public void addOnStateChangeListener(OnStateChangeListener stateChangeListener) {
		this.stateMachine.addOnStateChangeListener(stateChangeListener);
	}
	public void removeOnStateChangeListener(OnStateChangeListener stateChangeListener) {
		this.stateMachine.removeOnStateChangeListener(stateChangeListener);
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void die(){
		stateMachine.changeToState(BattleCharacterStates.DEAD);
		this.setActive(false);
	}

	/* ******************************************* Collision ******************************************************/
	@Override
	public void onCollision(CollisionEvent evt) {
		if(evt.contains(this)){
			fireEvent(evt);
		}
	}
	protected void registerCollisionListener() {
		getPhysicalWorld().getCollisionManager().addCollisionListener((CollisionListener)this , (PhysicsActor)this);
	}
	protected void unregisterCollisionListener() {
		getPhysicalWorld().getCollisionManager().removeCollisionListener((CollisionListener)this, (PhysicsActor)this);
	}
	
	/* ********************************************** Health ***************************************************/
	public HealthController getHealthController() {
		return healthController;
	}
	public HealthValue getHealthValue(){
		return healthController.getHealthValue();
	}
	
	@Override
	public void onHealthValueEvent(HealthValueEvent event) {
		if (event.getActualValue() <= 0){
			this.die();
		}
		
	}	
	
	/* ******************************************* Weapon ******************************************************/
	public Weapon getWeapon() {
		return weaponQuickSlot.getCurrentWeapon();
	}
	public void setWeapon(Weapon weapon) {
		this.weaponQuickSlot.setCurrentWeapon(weapon);
	}
	public WeaponQuickSlot getWeaponQuickSlot(){
		return this.weaponQuickSlot;
	}
	
	/* ******************************************* CharacterController *****************************************/
	public CharacterController getCharacterController() {
		return characterController;
	}
	public void setCharacterController(CharacterController characterController) {
		this.characterController = characterController;
		startCharacterController();
	}
	protected void startCharacterController() {
		if(characterController != null){
			characterController.setup(this);
		}
	}
	protected void updateCharacterController(float delta) {
		if(characterController != null){
			characterController.update(this,delta);
		}
	}
	protected void notifyEventToController(GameEvent event) {
		if(characterController != null){
			characterController.onEvent(this,event);
		}
	}
	
	@Override
	public void onGlobalEvent(GlobalEvent evt) {
		fireEvent(evt);
		return;
	}
	
	public void shouldUseHealth(boolean shouldUse){
		this.healthController.setEnabled(shouldUse);
	}
}
