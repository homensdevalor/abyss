package com.hdv.abyss.actors;

import com.hdv.abyss.actors.configurators.CharacterBuilderConfigurator;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.entities.OpenDoorCharacter;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.statemachine.StateMapBuilder;

public class OpenDoorBuilder extends GameCharacterBuilder{

	public OpenDoorBuilder(CharacterBuilderConfigurator configurator) {
		// TODO Auto-generated constructor stub
		super(configurator);
	}
	
	@Override
	protected GameCharacter buildNewCharacter(AnimationMapBuilder animMapBuilder, StateMapBuilder<GameCharacter> stateMapBuilder) {
		GameCharacter character = new OpenDoorCharacter(animMapBuilder.buildMap(), stateMapBuilder);
		return character;
	}
	
}
