package com.hdv.abyss.actors;

import com.hdv.abyss.anim.DirectionalAnimation;

public interface AnimatedEntity {
	void setCurrentAnimation(DirectionalAnimation animation);
}
