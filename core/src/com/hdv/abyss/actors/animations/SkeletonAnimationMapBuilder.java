package com.hdv.abyss.actors.animations;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.anim.SpritesheetAnimation;

public class SkeletonAnimationMapBuilder implements AnimationMapBuilder{

	@Override
	public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
		DirectionalAnimation stillAnim = new SpritesheetAnimation("sprites/esqueleto/esqueleto-still.png", 1, 1);
		DirectionalAnimation walkAnim = new SpritesheetAnimation("sprites/esqueleto/esqueleto-walking.png", 5, 4);
		DirectionalAnimation attackAnim = new SpritesheetAnimation("sprites/esqueleto/esqueleto-attack.png", 5, 4);
		//FIXME: (?) criar animação de dano p/ esqueleto
		DirectionalAnimation damageAnim = new SpritesheetAnimation("sprites/esqueleto/esqueleto-hit.png", 5, 1, 1);
		DirectionalAnimation dodgeAnim = new SpritesheetAnimation("sprites/esqueleto/esqueleto-walking.png", 10, 4);
		DirectionalAnimation deadAnim = new SpritesheetAnimation("sprites/esqueleto/esqueleto-dying.png", 5, 4, 1);
		deadAnim.setPlayMode(PlayMode.NORMAL);
		
		Map<Object, DirectionalAnimation> stateToAnim = new HashMap<Object, DirectionalAnimation>();
		stateToAnim.put(BattleCharacterStates.STILL, stillAnim);
		stateToAnim.put(BattleCharacterStates.MOVING, walkAnim);
		stateToAnim.put(BattleCharacterStates.ATTACKING, attackAnim);
		stateToAnim.put(BattleCharacterStates.DAMAGED, damageAnim);
		stateToAnim.put(BattleCharacterStates.DODGING, dodgeAnim);
		stateToAnim.put(BattleCharacterStates.DEAD, deadAnim);
		
		return stateToAnim;
	}

}