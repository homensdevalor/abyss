package com.hdv.abyss.actors.animations;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.door.states.DoorStates;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.anim.SpritesheetAnimation;

public class DoorAnimationMapBuilder implements AnimationMapBuilder{

	@Override
	public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
//		DirectionalAnimation closedAnim = new SpritesheetAnimation("objects/grandeportademadeira.png", 1, 1, 1);
		DirectionalAnimation closedAnim = new SpritesheetAnimation("objects/grandegradedeferro.png", 1, 1, 1);
		DirectionalAnimation openAnim = new SpritesheetAnimation("background/void.png", 1, 1, 1);
		
		Map<Object, DirectionalAnimation> stateToAnim = new HashMap<Object, DirectionalAnimation>();
		stateToAnim.put(DoorStates.OPEN, openAnim);
		stateToAnim.put(DoorStates.CLOSED, closedAnim);
		
		return stateToAnim;
	}

	
	
}
