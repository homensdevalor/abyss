package com.hdv.abyss.actors.animations;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.anim.SpritesheetAnimation;

public class MainCharacterAnimationMapBuilder implements AnimationMapBuilder{

	@Override
	public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
		DirectionalAnimation stillAnim = new SpritesheetAnimation("sprites/main-character/mc-still.png", 1, 1);
		DirectionalAnimation walkAnim = new SpritesheetAnimation("sprites/main-character/mc-running.png", 5, 4);
		DirectionalAnimation attackAnim = new SpritesheetAnimation("sprites/main-character/mc-attack-sword.png", 5, 4);
		DirectionalAnimation damageAnim = new SpritesheetAnimation("sprites/main-character/mc-damage.png", 5, 1);
		DirectionalAnimation dodgeAnim = new SpritesheetAnimation("sprites/main-character/mc-dodging.png", 8, 4);
		DirectionalAnimation dyingAnim = new SpritesheetAnimation("sprites/main-character/mc-dying.png", 3, 5, 1);
		dyingAnim.setPlayMode(PlayMode.NORMAL);
		
		Map<Object, DirectionalAnimation> stateToAnim = new HashMap<Object, DirectionalAnimation>();
		stateToAnim.put(BattleCharacterStates.STILL, stillAnim);
		stateToAnim.put(BattleCharacterStates.MOVING, walkAnim);
		stateToAnim.put(BattleCharacterStates.ATTACKING, attackAnim);
		stateToAnim.put(BattleCharacterStates.DAMAGED, damageAnim);
		stateToAnim.put(BattleCharacterStates.DODGING, dodgeAnim);
		stateToAnim.put(BattleCharacterStates.DEAD, dyingAnim);
		
		return stateToAnim;
	}

}
