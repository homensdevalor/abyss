package com.hdv.abyss.actors.animations;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.lever.states.LeverStates;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.anim.SpritesheetAnimation;

public class LeverAnimationMapBuilder implements AnimationMapBuilder{

	@Override
	public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
		DirectionalAnimation stillAnim = new SpritesheetAnimation("objects/switch-normal.png", 1, 1, 1);
		DirectionalAnimation pressedAnim = new SpritesheetAnimation("objects/switch-pressed.png", 1, 1, 1);
		
		Map<Object, DirectionalAnimation> stateToAnim = new HashMap<Object, DirectionalAnimation>();
		stateToAnim.put(LeverStates.STILL, stillAnim);
		stateToAnim.put(LeverStates.PRESSED, pressedAnim);
		
		return stateToAnim;
	}
	
	
	
}
