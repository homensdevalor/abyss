package com.hdv.abyss.actors.animations;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.hdv.abyss.Resources;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.anim.SpritesheetAnimation;

public class SpikeAnimationMapBuilder implements AnimationMapBuilder{

	@Override
	public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
		DirectionalAnimation stillAnim = new SpritesheetAnimation(Resources.spikeStillSprite, 1, 1,1);
		DirectionalAnimation attackAnim = new SpritesheetAnimation(Resources.spikeAttackSprite, 7, 4,1);
		stillAnim.setPlayMode(PlayMode.NORMAL);
		attackAnim.setPlayMode(PlayMode.NORMAL);
		
		Map<Object, DirectionalAnimation> stateToAnim = new HashMap<Object, DirectionalAnimation>();
		stateToAnim.put(BattleCharacterStates.STILL, stillAnim);
		stateToAnim.put(BattleCharacterStates.ATTACKING, attackAnim);
		
		return stateToAnim;
	}

}
