package com.hdv.abyss.actors.animations;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.anim.SpritesheetAnimation;

public class SlimeAnimationMapBuilder implements AnimationMapBuilder{

	@Override
	public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
		DirectionalAnimation stillAnim = new SpritesheetAnimation("sprites/slime/slime-still.png", 1, 1,1);
		DirectionalAnimation damageAnim = new SpritesheetAnimation("sprites/slime/slime-damage.png", 3.5f, 4);
		DirectionalAnimation moveAnim = new SpritesheetAnimation("sprites/slime/slime-moving.png", 5.0f, 4);
		DirectionalAnimation attackAnim = new SpritesheetAnimation("sprites/slime/slime-attack.png", 5, 5);
		DirectionalAnimation dyingAnim = new SpritesheetAnimation("sprites/slime/slime-dying.png", 3.5f, 6,1);
		dyingAnim.setPlayMode(PlayMode.NORMAL);
		attackAnim.setOffset(new Vector2(0,0));
		
		Map<Object, DirectionalAnimation> stateToAnim = new HashMap<Object, DirectionalAnimation>();
		stateToAnim.put(BattleCharacterStates.STILL, stillAnim);
		stateToAnim.put(BattleCharacterStates.DAMAGED, damageAnim);
		stateToAnim.put(BattleCharacterStates.MOVING, moveAnim);
		stateToAnim.put(BattleCharacterStates.DEAD, dyingAnim);
		stateToAnim.put(BattleCharacterStates.ATTACKING, attackAnim);
		
		return stateToAnim;
	}

}
