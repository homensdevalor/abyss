package com.hdv.abyss.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hdv.abyss.actors.configurators.CharacterBuilderConfigurator;
import com.hdv.abyss.actors.configurators.DefaultBuilderConfigurator;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.statebuilders.BattleStateMapBuilder;
import com.hdv.abyss.actors.weapon.Weapon;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.EmptyAnimationMapBuilder;
import com.hdv.abyss.health.HealthValue;
import com.hdv.abyss.physics.PhysicalProperties;
import com.hdv.abyss.physics.collision.CollisionCategory;
import com.hdv.abyss.statemachine.StateMapBuilder;


public class GameCharacterBuilder {
	private static final int DEFAULT_MAX_HEALTH_VALUE = 5;

	AnimationMapBuilder animationMapBuilder;
	StateMapBuilder<GameCharacter> stateMapBuilder;
	final HealthValue value;
	final PhysicalProperties properties;
	private Weapon weapon;
	private final Vector2 defaultSpriteOffset;
	private CharacterBuilderConfigurator configurator;
	private Float normalVelocity;
	private Float fastVelocity;
	
	
	public GameCharacterBuilder() {
		this(new DefaultBuilderConfigurator());
	}
	public GameCharacterBuilder(CharacterBuilderConfigurator configurator) {
		animationMapBuilder = new EmptyAnimationMapBuilder();
		stateMapBuilder = new BattleStateMapBuilder();
		value = new HealthValue(DEFAULT_MAX_HEALTH_VALUE);
		properties = new PhysicalProperties();
		defaultSpriteOffset = new Vector2(0,0);
		this.normalVelocity = null;
		this.fastVelocity = null;
		
		this.configurator = configurator;
		this.configurator.setupDefaultProperties(this);
	}

	public PhysicalProperties getPhysicalProperties(){
		return this.properties;
	}
	public Weapon getWeapon(){
		return this.weapon;
	}

	public GameCharacterBuilder setPhysicalProperties(PhysicalProperties physicalProperties){
		this.properties.set(physicalProperties);
		return this;
	}

	public GameCharacterBuilder setBodyType(BodyType bodyType) {
		this.properties.setBodyType(bodyType);
		return this;
	}

	public GameCharacterBuilder setLinearDamping(float linearDamping) {
		this.getPhysicalProperties().setLinearDamping(linearDamping);
		return this;
	}
	public GameCharacterBuilder setCollisionCategory(CollisionCategory category) {
		this.properties.setCollisionCategory(category);
		return this;
	}
	public GameCharacterBuilder setStartPosition(float x, float y){
		return setStartPosition(new Vector2(x, y));
	}
	public GameCharacterBuilder setStartPosition(Vector2 position){
		this.properties.setStartPosition(position);
		return this;
	}
	public GameCharacterBuilder setSize(float width, float height){
		return this.setSize(new Vector2(width, height));
	}
	public GameCharacterBuilder setSize(Vector2 size){
		this.properties.setSize(size);
		return this;
	}
	public GameCharacterBuilder setNormalVelocity(float vel){
		this.normalVelocity = vel;
		return this;
	}
	public GameCharacterBuilder setFastVelocity(float vel) {
		this.fastVelocity = vel;
		return this;
		
	}
	
	public GameCharacterBuilder setAnimationMap(AnimationMapBuilder animationMapBuilder){
		this.animationMapBuilder = animationMapBuilder;
		return this;
	}
	public void setDefaultSpriteOffset(float defaultAnimXOffset, float defaultAnimYOffset) {
		this.defaultSpriteOffset.set(defaultAnimXOffset, defaultAnimYOffset);
	}
	public GameCharacterBuilder setMaxHealthValue(int maxValue){
		this.value.setMaxValue(maxValue);
		return this;
	}
	public GameCharacterBuilder setWeapon(Weapon weapon){
		this.weapon = weapon;
		return this;
	}
	
	public GameCharacterBuilder setStateMapBuilder(StateMapBuilder<GameCharacter> stateMapBuilder){
		this.stateMapBuilder = stateMapBuilder;
		return this;
	}

	public GameCharacter build(){
		return build(properties.getStartPosition());
	}
	public GameCharacter build(Vector2 pos){
		return build(pos, properties.getSize());
	}
	public GameCharacter build(Vector2 pos, Vector2 size){
		GameCharacter character = buildNewCharacter(this.animationMapBuilder, this.stateMapBuilder);
		properties.setStartPosition(pos);
		properties.setSize(size);
		character.getPhysicalProperties().set(properties);
		character.getHealthValue().set(value);
		if(normalVelocity != null){
			character.setNormalVelocity(normalVelocity);
		}
		if(fastVelocity != null){
			character.setFastVelocity(fastVelocity);
		}
		if(getWeapon() != null){
			character.setWeapon((Weapon) getWeapon().clone());
		}
		character.setDefaultSpriteOffset(defaultSpriteOffset);
		this.configurator.configure(character);
		return character;
	}
	
	protected GameCharacter buildNewCharacter(AnimationMapBuilder animMapBuilder, StateMapBuilder<GameCharacter> stateMapBuilder) {
		GameCharacter character = new GameCharacter(animMapBuilder.buildMap(), stateMapBuilder);
		
		return character;
	}
}
