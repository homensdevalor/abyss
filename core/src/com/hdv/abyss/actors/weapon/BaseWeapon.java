package com.hdv.abyss.actors.weapon;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicalWorld;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.physics.collision.CollisionCategory;

public class BaseWeapon implements Weapon{
	public static final float DEFAULT_START_ATTACK_OFFSET = 0.4f;
	public static final float DEFAULT_ATTACK_DURATION = 0.8f;
	public static final float DEFAULT_ATTACK_WIDTH = 1.5f;
	public static final float DEFAULT_ATTACK_HEIGHT = 1f;
	
	private float attackDurationInSeconds;
	private float startAttackTimeInSeconds;
	/** Define o alcance de um ataque. Ou seja a distância que o ataque atinge na direção em que o ataque é criado.*/
	private float attackReach;
	/** Define a extensão de um ataque. Ou seja a distância que o ataque se extende na direção ortogonal à que foi criado.*/
	private float attackRange;
	private float timeCounter;
	private DirectionalAnimation attackAnimation;
	private DirectionalAnimation stillAttackAnimation;
	private int weaponHit;

	public BaseWeapon() {
		this(DEFAULT_ATTACK_DURATION, DEFAULT_START_ATTACK_OFFSET
				, DEFAULT_ATTACK_WIDTH, DEFAULT_ATTACK_HEIGHT);
	}

	public BaseWeapon(BaseWeapon baseWeapon) {
		this(baseWeapon.getAttackDurationInSeconds()
				, baseWeapon.getStartAttackTimeInSeconds()
				, baseWeapon.getAttackRange(), baseWeapon.getAttackReach());
	}
	/** Inicializa arma.
	 * @param attackDuration define a duração total do ataque em segundos
	 * @param attackStartOffset define em segundos o tempo após o início do ataque que levará para o surgimento da bound box
	 * @param attackSize define o tamanho da bound box do ataque
	 * */
	public BaseWeapon(float attackDuration, float attackStartOffset, float attackRange, float attackReach) {
		attackDurationInSeconds = attackDuration;
		startAttackTimeInSeconds = attackStartOffset;
		this.attackRange = attackRange;
		this.attackReach = attackReach;
		this.attackAnimation = null;
		this.stillAttackAnimation = null;
		this.weaponHit = 1;
	}

	/* *********************************** Cloneable Interface ******************************************/
	@Override
	public Object clone(){
		return new BaseWeapon(this);
	}
	/* *********************************** Getters and Setters ***************************************/
//	public Vector2 getAttackSize() {
//		return new Vector2(attackRange, attackReach);
//	}
//	public void setAttackSize(float attackRange, float attackReach){
////		this.attackSize.set(attackSize);
//		this.attackRange = attackRange;
//		this.attackReach = attackReach;
//	}

	public int getWeaponHit() {
		return weaponHit;
	}
	public void setWeaponHit(int weaponHit) {
		this.weaponHit = weaponHit;
	}
	public float getAttackDurationInSeconds() {
		return attackDurationInSeconds;
	}
	public void setAttackDurationInSeconds(float attackDurationInSeconds) {
		this.attackDurationInSeconds = attackDurationInSeconds;
	}
	public float getStartAttackTimeInSeconds() {
		return startAttackTimeInSeconds;
	}
	public void setStartAttackTimeInSeconds(float startAttackTimeInSeconds) {
		this.startAttackTimeInSeconds = startAttackTimeInSeconds;
	}
	public float getTimeCounter() {
		return timeCounter;
	}
	public void setAttackAnimation(DirectionalAnimation animation){
		this.attackAnimation = animation;
	}
	public void setStillAttackAnimation(DirectionalAnimation animation){
		this.stillAttackAnimation = animation;
	}
	/* *********************************** Weapon Interface ******************************************/
	@Override
	public DirectionalAnimation getAttackAnimation() {
		return attackAnimation;
	}
	@Override
	public DirectionalAnimation getStillAttackAnimation() {
		return stillAttackAnimation;
	}
	@Override
	public void startAttack(PhysicsActor character) {
		timeCounter = 0;
	}

	@Override
	public void update(PhysicsActor character, float delta) {
		if(timeCounter < startAttackTimeInSeconds && 
				(timeCounter + delta) >= startAttackTimeInSeconds)
		{
			onCreateAttack(character, timeCounter, delta);
			Attack attack = createAttack(character, getAttackRange(), getAttackReach());
			attack.setHitValue(this.getWeaponHit());
			spawnAttack(attack, character.getPhysicalWorld());
			onAttackCreated(character, attack,  timeCounter + delta);
		}
		
		timeCounter += delta;
	}


	/** Chamado no momento anterior à criação do ataque.
	 * @param character o criador do ataque.
	 * @param previousTimeCounter tempo em segundos desde o início do ataque até antes desta iteração.
	 * @param delta tempo em segundos desde a última iteração.
	 * */
	protected void onCreateAttack(PhysicsActor character, float previousTimeCounter,
			float delta) 
	{
		/** Método a ser implementado por subclasses*/
	}
	/** Chamado no momento anterior à criação do ataque.
	 * @param character o criador do ataque.
	 * @param attack o ataque criado.
	 * @param previousTimeCounter tempo em segundos desde o início do ataque incluindo esta iteração.
	 * */
	protected void onAttackCreated(PhysicsActor character, Attack attack, float timeCounter)  
	{
		/** Método a ser implementado por subclasses*/
	}

	@Override
	public boolean isFinished() {
		return (timeCounter >= attackDurationInSeconds);
	}

	@Override
	public void onFinishAttack(PhysicsActor character) 
	{}
	@Override
	public boolean canReach(PhysicsActor owner, PhysicsActor target, float distanceOffset) {
		
		Vector2 attackSize = createAttackSize(owner, this.getAttackRange(), this.getAttackReach());
		Vector2 attackCenterPosition = createAttackPosition(owner, attackSize);
		Rectangle attackRectangle = createCenteredRectangle(attackCenterPosition, attackSize);
		Rectangle targetArea = createTargetArea(target);
		targetArea.setSize(targetArea.getWidth() + distanceOffset,targetArea.getHeight() + distanceOffset);
		
		boolean canReach =  attackRectangle.overlaps(targetArea);
		
		return canReach;
	}
	
	@Override
	public float getAttackReach() {
		return attackReach;
	}

	@Override
	public float getAttackRange() {
		return attackRange;
	}
	public void setAttackReach(float attackReach) {
		this.attackReach = attackReach;
	}
	public void setAttackRange(float attackRange) {
		this.attackRange = attackRange;
	}
	
	@Override
	public boolean reachAttackPosition(PhysicsActor owner, PhysicsActor target,
			float distanceTolerance) {

		Vector2 direction = new Vector2();
		Vector2 targetPos = new Vector2();
		getTargetPos(owner, target, targetPos, direction);
		direction.scl(-1);//Inverte direção, pois direção retornada é relativa ao centro do alvo para o attackPos
		
		float reach = getAttackReach();
		float range = getAttackRange();
				
		Vector2 positionArea = direction.x != 0 
									? new Vector2(reach, range/2f)
									: new Vector2(range/2f, reach);
		
		Vector2 ownerPos = owner.getPositionInWorld();
		
		return Math.abs(ownerPos.x - targetPos.x) <= positionArea.x 
				&& Math.abs(ownerPos.y - targetPos.y) <= positionArea.y;
	}
	@Override
	public Vector2 getAttackPosition(PhysicsActor owner, PhysicsActor target, Vector2 outPosition) {
		return getAttackPosition(owner, target, outPosition, null);
	}
	protected Vector2 getAttackPosition(PhysicsActor owner, PhysicsActor target, Vector2 outPosition
			, Vector2 outDirAttack) 
	{
		float attackReach = getAttackReach();
		float attackRange = getAttackRange();

//		Vector2 ownerPos = owner.getPositionInWorld();
		Vector2 targetPos = new Vector2();
		Vector2 dirVec = new Vector2();

		getTargetPos(owner, target, targetPos, dirVec);
		
//		if(directions[index] == Direction.Up || directions[index] == Direction.Down){
		if(dirVec.y != 0){
			targetPos.add(dirVec.x * attackRange, dirVec.y * attackReach);
		}
		else{
			targetPos.add(dirVec.x * attackReach, dirVec.y * attackRange);
		}
		
		if(outDirAttack != null){
			outDirAttack.set(dirVec);
		}
		outPosition.set(targetPos);
		return outPosition;
	}
	
	protected void getTargetPos(PhysicsActor owner, PhysicsActor target, Vector2 outTargetPos, Vector2 outDir){

		Direction directions[] = Direction.values();
		List<Vector2> vectors = getTargetVectors(directions, target);
		int index = choiceVector(owner.getPositionInWorld(), vectors);
		
		Vector2 targetPos = vectors.get(index);
		Vector2 dirVec = directions[index].toVector();

		outTargetPos.set(targetPos);
		outDir.set(dirVec);
	}
	
	/* ***********************************************************************************************/
	protected Rectangle createTargetArea(PhysicsActor target) {
		Vector2 centerPos = target.getPositionInWorld();
		Vector2 size = target.getSizeInWorld();
		return createCenteredRectangle(centerPos, size);
	}

	private Rectangle createCenteredRectangle(Vector2 centerPos, Vector2 size) {
		return new Rectangle(centerPos.x - size.x/2f, centerPos.y - size.y/2f, size.x, size.y);
	}

	/* ************************************ Template Methods *****************************************/
	/** Cria o objeto de ataque.
	 * @param character o criador do ataque.
	 * @param character o criador do ataque.*/
	public Attack createAttack(PhysicsActor character, float attackRange, float attackReach) {
		Attack attack = newAttackInstance(character, character.getDirection());
		Vector2 finalAttackSize = createAttackSize(character, attackRange, attackReach);
		attack.getPhysicalProperties().setSize(finalAttackSize);
		attack.getPhysicalProperties().setStartPosition(createAttackPosition(character, finalAttackSize));
		attack.getPhysicalProperties().setCollisionCategory(createCollisionCattegory(character, attack));
		return attack;
	}
	protected Attack newAttackInstance(PhysicsActor character, Direction direction) {
		return new Attack(character, direction);
	}

	protected Vector2 createAttackSize(PhysicsActor character, float attackRange, float attackReach) {	
		Direction direction = character.getDirection();
		Vector2 size = (direction == Direction.Down || direction == Direction.Up 
									? new Vector2(attackRange, attackReach) 
									: new Vector2(attackReach,attackRange));
		return size;
	}

	/** Define a categoria de colisão do ataque para otimizar tratamento de colisão (evitar ataque colidir com seu criador).
	 * Caso o personagem seja da categoria {@link CollisionCategory.Enemy}, 
	 * o ataque será da categoria {@link CollisionCategory.EnemyAttack}.
	 * Isto faz com que o ataque não colida com atores de tipo {@link CollisionCategory.Enemy}.
	 * Se o personagem for da categoria {@link CollisionCategory.Player}, 
	 * o ataque será da categoria {@link CollisionCategory.PlayerAttack}.
	 * Isto faz com que o ataque não colida com atores de tipo {@link CollisionCategory.Player}.
	 * Em outros casos não altera categoria de colisão (por padrão continua {@link CollisionCategory.General}).
	 * Isto faz com que o ataque não colida com qualquer tipo de PhysicsActor.
	 * @param character o criador do ataque.
	 * @param attack o ataque a ser ajustado.*/
	protected CollisionCategory createCollisionCattegory(PhysicsActor character,
			Attack attack) {
		CollisionCategory characterCategory = character.getPhysicalProperties().getCollisionCategory();
		CollisionCategory attackCategory    = attack.getPhysicalProperties().getCollisionCategory();
		
		if(characterCategory == CollisionCategory.Enemy){
			attackCategory = CollisionCategory.EnemyAttack;
		}
		else if(characterCategory == CollisionCategory.Player){
			attackCategory = CollisionCategory.PlayerAttack;
		}
		return attackCategory;
	}
	public void spawnAttack(Attack attack, PhysicalWorld world) {
		if(world != null){
			world.spawn(attack, attack.getPhysicalProperties().getStartPosition()
					, secondsToMillis(getAttackDurationInSeconds() - getStartAttackTimeInSeconds()));
		}
	}
	protected long secondsToMillis(float timeInSeconds) {
		return (long) (timeInSeconds * 1000);
	}
	/** retorna posição para criação do ataque (posição adjacente à bound box atual e direcionada
	 * de acordo com a orientação atual do personagem)*/
	protected Vector2 createAttackPosition(PhysicsActor character, Vector2 attackSize) {
		Vector2 totalSize = character.getSizeInWorld().cpy().add(attackSize).scl(0.5f);
		
		Vector2 playerDir = character.getDirection().toVector().cpy().nor();
		totalSize.x *= playerDir.x;
		totalSize.y *= playerDir.y;
		Vector2 spawnPos = totalSize.add(character.getPositionInWorld());
		
		return spawnPos;
	}

	private int choiceVector(Vector2 ownerPos, List<Vector2> vectors) {
		float minDistance = vectors.get(0).dst(ownerPos);
		int index = 0;
		for(int i=1; i < vectors.size(); ++i){
			float distance = vectors.get(i).dst(ownerPos);
			if(distance < minDistance){
				minDistance = distance;
				index = i;
			}
		}
		return index;
	}

	private List<Vector2> getTargetVectors(Direction[] directions, PhysicsActor target) {
		Vector2 targetCenter = target.getPositionInWorld();
		Vector2 targetMiddleSize = target.getSizeInWorld().cpy().scl(0.5f);
		
		Vector2 tmp = new Vector2();
		List<Vector2> targets = new ArrayList<Vector2>(directions.length);
		for(Direction direction : directions){
			Vector2 dirVec = direction.toVector();
			tmp.set(dirVec.x * targetMiddleSize.x, dirVec.y* targetMiddleSize.y);
			tmp.add(targetCenter);
			targets.add(tmp.cpy());
		}
		
		return targets;
	}
}
