package com.hdv.abyss.actors.weapon;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicsActor;

/** Uma arma que gera um ataque com mesmo tamanho e dimensão do corpo*/
public class BodyWeapon extends BaseWeapon {
	@Override
	protected Attack newAttackInstance(PhysicsActor character,
			Direction direction) {
		return new AttachedAttack(character, direction);
	}
	@Override
	protected Vector2 createAttackPosition(PhysicsActor character,
			Vector2 attackSize) {
		return character.getPositionInWorld();
	}
	@Override
	protected Vector2 createAttackSize(PhysicsActor character,
			float attackRange, float attackReach) {
		return character.getSizeInWorld();
	}
}
