package com.hdv.abyss.actors.weapon;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.hdv.abyss.actors.MovementController;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicsActor;
import com.hdv.abyss.statemachine.events.Movement;

public class DaggerWeapon extends BaseWeapon {
	private static final float DEFAULT_DISTANCE_TO_MOVE = 1.5f;
//	private static final float DEFAULT_DISTANCE_TO_MOVE = 10f;
	private static final float DEFAULT_ATTACK_RANGE = 0.5f;
	private static final float DEFAULT_ATTACK_REACH = 1f;
	private boolean moving;

	MovementController continousMovement;
	
	public DaggerWeapon() {
		this(DEFAULT_DISTANCE_TO_MOVE);
	}
	public DaggerWeapon(float distanceToMove) {
		setDefaultValues(distanceToMove);
	}
	public DaggerWeapon(DaggerWeapon otherWeapon) {
		super(otherWeapon);
		setDefaultValues(otherWeapon.getDistanceToMove());
	}
	private final void setDefaultValues(float distanceToMove) {
		moving = false;
		continousMovement = new MovementController();
		continousMovement.setDistanceToMove(distanceToMove);
		
		super.setAttackRange(DEFAULT_ATTACK_RANGE);
		super.setAttackReach(DEFAULT_ATTACK_REACH);
	}
	@Override
	public Object clone() {
		return new DaggerWeapon(this);
	}
	
	public float getDistanceToMove() {
		return continousMovement.getDistanceToMove();
	}
	public void setDistanceToMove(float distanceToMove) {
		continousMovement.setDistanceToMove(distanceToMove);
	}

	@Override
	public void startAttack(PhysicsActor character) {
		super.startAttack(character);
		moving = true;
		continousMovement.resetMovement(character.getPositionInWorld());
	}
	private float getCharacterVelocity(PhysicsActor character) {
		return character.getVelocity().len();
	}
	@Override
	protected Attack newAttackInstance(PhysicsActor character, Direction direction) {
		return new AttachedAttack(character, direction);
	}
	
	@Override
	public void update(PhysicsActor character, float delta) {
		super.update(character, delta);
		if(moving){
			moveCharacter(character);
			
			if(!continousMovement.hasStarted()){
				continousMovement.setVelocity(getCharacterVelocity(character));
				continousMovement.start();
			}
			continousMovement.update(delta, character);
			moving = !continousMovement.hasFinished();
		}
	}

	@Override
	public void onFinishAttack(PhysicsActor character) {
		super.onFinishAttack(character);
		this.moving = false;
		stopCharacter(character);
	}
	
	private void moveCharacter(PhysicsActor character) {
		if(character instanceof GameCharacter){
			GameCharacter gameCharacter = ((GameCharacter)character);
			gameCharacter.move(gameCharacter.getMoveDirection(), Movement.FAST);
		}
		else{
			float velMag = character.getVelocity().len2();
			Vector2 moveDir = character.getDirection().toVector();
			character.getBody().setLinearVelocity(moveDir.cpy().scl(velMag));
		}
	}

	public void stopCharacter(PhysicsActor character) {
		if(character instanceof GameCharacter){
			GameCharacter gameCharacter = ((GameCharacter)character);
			gameCharacter.stop();
		}
		else{
			Body body = character.getBody();
			if(body != null){
				body.setLinearVelocity(0, 0);
			}
		}
	}
	
}
