package com.hdv.abyss.actors.weapon;

import java.util.ArrayList;
import java.util.List;

public class WeaponQuickSlot {
	private int currentWeaponIndex;
	private final List<Weapon> weapons;
	
	public WeaponQuickSlot() {
		weapons = new ArrayList<Weapon>();
		currentWeaponIndex = 0;
	}
	
	public Weapon getCurrentWeapon() {
		return weapons.size() > 0 ? weapons.get(currentWeaponIndex) : null;
	}

	public void setCurrentWeapon(Weapon weapon) {
		if(!this.weapons.contains(weapon)){
			this.weapons.add(weapon);
		}
		this.currentWeaponIndex = weapons.indexOf(weapon);
	}
	public void addWeapon(Weapon weapon){
		if(!weapons.contains(weapon)){
			this.weapons.add(weapon);
		}
	}
	public void removeWeapon(Weapon weapon){
		int indexOfWeapon = weapons.indexOf(weapon);
		if(indexOfWeapon < 0 || !weapons.remove(weapon)){
			return;
		}
		if(indexOfWeapon <= currentWeaponIndex){
			//Impede que índice seja negativo
			currentWeaponIndex = Math.max(0, currentWeaponIndex - 1);
		}
	}

	public void nextWeapon() {
		this.currentWeaponIndex = (this.currentWeaponIndex + 1) % this.weapons.size();
	}
}
