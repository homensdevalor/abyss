package com.hdv.abyss.actors.weapon;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hdv.abyss.Configurations;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicsActor;

public class Attack extends PhysicsActor{
	//Tamanho padrão, definido por número de pixels
	private static final Vector2 HORIZONTAL_ATTACK_SIZE = new Vector2(1.5f,1f).scl(Configurations.TILE_IN_METERS);
	private static final Vector2 VERTICAL_ATTACK_SIZE = new Vector2(1f,1.5f).scl(Configurations.TILE_IN_METERS);
	private final Object attackCreator;
	private final Direction attackDirection;
	private int hitValue;
	
	public Attack(Object attackCreator, Direction direction) {
		super();
		this.getPhysicalProperties().setIsSensor(true);
		Vector2 size = (direction == Direction.Down || direction == Direction.Up ? 
									HORIZONTAL_ATTACK_SIZE : VERTICAL_ATTACK_SIZE);
		this.getPhysicalProperties().setSize(size);
		//Parece ser necessário para detecção de colisão
		this.getPhysicalProperties().setBodyType(BodyType.DynamicBody);
		this.getPhysicalProperties().setIsSensor(true);
		
		this.attackCreator = attackCreator;
		this.attackDirection = direction;
		this.hitValue = 1;
	}
	
	public Object getAttackCreator() {
		return attackCreator;
	}
	
	@Override
	public Direction getDirection() {
		return this.attackDirection;
	}

	public int getHitValue() {
		return hitValue;
	}
	public void setHitValue(int hitValue) {
		this.hitValue= hitValue ;
	}
}
