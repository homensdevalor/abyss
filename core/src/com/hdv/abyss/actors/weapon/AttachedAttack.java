package com.hdv.abyss.actors.weapon;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.physics.Direction;
import com.hdv.abyss.physics.PhysicsActor;

/** Um ataque que mantém fixa uma posição relativa ao seu criador*/
public class AttachedAttack extends Attack{
	PhysicsActor attackCreator;
	final Vector2 relativePosition, creatorPosition;
	public AttachedAttack(PhysicsActor attackCreator, Direction direction) {
		super(attackCreator, direction);
		this.attackCreator = attackCreator;
		relativePosition = new Vector2();
		creatorPosition = new Vector2();
	}

	@Override
	protected void onAttachToWorld() {
		super.onAttachToWorld();

		creatorPosition.set(attackCreator.getPositionInWorld());
		relativePosition.set(this.getPositionInWorld().cpy().sub(creatorPosition));
	}
	@Override
	public void act(float delta) {
		super.act(delta);
		creatorPosition.set(attackCreator.getPositionInWorld()).add(relativePosition);
		this.setPosition(creatorPosition.x, creatorPosition.y);
	}
}
