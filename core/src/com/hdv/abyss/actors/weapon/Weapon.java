package com.hdv.abyss.actors.weapon;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.physics.PhysicsActor;

public interface Weapon extends Cloneable{
	void startAttack(PhysicsActor character);
	void update(PhysicsActor character, float delta);
	boolean isFinished();
	void onFinishAttack(PhysicsActor character);
	Object clone();
	boolean canReach(PhysicsActor owner, PhysicsActor target, float distanceOffset);
	/** Retorna a distância máxima que o ataque alcança na sua direção de disparo.*/
	float getAttackReach();
	/** Retorna a "largura" do ataque na direção perpendicular à direção de disparo.*/
	float getAttackRange();
	Vector2 getAttackPosition(PhysicsActor owner, PhysicsActor target, Vector2 outPosition);
	/** Determina se o personagem owner está em uma posição que possa atacar o target.*/
	boolean reachAttackPosition(PhysicsActor owner, PhysicsActor target,
			float distanceTolerance);
	
	DirectionalAnimation getAttackAnimation();
	DirectionalAnimation getStillAttackAnimation();
}
