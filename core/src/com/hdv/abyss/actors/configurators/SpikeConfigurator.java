package com.hdv.abyss.actors.configurators;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.animations.SpikeAnimationMapBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.statebuilders.AttackMachineStateBuilder;
import com.hdv.abyss.actors.weapon.BodyWeapon;
import com.hdv.abyss.physics.collision.CollisionCategory;

public class SpikeConfigurator extends DefaultBuilderConfigurator{
	public static final Vector2 DEFAULT_SPIKE_SIZE = new Vector2(0.5f,0.5f);
	public static final long SPIKE_WAITING_ATTACK_TIME = 1000;
	public static final float SPIKE_START_ATTACK_TIME_SECONDS = 0.02f;

	@Override
	public void setupDefaultProperties(GameCharacterBuilder builder) {
		super.setupDefaultProperties(builder);
		builder.setBodyType(BodyType.StaticBody);
		builder.setCollisionCategory(CollisionCategory.Enemy);
		builder.getPhysicalProperties().setIsSensor(true);
		builder.getPhysicalProperties().setSize(DEFAULT_SPIKE_SIZE);
		builder.setAnimationMap(new SpikeAnimationMapBuilder());
		builder.setStateMapBuilder(new AttackMachineStateBuilder(SPIKE_WAITING_ATTACK_TIME));
	}
	@Override
	public void configure(GameCharacter character) {
		super.configure(character);
		
		BodyWeapon spikeWeapon = new BodyWeapon();
		spikeWeapon.setStartAttackTimeInSeconds(SPIKE_START_ATTACK_TIME_SECONDS);
		spikeWeapon.setAttackDurationInSeconds(SPIKE_WAITING_ATTACK_TIME/1000f);
		character.setWeapon(spikeWeapon);
		character.setZIndex(0);
	}
}
