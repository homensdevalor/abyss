package com.hdv.abyss.actors.configurators;

import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.animations.SlimeAnimationMapBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.weapon.BaseWeapon;
import com.hdv.abyss.physics.collision.CollisionCategory;

public class SlimeBuilderConfigurator extends DefaultBuilderConfigurator{
	public static final float DEFAULT_SLIME_WIDTH = 0.6f;
	public static final float DEFAULT_SLIME_HEIGHT = 0.55f;
	public static final float DEFAULT_SLIME_VELOCITY = 2f;
	public static final int SLIME_LIFE = 3;
	public static final float DEFAULT_SLIME_ATTACK_REACH = 1.0f;
	public static final float DEFAULT_SLIME_ATTACK_RANGE = DEFAULT_SLIME_WIDTH*1.5f;
	public static final float DEFAULT_ANIM_X_OFFSET = 0;
	public static final float DEFAULT_ANIM_Y_OFFSET = 0.32f;

	@Override
	public void setupDefaultProperties(GameCharacterBuilder builder) {
		super.setupDefaultProperties(builder);
		builder.setAnimationMap(new SlimeAnimationMapBuilder())
		.setMaxHealthValue(SLIME_LIFE)
		.setNormalVelocity(DEFAULT_SLIME_VELOCITY)
		.setCollisionCategory(CollisionCategory.Enemy)
		.setSize(DEFAULT_SLIME_WIDTH, DEFAULT_SLIME_HEIGHT)
		.setDefaultSpriteOffset(DEFAULT_ANIM_X_OFFSET, DEFAULT_ANIM_Y_OFFSET);
	}
	@Override
	public void configure(GameCharacter character) {
		super.configure(character);
		BaseWeapon slimeBaseWeapon = new BaseWeapon();
		slimeBaseWeapon.setAttackRange(DEFAULT_SLIME_ATTACK_RANGE);
		slimeBaseWeapon.setAttackReach(DEFAULT_SLIME_ATTACK_REACH);
		character.setWeapon(slimeBaseWeapon);
	}
	
}
