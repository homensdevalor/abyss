package com.hdv.abyss.actors.configurators;

import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;

public interface CharacterBuilderConfigurator {
	void setupDefaultProperties(GameCharacterBuilder builder);
	void configure(GameCharacter character);
}
