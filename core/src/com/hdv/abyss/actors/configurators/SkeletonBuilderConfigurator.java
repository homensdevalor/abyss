package com.hdv.abyss.actors.configurators;

import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.animations.SkeletonAnimationMapBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.statebuilders.BattleStateMapBuilder;
import com.hdv.abyss.actors.weapon.BaseWeapon;
import com.hdv.abyss.physics.collision.CollisionCategory;


public class SkeletonBuilderConfigurator extends DefaultBuilderConfigurator{
	public static final float DEFAULT_SKELETON_VELOCITY = 3f;
	public static final float DEFAULT_SKELETON_WIDTH = 0.6f;
	public static final float DEFAULT_SKELETON_HEIGHT = 1f;
	@Override
	public void setupDefaultProperties(GameCharacterBuilder builder) {
		super.setupDefaultProperties(builder);
		builder.setAnimationMap(new SkeletonAnimationMapBuilder());
		builder.setStateMapBuilder(new BattleStateMapBuilder());
		builder.setCollisionCategory(CollisionCategory.Enemy);
		builder.setNormalVelocity(DEFAULT_SKELETON_VELOCITY);
		builder.setSize(DEFAULT_SKELETON_WIDTH, DEFAULT_SKELETON_HEIGHT);
		builder.setMaxHealthValue(5);
	}
	@Override
	public void configure(GameCharacter character) {
		BaseWeapon skeletonWeapon = new BaseWeapon();
		skeletonWeapon.setWeaponHit(2);
		character.setWeapon(skeletonWeapon);
	}
}
