package com.hdv.abyss.actors.configurators;

import java.util.HashMap;
import java.util.Map;

import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.statemachine.BattleCharacterStates;
import com.hdv.abyss.anim.AnimationMapBuilder;
import com.hdv.abyss.anim.DirectionalAnimation;
import com.hdv.abyss.anim.SpritesheetAnimation;

public class BossConfigurator extends DefaultBuilderConfigurator{
	class BossAnimationMapBuilder implements AnimationMapBuilder{
		@Override
		public Map<? extends Object, ? extends DirectionalAnimation> buildMap() {
			Map<Object, DirectionalAnimation> animMap = new HashMap<Object, DirectionalAnimation>();
			animMap.put(BattleCharacterStates.STILL, new SpritesheetAnimation("tilesets/owl-idle.png", 4, 4,1));
			return animMap;
		}
		
	}
	
	@Override
	public void setupDefaultProperties(GameCharacterBuilder builder) {
		super.setupDefaultProperties(builder);
		builder.getPhysicalProperties().setIsSensor(true);
		builder.setAnimationMap(new BossAnimationMapBuilder());
	}
}
