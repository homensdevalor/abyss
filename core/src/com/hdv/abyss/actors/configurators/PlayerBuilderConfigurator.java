package com.hdv.abyss.actors.configurators;

import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.animations.MainCharacterAnimationMapBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.statemachine.statebuilders.PlayerStateMapBuilder;
import com.hdv.abyss.actors.weapon.BaseWeapon;
import com.hdv.abyss.actors.weapon.DaggerWeapon;
import com.hdv.abyss.anim.SpritesheetAnimation;
import com.hdv.abyss.physics.collision.CollisionCategory;

public class PlayerBuilderConfigurator extends DefaultBuilderConfigurator{
	public static final float DEFAULT_START_ATTACK_TIME = 0.2f;
	public static final float DEFAULT_PLAYER_NORMAL_VELOCITY = 7.5f;
	public static final float DEFAULT_PLAYER_FAST_VELOCITY   = DEFAULT_PLAYER_NORMAL_VELOCITY * 1.5f;
	public static final float DEFAULT_PLAYER_WIDTH 			 = 0.6f;
	public static final float DEFAULT_PLAYER_HEIGHT          = 1f;
	public static final float SWORD_ATTACK_RANGE = 1f;

	@Override
	public void setupDefaultProperties(GameCharacterBuilder builder) {
		super.setupDefaultProperties(builder);
		builder.setStateMapBuilder(new PlayerStateMapBuilder());
		builder.setAnimationMap(new MainCharacterAnimationMapBuilder());
		builder.setNormalVelocity(DEFAULT_PLAYER_NORMAL_VELOCITY);
		builder.setFastVelocity(DEFAULT_PLAYER_FAST_VELOCITY);
		builder.setCollisionCategory(CollisionCategory.Player);
		builder.setSize(DEFAULT_PLAYER_WIDTH, DEFAULT_PLAYER_HEIGHT);
	}
	@Override
	public void configure(GameCharacter character) {
		BaseWeapon swordWeapon = new BaseWeapon();
		swordWeapon.setStillAttackAnimation(new SpritesheetAnimation("sprites/main-character/mc-still-sword.png", 1, 1));
		swordWeapon.setAttackRange(SWORD_ATTACK_RANGE);
		swordWeapon.setWeaponHit(2);
		swordWeapon.setStartAttackTimeInSeconds(DEFAULT_START_ATTACK_TIME);
		character.setWeapon(swordWeapon);
		BaseWeapon daggerWeapon = new DaggerWeapon();
		daggerWeapon.setAttackAnimation(new SpritesheetAnimation("sprites/main-character/mc-attack-dagger.png", 5, 4));
		daggerWeapon.setStillAttackAnimation(new SpritesheetAnimation("sprites/main-character/mc-still-dagger.png", 1, 1));
		daggerWeapon.setStartAttackTimeInSeconds(DEFAULT_START_ATTACK_TIME);
		character.getWeaponQuickSlot().addWeapon(daggerWeapon);
	}
}
