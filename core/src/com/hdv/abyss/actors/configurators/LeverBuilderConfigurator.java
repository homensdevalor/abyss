package com.hdv.abyss.actors.configurators;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.LeverBuilder;
import com.hdv.abyss.actors.animations.LeverAnimationMapBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.lever.states.LeverStateMapBuilder;
import com.hdv.abyss.global.events.type.LeverSwitch;

public class LeverBuilderConfigurator implements CharacterBuilderConfigurator{
	
	private LeverSwitch currentTrigger;
	
	public LeverSwitch getCurrentTrigger() {
		return currentTrigger;
	}

	public void setCurrentTrigger(LeverSwitch currentTrigger) {
		this.currentTrigger = currentTrigger;
	}

	@Override
	public void setupDefaultProperties(GameCharacterBuilder buil) {
		LeverBuilder builder = (LeverBuilder) buil;
		builder.setTrigger(currentTrigger);
		builder.setAnimationMap(new LeverAnimationMapBuilder());
		builder.setSize(new Vector2(1, 1));
		builder.setStateMapBuilder(new LeverStateMapBuilder());
	}

	@Override
	public void configure(GameCharacter character) {
		character.shouldUseHealth(false);
		return;
	}

}
