package com.hdv.abyss.actors.configurators;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;
import com.hdv.abyss.actors.weapon.BaseWeapon;

public class DefaultBuilderConfigurator implements CharacterBuilderConfigurator{

	private static final float DEFAULT_NORMAL_VELOCITY = 5f;
	private static final int DEFAULT_MAX_HEALTH_VALUE = 5;
	private static final float DEFAULT_LINEAR_DAMPING = 30;

	@Override
	public void setupDefaultProperties(GameCharacterBuilder builder) {
		builder.setBodyType(BodyType.DynamicBody)
		   .setLinearDamping(DEFAULT_LINEAR_DAMPING)
		   .setNormalVelocity(DEFAULT_NORMAL_VELOCITY)
		   .setMaxHealthValue(DEFAULT_MAX_HEALTH_VALUE);
	}
	@Override
	public void configure(GameCharacter character) 
	{
		character.setWeapon(new BaseWeapon());
	}

}
