package com.hdv.abyss.actors.configurators;

import com.badlogic.gdx.math.Vector2;
import com.hdv.abyss.actors.GameCharacterBuilder;
import com.hdv.abyss.actors.animations.DoorAnimationMapBuilder;
import com.hdv.abyss.actors.door.states.DoorStateMapBuilder;
import com.hdv.abyss.actors.entities.GameCharacter;

public class DoorConfigurator implements CharacterBuilderConfigurator{

	@Override
	public void setupDefaultProperties(GameCharacterBuilder builder) {
		builder.setAnimationMap(new DoorAnimationMapBuilder());
		builder.setSize(new Vector2(2, 2));
		builder.setStateMapBuilder(new DoorStateMapBuilder());
	}

	@Override
	public void configure(GameCharacter character) {
		character.shouldUseHealth(false);
	}
	
	
	
}
